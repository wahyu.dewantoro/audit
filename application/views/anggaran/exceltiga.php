<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Anggaran ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Anggaran <br><?= $kd_skpd." - ".$nm_sub_unit ?></h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>Kode Rekening</th>
                                <th>Rekening</th>
                                <th>Program</th>
                                <th>Kegiatan</th>
                                <th>Anggaran Awal</th>
                                <th>Anggaran Perubahan 1</th>
                                <th>Anggaran Perubahan 2</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($anggaran_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td><?php if($rk->akun_akrual_5<>''){ echo $rk->akun_akrual_5; }else{echo "-";} ?></td>
                                <td><?php if($rk->nm_akrual_5<>''){ echo $rk->nm_akrual_5;}else{echo "-";}?></td>               
                                <td><?= $rk->ket_program ?></td>
                                <td><?= $rk->ket_kegiatan ?></td>
                                <td align ="right"><?php echo number_format($rk->anggaran_awal,'2',',','.'); ?></td>
                                <td align ="right"><?php echo number_format($rk->anggaran_perubahan_1,'2',',','.'); ?></td>
                                <td align ="right"><?php echo number_format($rk->anggaran_perubahan_2,'2',',','.'); ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="5" align="right">TOTAL</th>
                                <td class="text-right"><?= $awal ?></td>
                                <td class="text-right"><?= $satu ?></td>
                                <td class="text-right"><?= $dua ?></td>
                            </tr>
                        </tfoot>
					</table>