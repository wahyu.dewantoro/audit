<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            	<div class="row">
            		<div class="col-md-4">
            			<table>
            				<tr>
            					<td>SKPD</td>
            					<td>: <strong><?= $kd_skpd?></strong></td>
            				</tr>
            				<tr>
            					<td>Sub Unit</td>
            					<td>: <strong><?= $nm_sub_unit ?></strong></td>
            				</tr>
            			</table>
            		</div>
            		<div class="col-md-8">

		                    <form action="<?php echo $action ?>" method="get">
		                    	<div class="row">
		                    		<div class="col-md-6">
		                    			<div class="form-group">
			                    			<label><strong>Akrual 3</strong></label>
			                    			<select name='akun_akrual_3' class='form-control select2 form-control-sm' data-placeholder='Semua'>
							                    <option value=""></option>
							                    <?php foreach($akrual3 as $ak){?>
							                    <option <?php if($akun_akrual_3== $ak->akun_akrual_3 ){echo "selected";}?> value="<?=  $ak->akun_akrual_3 ?>"><?= $ak->akun_akrual_3.' - '.$ak->nm_akrual_3  ?></option>
							                    <?php } ?>
						                    </select>
					                    </div>
		                    		</div>
		                    		<div class="col-md-6">
		                    			<div class="form-group">
		                    				<label><strong>Pencarian</strong></label>
		                    				<div class="input-group">
						                    	<input type='hidden' name='kd_skpd' value='<?= $kd_skpd ?>' >
						                    	<input type='hidden' name='nm_sub_unit' value='<?= $nm_sub_unit ?>' >

						                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
						                            <span class="input-group-btn">
						                            <div class="btn-group">

						                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
						                                <?php if ($q <> '' || $akun_akrual_3<>'')  { ?>
						                                    <a href="<?php echo $action ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
						                              <?php }


						                                 ?>
														 <?= anchor($cetak,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success"')?>
						                            </div>
						                        </span>
						                    </div>
						                </div>
		                    		</div>
		                    	</div>

		                </form>

            		</div>
            	</div>

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Kode Rekening</th>
								<th>Rekening</th>
								<th>Anggaran Awal</th>
								<th>Anggaran Perubahan 1</th>
								<th>Anggaran Perubahan 2</th>
								<th></th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($anggaran_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php if($rk->akun_akrual_3<>''){ echo $rk->akun_akrual_3; }else{echo "-";} ?></td>
								<td><?php if($rk->nm_akrual_3<>''){ echo $rk->nm_akrual_3;}else{echo "-";}?></td>								
								<td align="right"><?php echo number_format($rk->anggaran_awal,'2',',','.'); ?></td>
								<td align="right"><?php echo number_format($rk->anggaran_perubahan_1,'2',',','.'); ?></td>
								<td align="right"><?php echo number_format($rk->anggaran_perubahan_2,'2',',','.'); ?></td>
								<th>
									<form action="<?= base_url().'anggaran/readtiga'?>" method="get">
										<input type="hidden" name="kd_skpd" value="<?= urlencode($kd_skpd ) ?>" >
										<input type="hidden" name="nm_sub_unit" value="<?= urlencode($nm_sub_unit ) ?>" >
										<input type="hidden" name="akun_akrual_3" value="<?= urlencode($rk->akun_akrual_3 ) ?>" >
										<input type="hidden" name="nm_akrual_3" value="<?= urlencode($rk->nm_akrual_3 ) ?>" >
										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
									
								</th>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

					<div class="float-right">
						<?php echo $pagination ?>
					</div>
					<div class="float-right">
                    <div class="btn-group">
                        <button  class="btn  page-link btn-space btn-success" disabled>  Awal : <?php echo $awal ?></button>
                    <button  class="btn  page-link btn-space btn-warning" disabled>  Perubahan 1 : <?php echo $satu ?></button>
                    <button  class="btn  page-link btn-space btn-success" disabled>  Perubahan 2 : <?php echo $dua ?></button>
                    </div>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>