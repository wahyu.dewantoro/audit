<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                
                    <form action="<?php echo site_url('anggaran/realisasi'); ?>"  method="get">
                    <div class="row">
                        <div class="col-md-6">
                                <select class="form-control form-control-sm select2" name="q">
                                <option value="">Pilih SKPD/OPD</option>
                                <?php foreach($subunit as $su){?>
                                <option <?php if($su->nm_sub_unit==$q){echo "selected";}?> value="<?= $su->nm_sub_unit ?>"><?= $su->nm_sub_unit ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <span class="input-group-btn">
                                <div class="btn-group">
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                    <?php if ($q <> '')  { ?>
                                        <a href="<?php echo site_url('anggaran/realisasi'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                  <?php }
                                     ?>
                                     
                                </div>
                            </span>
                        </div>
                            
                    </div>
                </form>
                
                <div class="table-responsive">
                    <table class="table  table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>SKPD</th>
								<th>Nm Sub Unit</th>
                                <th>Rekening</th>
								<th>Anggaran </th>
								<th>Realisasi</th>
                                <th>Presentase</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($anggaran_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>

								<td><?php echo $rk->kd_skpd ?></td>
								<td><?php echo $rk->nm_sub_unit ?></td>
                                <td class="cell-detail"><span><?= $rk->kd_rek_gabung?></span><span class="cell-detail-description"><?= $rk->nm_rek_5?></span></td>
								<td align="right"><?php echo number_format($rk->anggaran,'2',',','.') ?></td>
							     <td align="right"><?php echo number_format($rk->nilai,'2',',','.') ?></td>
                                 <td align="center"> <?php if($rk->anggaran > 0){echo  number_format(($rk->nilai/$rk->anggaran)*100,'0','',''); }else{ echo "- ";} ?> %</td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

					<div class="float-right">
						<?php echo $pagination ?>
					</div>
                    <div class="float-right">
                    <div class="btn-group">

                    </div>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>