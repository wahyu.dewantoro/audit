<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <form action="<?php echo site_url('anggaran/index'); ?>"  method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control select2 form-control-sm" name="q" data-placeholder="Pilih SKPD/OPD">
                                <option value=""></option>
                                <?php foreach($subunit as $su){?>
                                <option <?php if($su->nm_sub_unit==$q){echo "selected";}?> value="<?= $su->nm_sub_unit ?>"><?= $su->nm_sub_unit ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <span class="input-group-btn">
                                <div class="btn-group">
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                    <?php if ($q <> '')  { ?>
                                        <a href="<?php echo site_url('anggaran'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                  <?php }
                                     ?>
                                </div>
                            </span>
                        </div>
                </div>
                </form>
                <div class="table-responsive">
                    <table class="table  table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>SKPD</th>
								<th>Sub Unit</th>
								<th>Anggaran Awal</th>
								<th>Anggaran Perubahan 1</th>
								<th>Anggaran Perubahan 2</th>
								<th width="100px">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($anggaran_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>

								<td><?php echo $rk->kd_skpd ?></td>
								<td><?php echo $rk->nm_sub_unit ?></td>

								<td align="right"><?php echo number_format($rk->anggaran_awal,'2',',','.') ?></td>
								<td align="right"><?php echo number_format($rk->anggaran_perubahan_1,'2',',','.') ?></td>
								<td align="right"><?php echo number_format($rk->anggaran_perubahan_2,'2',',','.') ?></td>
								<td align="center">
									<form action="<?= base_url().'anggaran/read' ?>">
										<input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd ?>'>
										<input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit ?>'>
										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
								</td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

					<!-- <div class="float-right">
						<?php echo $pagination ?>
					</div> -->
                    <div class="float-right">
                    <div class="btn-group">
                        <button  class="btn  page-link btn-space btn-success" disabled>  Awal : <?php echo $awal ?></button>
                    <button  class="btn  page-link btn-space btn-warning" disabled>  Perubahan 1 : <?php echo $satu ?></button>
                    <button  class="btn  page-link btn-space btn-success" disabled>  Perubahan 2 : <?php echo $dua ?></button>
                    </div>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>