<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
    				<table valign="top">
    					<tr valign="top">
    						<td width="20%">No SKPD</td>
    						<td width="1%">:</td>
    						<td><strong><?= $kd_skpd?></strong></td>
    					</tr>
                        <tr>
                            <td>Unit</td>
                            <td>:</td>
                            <td><?= $nm_unit ?></td>
                        </tr>
                        <tr>
                            <td>Sub Unit</td>
                            <td width="1%">:</td>
                            <td><?= $nm_sub_unit ?></td>
                        </tr>
    					<tr>
    						<td>No SPJ</td>
    						<td>:</td>
    						<td><?= $no_spj ?></td>
    					</tr>
    				</table>
            	</div>
            </div>
			<hr>
			
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="table1">
                        <thead>
                            <tr>
								<th>No</th>
                                <th>No SPJ</th>
                                <th>No Bukti</th>
                                <th>No Pengesahan</th>
                                <th>Keterangan</th>
                                <th>Jenis</th>
                                <th>Nilai</th>
                                <th>Kode Rekening</th>
                                <th>Rekening</th>
                                <th>uraian</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($spj_data as $rk)  { ?>
                            <tr>
                              <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                              
                              <td class="cell-detail"><?= $rk->no_spj ?><span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spj))) ?></span></td>
                              
                              <td class="cell-detail"><?= $rk->no_bukti ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_bukti))) ?></span></td>
                              
                              <td class="cell-detail"><?= $rk->no_pengesahan ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_pengesahan))) ?></span></td>
                              <td><?= $rk->keterangan ?></td>
                              <td><?= $rk->jenis_spj ?></td>
                              <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                              <td><?= $rk->kd_rek_gabung ?></td>
                              <td><?= $rk->nm_rek_5 ?></td>
                              <td><?= $rk->uraian ?></td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
				</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>