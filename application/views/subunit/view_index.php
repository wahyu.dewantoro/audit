<style type="text/css">     
  #wgtmsr{
     width:150px;   
    }

    #wgtmsr option{
      width:150px;   
    }


</style>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
                <div class="tools float-right">
                    <form action="<?php echo site_url('subunit/index'); ?>" class="form-inline" method="get">
                        <select name="nm_bidang" class="form-control form-control-sm" id="wgtmsr">
                                <option value="">Pilih Bidang</option>
                                <?php foreach($bidang as $rb){ ?>
                                <option <?php if($rb->nm_bidang==$nm_bidang){echo "selected";}?> value="<?= $rb->nm_bidang ?>"><?= $rb->nm_bidang ?></option>
                                <?php } ?>
                            </select>
                        <div class="input-group">
                            
                            <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                            <span class="input-group-btn">
                            <div class="btn-group">
                            
                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                <?php if ($nm_bidang <>'' || $q <> '')  { ?>
                                    <a href="<?php echo site_url('subunit'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php } 
                              
                                    if($akses['create']==1){
                                     echo anchor(site_url('subunit/create'),'<i class="mdi mdi-plus"></i> Tambah', 'class="btn btn-success btn-sm"');
                                    }
                                 ?>
                            </div>
                        </span>
                    </div>
                </form>
                </div>
                    <div class="table-responsive">
                    <table class="table  table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th width="100px">Kode SKPD</th>
								<th>Urusan</th>
								<th>Bidang</th>
								<th>Unit</th>
								<th>Sub Unit</th>
								
                            </tr>
                        </thead>
                        <tbody>
							<?php if($total_rows>0){foreach ($subunit_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td align="center"><?php echo $rk->Kd_SKPD ?></td>
								
								<td><?php echo $rk->Nm_Urusan ?></td>
								<td><?php echo $rk->Nm_Bidang ?></td>
								<td><?php echo $rk->Nm_Unit ?></td>
								<td><?php echo $rk->Nm_Sub_Unit ?></td>
								
							</tr>
							<?php  }  }else{ ?>
                            <tr>
                                <td colspan="6" align="center"><strong>Tidak ada data.</strong></td>
                            </tr>
                            <?php } ?>
						</tbody>
					</table>
                    </div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>