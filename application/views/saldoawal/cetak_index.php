<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Saldo Awal.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Saldo Awal</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th width="100px">Kode SKPD</th>
								<th>Sub Unit</th>
								<th>Debet</th>
								<th>Kredit</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($saldoawal_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top" ><?php echo $rk->kd_skpd ?></td>
								<td valign="top" ><?php echo $rk->nm_sub_unit ?></td>
								<td valign="top"  align="right"><?php echo number_format($rk->debet,'2',',','.'); ?></td>
								<td valign="top"  align="right"><?php echo number_format($rk->kredit,'2',',','.'); ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="3" align="right">TOTAL</th>
                                <td class="text-right"><?= $debet ?></td>
                                <td class="text-right"><?= $kredit ?></td>
                            </tr>
                        </tfoot>
					</table>