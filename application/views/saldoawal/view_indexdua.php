<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            	<div class="row">
            		<div class="col-md-4">
            			<table>
            				<tr>
            					<td>SKPD</td>
            					<td>: <strong><?= $kd_skpd ?></strong></td>
            				</tr>
            				<tr>
            					<td>Su Unit</td>
            					<td>: <strong><?= $nm_sub_unit ?></strong></td>
            				</tr>
            			</table>
            		</div>
            	 	<div class="col-md-8">

    				    <form action="<?php echo $action ?>"  method="get">
	    				    <div class="row">
	    				    	<div class="col-md-6">
	    				    		<div class="form-group">
	    				    			<label><strong>AKrual 5</strong></label>
	    				    			<select class="form-control form-control-sm select2" data-placeholder="Semua" name="akun_akrual_5">
					                    	<option value=""></option>
					                    	<?php foreach($akrual5 as $ak){?>
					                    	<option <?php if($akun_akrual_5==$ak->akun_akrual_5){echo "selected";} ?> value="<?= $ak->akun_akrual_5 ?>"><?= $ak->akun_akrual_5.' - '.$ak->nm_akrual_5 ?></option>>
					                    	<?php } ?>
					                    </select>
	    				    		</div>
	    				    	</div>
	    				    	<div class="col-md-6">
	    				    		<div class="form-group">
	    				    			<label><strong>Pencarian</strong></label>
	    				    			<div class="input-group">
				                        	<input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
											<input type='hidden' name='kd_skpd' value='<?= $kd_skpd ?>' >
											<input type='hidden' name='nm_sub_unit' value='<?= $nm_sub_unit ?>' >
				                            <span class="input-group-btn">
					                            <div class="btn-group">

					                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
					                                <?php if ($akun_akrual_5 <> '' || $q<>'')  { ?>
					                                    <a href="<?php echo $action; ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
					                              <?php }

					                                 ?>
													 <?= anchor($cetak,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success"')?>
					                            </div>
					                        </span>
				                    	</div>
	    				    		</div>
	    				    	</div>
	    				    </div>
	    				  </form>
			          </div>
            	</div>


                	<div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered ">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tahun</th>
								<th>Jurnal</th>
								<th>Tgl Bukti</th>


								<th>Keterangan</th>
								<th>Akrual 1 - 4</th>
								<th>Akrual 5</th>

								<th>Debet</th>
								<th>Kredit</th>

                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($saldoawal_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->Tahun ?></td>
								<td><?php echo $rk->Nm_Jurnal ?></td>
								<td><?php echo $rk->Tgl_Bukti ?></td>
								<td><?php echo $rk->Keterangan ?></td>
								<td class="cell-detail"><span><?php echo $rk->Akun_Akrual_1 ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Akrual_1 ?></span>
									<span><?php echo $rk->Akun_Akrual_2 ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Akrual_2 ?></span>
										<span><?php echo $rk->Akun_Akrual_3 ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Akrual_3 ?></span>
										<span><?php echo $rk->Akun_Akrual_4 ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Akrual_4 ?></span>
								</td>
								<td class="cell-detail"><span><?php echo $rk->Akun_Akrual_5 ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Akrual_5 ?></span></td>
								<td align="right"><?php echo number_format($rk->Saldo_Awal_Debet,'2',',','.') ?></td>
								<td align="right"><?php echo number_format($rk->Saldo_Awal_Kredit,'2',',','.') ?></td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo number_format($total_rows,'0','','.') ?></button>

					<div class="float-right">
						<?php echo $pagination ?>
					</div>
					<div class="float-right">
                        <div class="btn-group">
                        <button  class="btn page-link btn-space btn-success" disabled>Total Debet : <?php echo $debet ?></button>
                        <button  class="btn page-link btn-space btn-warning" disabled>Total Kredit : <?php echo $kredit ?></button>
                        </div>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>