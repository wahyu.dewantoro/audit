<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
    				<table >
                        <tr valign="top">
                            <td>Unit</td>
                            <td>:</td>
                            <td><strong><?= $nm_unit ?></strong></td>
                        </tr>
                        <tr valign="top">
                            <td>Kode SKPD</td>
                            <td>:</td>
                            <td><strong><?= $kd_skpd ?></strong></td>
                        </tr>
                        <tr valign="top">
                            <td>Sub Unit</td>
                            <td>:</td>
                            <td><strong><?= $nm_sub_unit ?></strong></td>
                        </tr>
                        <tr valign="top">
                            <td>No SPM</td>
                            <td>:</td>
                            <td><strong><?= $no_spm ?></strong></td>
                        </tr>
    				</table>
            	</div>
            </div>
			<hr>
			
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="table1">
                        <thead>
                            <tr>
								<th>No</th>
								<th>SPM</th>
                                <th>Jenis</th>
                                <th>SPP</th>
                                <th>Nilai</th>
                                <th>Rekening</th>
                                <th>Keterangan</th>
                                <th>Penerima</th>
                                <th>Bank Penerima</th>
                                

                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($spm_data as $rk)  { ?>
                            <tr>
                                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                
                                <td class="cell-detail"><?= $rk->no_spm ?>
                                    <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spm))) ?></span>
                                </td>
                                <td><?= $rk->jenis_spm ?></td>
                                <td><?= $rk->no_spp ?></td>
                                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                                
                                <td class="cell-detail">
                                    <?= $rk->kd_rek_gabung ?>
                                    <span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span>
                                </td>
                                <td><?= $rk->uraian ?></td>
                                <td><?= $rk->nm_penerima ?></td>
                                <td class="cell-detail"><?= $rk->rek_penerima ?>
                                <span class="cell-detail-description"><?= $rk->bank_penerima ?></span>
                                </td>
                                
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
				</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>