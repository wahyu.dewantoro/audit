<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=SPM.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3><?= $title ?><br>
</h3>
<table >
                        <tr valign="top">
                            <td>Unit</td>
                            <td>:</td>
                            <td><strong><?= $nm_unit ?></strong></td>
                        </tr>
                        <tr valign="top">
                            <td>Kode SKPD</td>
                            <td>:</td>
                            <td><strong><?= $kd_skpd ?></strong></td>
                        </tr>
                        <tr valign="top">
                            <td>Sub Unit</td>
                            <td>:</td>
                            <td><strong><?= $nm_sub_unit ?></strong></td>
                        </tr>
                        <tr valign="top">
                            <td>No SPM</td>
                            <td>:</td>
                            <td><strong><?= $no_spm ?></strong></td>
                        </tr>
            </table>
 
<table class="tablee" border="1">
    <thead>
        <tr>
          <th>No</th>
          <th>SPM</th>
          <th>Tanggal SPM</th>
          <th>Jenis</th>
          <th>SPP</th>
          <th>Nilai</th>
          <th>Kode Rekening</th>
          <th>Rekening</th>
          <th>Keterangan</th>
          <th>Penerima</th>
          <th>Rekening Penerima</th>
          <th>Bank Penerima</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($spm_data as $rk)  { ?>
          <tr>
            <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
            
            <td class="cell-detail"><?= $rk->no_spm ?></td>
            <td><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spm))) ?></td>
            <td><?= $rk->jenis_spm ?></td>
            <td><?= $rk->no_spp ?></td>
            <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
            <td class="cell-detail">
            <?= $rk->kd_rek_gabung ?>
            </td>
            <td><?= $rk->nm_rek_5 ?></td>
            <td><?= $rk->uraian ?></td>
            <td><?= $rk->nm_penerima ?></td>
            <td class="cell-detail"><?= $rk->rek_penerima ?>
            </td>
            <td><?= $rk->bank_penerima ?></td>
          </tr>
      <?php  }   ?>
  </tbody>
</table>