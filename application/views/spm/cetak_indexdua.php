<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=SPM.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3><?= $title ?><br>
SKPD :<?= $nm_unit?>
</h3>
 
<table class="tablee" border="1">
  <thead>
    <tr>
        <th rowspan="2" >No</th>
        <th rowspan="2">Kode</th>
        <th rowspan="2">Sub Unit</th>
        <th rowspan="2">Rekening</th>
        <th colspan="3">SPM</th>
    </tr>
    <tr>
        
        <th>Jenis</th>
        <th>Jumlah</th>
        <th>Nilai</th>
                    </tr>
                </thead>
                <tbody>
      <?php foreach ($spm_data as $rk)  { ?>
                    <tr>
        <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
        <td><?= $rk->kd_skpd ?> </td>
        <td><?= $rk->nm_sub_unit ?> </td>
        <td align="center"><?= $rk->rekening ?> </td>
        <td align="center"><?= $rk->jenis_spm ?> </td>
        <td align="center"><?= $rk->jumlah ?> </td>
        <td align="right"><?= number_format($rk->nilai,'0','','.') ?> </td>
         
      </tr>
      <?php  }   ?>
    </tbody>
</table>