<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Perintah Pembayaran (SPM).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Perintah Pembayaran (SPM)</h3>
    <table class="tablee" border="1">
            <thead>
              <tr> 
                    <th>No</th>
                    <th>No SKPD</th>
                    <th>Unit    </th>
                    <th>Sub Unit</th>
                    <th> SPM</th>
                    <th>Tanggal SPM</th>
                    <th>keterangan </th>
                    <th>Jenis  </th>
                    <th>Jumlah</th>
                    <th>Nilai</th>
                    
              </tr>
            </thead>
            <tbody>
               
               <?php foreach ($spm_data as $rk)  { ?>
                <tr>
                    <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                    <td><?= $rk->kd_skpd ?></td>
                    <td><?= $rk->nm_unit ?></td>
                    <td><?= $rk->nm_sub_unit ?></td>
                    <td class="cell-detail"><?= $rk->no_spm ?></td>
                    <td><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spm))) ?></td>
                    <td><?= $rk->keterangan ?></td>
                    <td><?= $rk->jenis_spm ?></td>
                    <td align="center"><?= number_format($rk->jumlah,'0','','.') ?></td>
                    <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                     
                </tr>
                <?php  }   ?>
            </tbody>
		</table>