<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <form action="<?php echo site_url('pajak/index'); ?>"  method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control form-control-sm select2" name="q">
                                <option value="">Pilih SKPD/OPD</option>
                                <?php foreach($subunit as $su){?>
                                <option <?php if($su->nm_sub_unit==$q){echo "selected";}?> value="<?= $su->nm_sub_unit ?>"><?= $su->nm_sub_unit ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">

                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('pajak'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php }

                                    if($akses['create']==1){
                                     echo anchor(site_url('pajak/create'),'<i class="mdi mdi-plus"></i> Tambah', 'class="btn btn-success btn-sm"');
                                    }
                                 ?>
                            </div>
                        </div>
                    </div>
                </form>
                
                	<div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>SKPD</th>
								<th>Sub Unit</th>
								<th>Nilai</th>
								<th width="10px">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($pajak_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->kd_skpd ?></td>
								<td><?php echo $rk->nm_sub_unit ?></td>
								<td align="right"><?php echo number_format($rk->nilai,'2',',','.') ?></td>
								<td align="center">
                                    <form action="<?= base_url().'pajak/read'?>">
                                        <input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd ?>'>
                                        <input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit ?>'>
                                        <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
                                    </form>
                                </td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

					<!-- <div class="float-right">
						<?php echo $pagination ?>
					</div> -->
                    <div class="float-right">
                        <button  class="btn  btn-space btn-success page-link" disabled>Total Nilai : <?php echo $nilai ?></button>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>