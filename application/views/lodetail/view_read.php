<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
<table class="table">
	    <tr><td>Tahun</td><td><?php echo $Tahun; ?></td></tr>
	    <tr><td>Kd Jurnal</td><td><?php echo $Kd_Jurnal; ?></td></tr>
	    <tr><td>Nm Jurnal</td><td><?php echo $Nm_Jurnal; ?></td></tr>
	    <tr><td>Tgl Bukti</td><td><?php echo $Tgl_Bukti; ?></td></tr>
	    <tr><td>No Bukti</td><td><?php echo $No_Bukti; ?></td></tr>
	    <tr><td>No BKU</td><td><?php echo $No_BKU; ?></td></tr>
	    <tr><td>Kd SKPD</td><td><?php echo $Kd_SKPD; ?></td></tr>
	    <tr><td>Nm Unit</td><td><?php echo $Nm_Unit; ?></td></tr>
	    <tr><td>Nm Sub Unit</td><td><?php echo $Nm_Sub_Unit; ?></td></tr>
	    <tr><td>ID Prog</td><td><?php echo $ID_Prog; ?></td></tr>
	    <tr><td>Ket Program</td><td><?php echo $Ket_Program; ?></td></tr>
	    <tr><td>Kd Keg</td><td><?php echo $Kd_Keg; ?></td></tr>
	    <tr><td>Ket Kegiatan</td><td><?php echo $Ket_Kegiatan; ?></td></tr>
	    <tr><td>Keterangan</td><td><?php echo $Keterangan; ?></td></tr>
	    <tr><td>Akun Akrual 1</td><td><?php echo $Akun_Akrual_1; ?></td></tr>
	    <tr><td>Akun Akrual 2</td><td><?php echo $Akun_Akrual_2; ?></td></tr>
	    <tr><td>Akun Akrual 3</td><td><?php echo $Akun_Akrual_3; ?></td></tr>
	    <tr><td>Akun Akrual 4</td><td><?php echo $Akun_Akrual_4; ?></td></tr>
	    <tr><td>Akun Akrual 5</td><td><?php echo $Akun_Akrual_5; ?></td></tr>
	    <tr><td>Nm Akrual 1</td><td><?php echo $Nm_Akrual_1; ?></td></tr>
	    <tr><td>Nm Akrual 2</td><td><?php echo $Nm_Akrual_2; ?></td></tr>
	    <tr><td>Nm Akrual 3</td><td><?php echo $Nm_Akrual_3; ?></td></tr>
	    <tr><td>Nm Akrual 4</td><td><?php echo $Nm_Akrual_4; ?></td></tr>
	    <tr><td>Nm Akrual 5</td><td><?php echo $Nm_Akrual_5; ?></td></tr>
	    <tr><td>Debet</td><td><?php echo $Debet; ?></td></tr>
	    <tr><td>Kredit</td><td><?php echo $Kredit; ?></td></tr>
	</table>
                </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>