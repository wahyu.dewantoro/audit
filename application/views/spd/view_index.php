<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <form action="<?php echo site_url('spd/index'); ?>" method="get">
                <div class="row">
                    <div class="col-md-6">
                        <select class="form-control select2 form-control-sm" name="q">
                            <option value="">Pilih SKPD/OPD </option>
                            <?php foreach($subunit as $su){?>
                            <option <?php if($su->nm_sub_unit==$q){echo "selected";}?> value="<?= $su->nm_sub_unit ?>"><?= $su->nm_sub_unit ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                        <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('spd'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php }?>
                    </div>
                
                </div>
                </form>
                	<div class="table-responsive noSwipe">
                    <table class="table table-striped table-hover table-bordered"  >
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Kode SKPD</th>
								<th>Sub Unit</th>
                                <th>Jumlah</th>
								<th>Nilai</th>
								<th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>

							<?php

							foreach ($spd_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td><?php echo $rk->Kd_SKPD ?></td>

								<td><?php echo $rk->Nm_Sub_Unit ?></td>
                                <td align="center"><?php echo $rk->jumlah ?></td>
								<td align="right"><?php echo number_format($rk->nilai,'2',',','.') ?></td>
								<td align="center">
									<form action="<?= base_url().'spd/read' ?>">
										<input type="hidden" name="Kd_SKPD" value="<?= $rk->Kd_SKPD ?>">
										<input type="hidden" name="Nm_Sub_Unit" value="<?= $rk->Nm_Sub_Unit ?>">

										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
								</td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo number_format($total_rows,'0','','.' )?></button>
					<!-- <div class="float-right">
						<?php echo $pagination ?>
					</div> -->
					<div class="float-right">
                        <button  class="btn page-link btn-space btn-success" disabled>Total Nilai : <?php echo $jum_nilai ?></button>
					</div>
            </div>
        </div><!-- end card-->
    </div>
</div>