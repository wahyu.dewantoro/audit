<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Penyediaan Dana (SPD).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Penyediaan Dana (SPD)</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tahun</th>
								<th>No SPD</th>
                                <th>Tanggal</th>
								<th>Program</th>
								<th>Kegiatan</th>
								<th>Keterangan</th>
								<th>No Rekening</th>
                                <th>Nama Rekening</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($spd_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top" ><?php echo $rk->Tahun ?></td>
								<td valign="top" ><?php echo $rk->No_SPD ?></td>
								<td valign="top" style='mso-number-format:"\@"'><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_SPD))) ?></td>
								<td valign="top" ><?php echo $rk->Ket_Program ?></td>
								<td valign="top" ><?php echo $rk->Ket_Kegiatan ?></td>
								<td valign="top" ><?php echo $rk->Uraian ?></td>
							 	<td valign="top" ><?= $rk->Kd_Rek_Gabung?></td>
							 	<td valign="top" ><?= $rk->Nm_Rek_5?></td>
							 	<td valign="top"  align="right"><?php echo number_format($rk->Nilai,'2',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="9" align="right">TOTAL</th>
                                <td class="text-right"><?php echo $nilai ?></td>
                            </tr>
                        </tfoot>
					</table>