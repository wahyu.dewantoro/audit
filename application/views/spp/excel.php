<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Permintaan Pembayaran (SPP).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Permintaan Pembayaran (SPP)</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px" >No</th>
								<th >SPP</th>
                                <th>Tanggal</th>
								<th >Program</th>
								<th >Kegiatan</th>
								<th >Uraian</th>
								<th >No Rekening</th>
								<th >Nama Rekening</th>
								<th >Penerima</th>
								<th >NPWP</th>
								<th >SPD</th>
								<th >Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($spp_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td valign="top" width="200px"><?php echo $rk->No_SPP; ?></td>
                                <td valign="top" style='mso-number-format:"\@"'><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_SPP))) ?></td>
                                <td valign="top" ><?= $rk->Ket_Program;?></td>
                                <td valign="top" ><?= $rk->Ket_Kegiatan ?></td>
                                <td valign="top" ><?=  $rk->Uraian?></td>
                                <td valign="top" ><?= $rk->Kd_Rek_Gabung  ?></td>
                                <td valign="top" ><?php echo $rk->Nm_Rek ?></td>
                                <td valign="top" ><?= $rk->Rek_Penerima ?>a/n <?= $rk->Nm_Penerima ?><br><span class="cell-detail-description"><?= $rk->Bank_Penerima?></span></td>
                                <td valign="top" ><?=  $rk->NPWP?></td>
                                <td valign="top" ><?=  $rk->No_SPD?></td>
                                <td valign="top" class="text-right"><?php echo number_format($rk->Nilai,'2',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="11" align="right">TOTAL NILAI</th>
                                <td class="text-right"><?= $nilai ?></td>
                            </tr>
                        </tfoot>
					</table>