<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-3">
    				<table >
    					<tr valign="top">
    						<td>SKPD</td>
    						<td>:</td>
    						<td> <strong><?= $nm_unit?></strong></td>
    					</tr>
    					
    				</table>
            	</div>
            </div>
			<hr>
			
                <div class="table-responsive">
                    <table class="table   table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2" width="10px">No</th>
								<th rowspan="2">Kode</th>
								<th rowspan="2">Sub Unit</th>
								<th rowspan="2">Rekening</th>
								<th colspan="3">spp</th>
								<th rowspan="2"></th>
                            </tr>
                            <tr>
								
								<th>Jenis</th>
								<th>Jumlah</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($spp_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td><?= $rk->kd_skpd ?> </td>
								<td><?= $rk->nm_sub_unit ?> </td>
								<td align="center"><?= $rk->rekening ?> </td>
								<td align="center"><?= $rk->jenis_spp ?> </td>
								<td align="center"><?= $rk->jumlah ?> </td>
								<td align="right"><?= number_format($rk->nilai,'0','','.') ?> </td>
								<th align="center">
									<form action="<?= base_url().'spp/readdua'?>">
										<input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd?>'>
										<input type='hidden' name='nm_unit' value='<?= $nm_unit ?>'>
										<input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit?>'>
										<input type='hidden' name='rekening' value='<?= $rk->rekening?>'>
										<input type='hidden' name='jenis_spp' value='<?= $rk->jenis_spp?>'>
										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
								</th>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
				</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>