<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                
                
                    <form action="<?php echo site_url('spp'); ?>"   method="get">
                        <div class="row">
                            
                            <div class="col-md-4">
                                    <select class="form-control form-control-sm select2" name="q" data-placeholder="Pilih SKPD/OPD">
                                        <option value=""></option>
                                        <?php foreach($subunit as $su){?>
                                        <option <?php if($su->nm_unit==$q){echo "selected";}?> value="<?= $su->nm_unit ?>"><?= $su->nm_unit ?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                            <div class="col-md-2">
                                    <span class="input-group-btn">
                                    <div class="btn-group">

                                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                        <?php if ($q <> '')  { ?>
                                            <a href="<?php echo site_url('spp'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                      <?php }
                                      ?>
                                        
                                    </div>
                                </span>
                            </div>
                        </div>
                    </form>
               
                
                
                <div class="table-responsive">
                    <table class="table  table-striped table-hover table-bordered" id="table2">
                        <thead>
                          <tr> 
                                <th>No</th>
                                <th>No SKPD</th>
                                <th>Unit    </th>
                                <th>Sub Unit</th>
                                <th> SPP</th>
                                <th>keterangan </th>
                                <th>Jenis  </th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th></th>
                          </tr>
                        </thead>
                        <tbody>
                           
                           <?php foreach ($spp_data as $rk)  { ?>
                            <tr>
                                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td><?= $rk->kd_skpd ?></td>
                                <td><?= $rk->nm_unit ?></td>
                                <td><?= $rk->nm_sub_unit ?></td>
                                <td class="cell-detail"><?= $rk->no_spp ?><span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spp))) ?></span></td>
                                <td><?= $rk->keterangan ?></td>
                                <td><?= $rk->jenis_spp ?></td>
                                <td align="center"><?= number_format($rk->jumlah,'0','','.') ?></td>
                                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                                <td align="center">
                                    <form action="<?= base_url().'spp/readdua'?>">
                                        <input type="hidden" name="kd_skpd" value="<?= $rk->kd_skpd ?>">
                                        <input type="hidden" name="nm_unit" value="<?= $rk->nm_unit ?>">
                                        <input type="hidden" name="nm_sub_unit" value="<?= $rk->nm_sub_unit ?>">
                                        <input type="hidden" name="no_spp"  value="<?= $rk->no_spp?>">
                                       <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
                                    </form>
                                </td>
                            </tr>
                            <?php  }   ?>
                        </tbody>
                         
                    </table>
                </div>                  
                <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

                    <div class="float-right">
                        <?php echo $pagination ?>
                    </div>                 
            </div>
        </div><!-- end card-->
    </div>
</div>