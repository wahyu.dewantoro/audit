<div class="row">
    <div class="col-md-12">
        <div class="card card-border">
            <div class="card">
                <div class="card-body ">
                     <div class="tools float-right">
                            <form action="<?php echo site_url('menu'); ?>" class="form-inline" method="get">
                                <div class="input-group">
                                    <input type="text" placeholder="Pencarian" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>">
                                    <span class="input-group-btn">
                                        <div class="btn-group">
                                        <?php 
                                            if ($q <> '')
                                            {
                                                ?>
                                                <a href="<?php echo site_url('menu'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                                <?php
                                            }
                                        ?>
                                        
                                            <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Search</button>
                                            <?php 
                                                if($akses['create']==1){
                                                    echo anchor(site_url('menu/create'),'<i class="mdi mdi-plus"></i> Create', 'class="btn btn-success btn-sm"');    
                                                }
                                                
                                             ?>
                                        </div>
                                    </span>
                                </div>
                            </form>
                        </div>
                    <!-- body -->
                      <table class="table  table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>Nama Menu</th>
                                <th>Link Menu</th>
                                <th>Parent</th>
                                <th>Sort</th>
                                <th width="5%">Icon</th>
                                <th width="20px"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php  
                            if($total_rows>0 ){

                            foreach ($menu_data as $rk) { ?>
                            <tr>
                                <td  align="center"><?php echo ++$start ?></td>
                                <td><?php echo $rk->nama_menu ?></td>
                                <td><?php echo $rk->link_menu ?></td>
                                <td><?php echo $rk->parent ?></td>
                                <td><?php echo $rk->sort ?></td>
                                <td align="center"><span class="<?php echo $rk->icon ?>"></span> </td>
                                <td style="text-align:center" ><div class="btn-group">
                                    <?php 
                                    echo anchor(site_url('menu/read/'.acak($rk->id_inc)),'<i class="mdi mdi-search"></i>','class="btn btn-xs btn-primary"'); 
                                    
                                    if($akses['update']==1){
                                        echo anchor(site_url('menu/update/'.acak($rk->id_inc)),'<i class="mdi mdi-edit"></i>','class="btn btn-xs btn-success"');
                                    }

                                    if($akses['delete']==1){
                                        echo anchor(site_url('menu/delete/'.acak($rk->id_inc)),'<i class="mdi mdi-delete"></i>','onclick="javasciprt: return confirm(\'Are You Sure ?\')" class="btn btn-xs btn-danger"'); 
                                    }
                                    ?>
                                </div>
                                </td>
                            </tr>
                        <?php }  } else { ?>
                            <tr>
                            <td colspan="7"> Tidak ada data.</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <button class="btn btn-space btn-secondary"><span> Record : <?php echo $total_rows ?></span></button>
                    <div class="float-right">
                        
                        <?php echo $pagination ?>
                        
                    </div>
                    
                    <!-- <span>Record : <?php echo $total_rows ?></span> -->
                </div>
            </div>
        </div>
    </div>
</div>

