<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
    				<table >
    					<tr valign="top">
                            <td width="20%">Kode Unit</td>
                            <td>:</td>
                            <td><?= $kode_unit ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">Kode SKPD</td>
                            <td>:</td>
                            <td><?= $kd_skpd ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">Unit</td>
                            <td>:</td>
                            <td><?= $nm_unit ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">Sub Unit</td>
                            <td>:</td>
                            <td><?= $nm_sub_unit ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">No SP2D</td>
                            <td>:</td>
                            <td><?= $no_sp2d ?></td>
                        </tr>
    				</table>
            	</div>
            </div>
			<hr>
			
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="table1">
                        <thead>
                            <tr>
								<th>No</th>
								<th>No SP2D</th>
								<th>Rekening</th>
								<th>Nilai</th>
								<th>Program</th>
								<th>Kegiatan</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($sppd_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td class="cell-detail"><?= $rk->no_sp2d ?><span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_sp2d))) ?></span></td>
								<td class="cell-detail"><?= $rk->kd_rek_gabung ?><span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span></td>
								<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
								
								
								<td><?= $rk->ket_program ?></td>
								<td><?= $rk->ket_kegiatan ?></td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
				</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>