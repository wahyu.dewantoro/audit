<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo $action ?>" method="get">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label><strong>Pencarian</strong></label>
                                     <div class="input-group">
                                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                                        <span class="input-group-btn">
                                        <div class="btn-group">
                                            <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                            <?php if ($q <> '')  { ?>
                                                <a href="<?php echo $action ; ?>" class="btn btn-warning"><i class="mdi mdi-close"></i></a>
                                              <?php } ?>
                                              

                                        </div>
                                    </span>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                <div class="table-responsive">
                    <table class="table   table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>Kode SKPD</th>
                                <th>Sub Unit</th>
                                <th>SPM</th>
                                <th>SP2D</th>
                                <th>Program</th>
                                <th>Kegiatan</th>
                                <th>Rekening</th>
                                <th>Penerima</th>
                                <th>NPWP</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($sppd_data as $rk)  { ?>
                            <tr>

                                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td><?= $rk->Kd_SKPD?></td>
                                <td><?= $rk->Nm_Sub_Unit ?></td>
                                <td  class="cell-detail"><span><?php echo $rk->No_SPM ?></span>
                                    <span class="cell-detail-description"><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_SPM))) ?></span>
                                </td>
                                <td  class="cell-detail"><span><?php echo $rk->No_SP2D ?></span>
                                    <span class="cell-detail-description"><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_SP2D))) ?></span>
                                </td>
                                <!-- <td><?= $rk->Jenis_SP2D?></td> -->
                                <!-- <td><?= $rk->Kd_SKPD ?></td>
                                <td class="cell-detail"><span><?php echo $rk->Nm_Unit ?></span>
                                    <span class="cell-detail-description"><?php echo $rk->Nm_Sub_Unit ?></span>
                                </td> -->
                                <td><?= $rk->Ket_Program?></td>
                                <td><?= $rk->Ket_Kegiatan?></td>

                                <td class="cell-detail"><span><?php echo $rk->Kd_Rek_Gabung ?></span>
                                    <span class="cell-detail-description"><?php echo $rk->Nm_Rek_5 ?></span>
                                </td>
                                <td class="cell-detail">
                                        <span><?= $rk->Rek_Penerima ?>  a/n <?= $rk->Nm_Penerima ?></span>
                                        <span class="cell-detail-description"><?= $rk->Bank_Penerima?></span>
                                </td>
                                <td><?= $rk->NPWP ?></td>

                                <td align="right"><?php echo number_format($rk->Nilai,'2',',','.') ?></td>


                            </tr>
                            <?php  }   ?>
                        </tbody>
                    </table>
                </div>
                    <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo number_format($total_rows,'0','','.'); ?></button>
                    <div class="float-right">
                        <?php echo $pagination ?>
                    </div>
                    <div class="float-right">
                        <button  class="btn page-link  btn-space btn-success" disabled>Total Nilai : <?php echo $nilai ?></button>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>