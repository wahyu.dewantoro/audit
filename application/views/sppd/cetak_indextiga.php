<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<table valign="top">
	<tr valign="top">
                            <td width="20%">Kode Unit</td>
                            <td>:</td>
                            <td><?= $kode_unit ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">Kode SKPD</td>
                            <td>:</td>
                            <td><?= $kd_skpd ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">Unit</td>
                            <td>:</td>
                            <td><?= $nm_unit ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">Sub Unit</td>
                            <td>:</td>
                            <td><?= $nm_sub_unit ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%">No SP2D</td>
                            <td>:</td>
                            <td><?= $no_sp2d ?></td>
                        </tr>
</table>

<table class="table   table-striped table-hover table-bordered" border="1">
    <thead>
        <tr>
			<th>No</th>
			<th>Tanggal</th>
			<th>No SP2D</th>
			<th>Kode</th>
			<th>Rekening</th>
			<th>Nilai</th>
			<th>Program</th>
			<th>Kegiatan</th>
        </tr>
    </thead>
    <tbody>
		<?php foreach ($sppd_data as $rk)  { ?>
        <tr>
			<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
			<td><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_sp2d))) ?></td>
			<td><?= $rk->no_sp2d ?></td>
			<td><?= $rk->kd_rek_gabung ?></td>
			<td><?= $rk->nm_rek_5 ?></td>
			
			<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
			<td><?= $rk->ket_program ?></td>
			<td><?= $rk->ket_kegiatan ?></td>
		</tr>
		<?php  }   ?>
	</tbody>
</table>