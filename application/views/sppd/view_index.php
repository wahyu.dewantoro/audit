<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                
                
                    <form action="<?php echo site_url('sppd/index'); ?>"   method="get">
                        <div class="row">
                            
                            <div class="col-md-4">
                                    <select class="form-control form-control-sm select2" name="q" data-placeholder="Pilih SKPD/OPD">
                                        <option value=""></option>
                                        <?php foreach($subunit as $su){?>
                                        <option <?php if($su->nm_unit==$q){echo "selected";}?> value="<?= $su->nm_unit ?>"><?= $su->nm_unit ?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                            <div class="col-md-2">
                                    <span class="input-group-btn">
                                    <div class="btn-group">

                                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                        <?php if ($q <> '')  { ?>
                                            <a href="<?php echo site_url('sppd'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                      <?php }
                                      ?>
                                        
                                    </div>
                                </span>
                            </div>
                        </div>
                    </form>
               
                
                
                <div class="table-responsive">
                    <table class="table  table-striped table-hover table-bordered">
                        <thead>
                            <th>No</th>
                            <th>Unit</th>
                            <th>Sub Unit</th>
                            <th>No SP2D</th>
                            <th>Keterangan</th>
                            <th>Jenis</th>
                            <th>Jumlah</th>
                            <th>Nilai</th>
                            <th>Penerima</th>
                            <th>Rek Penerima</th>
                            <th></th>
                        </thead>
                        <tbody>
							<?php  foreach ($sppd_data as $rk)  {  ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td class="cell-detail"><?= $rk->kode_unit ?>
                                    <span class="cell-detail-description"><?= $rk->nm_unit ?></span>
                                </td>
                                <td class="cell-detail"><?= $rk->kd_skpd ?>
                                    <span class="cell-detail-description"><?= $rk->nm_sub_unit ?></span>
                                </td>
                                <td class="cell-detail"><?= $rk->no_sp2d ?> <span class='cell-detail-description'><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sp2d))) ?></span></td>
                                <td><?= $rk->keterangan ?></td>
                                <td><?= $rk->jenis_sp2d ?></td>
                                <td><?= $rk->jumlah ?></td>
                                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                                <td><?= $rk->nm_penerima ?></td>
                                <td class='cell-detail'><?= $rk->rek_penerima ?> <span class="cell-detail-description"><?= $rk->bank_penerima ?> </span></td>
                                
								<td align="center">
									<form action="<?= base_url().'sppd/readdua'?>">
									   <input type="hidden" name="kode_unit" value="<?= $rk->kode_unit ?>">
                                       <input type="hidden" name="kd_skpd" value="<?= $rk->kd_skpd ?>">
                                       <input type='hidden' name='nm_unit' value='<?= $rk->nm_unit ?>' >
                                       <input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit ?>' >
                                       <input type='hidden' name='no_sp2d' value='<?= $rk->no_sp2d ?>' >

									   <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
								</td>
							</tr>
							<?php  }   ?>
						</tbody>
                         
					</table>
				</div>					
                <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

                    <div class="float-right">
                        <?php echo $pagination ?>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>