<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Perintah Pencairan Dana (SP2D)</h3>
<table border="1">
                        <thead>
                            <tr>
                                <th rowspan="2" width="10px">No</th>
                                <th rowspan="2">SKPD</th>
                                <th colspan="2">UP</th>
                                <th colspan="2">GU</th>
                                <th colspan="2">TU</th>
                                <th colspan="2">LS</th>
                                <th colspan="2">NIHIL</th>
                            </tr>
                            <tr>
                                
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
              <?php foreach ($sppd_data as $rk)  { ?>
                            <tr>
                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td><?= $rk->nm_unit ?></td>
                                <td align="center"><?= $rk->jnsup ?></td>
                                <td align="right"><?= number_format($rk->nilaiup,'0','','.') ?></td>
                                <td align="center"><?= $rk->jnsgu ?></td>
                                <td align="right"><?= number_format($rk->nilaigu,'0','','.') ?></td>
                                <td align="center"><?= $rk->jnstu ?></td>
                                <td align="right"><?= number_format($rk->nilaitu,'0','','.') ?></td>
                                <td align="center"><?= $rk->jnsls ?></td>
                                <td align="right"><?= number_format($rk->nilails,'0','','.') ?></td>
                                <td align="center"><?= $rk->jnsnihil ?></td>
                                <td align="right"><?= number_format($rk->nilainihil,'0','','.') ?></td>
              </tr>
              <?php  }   ?>
            </tbody>
          </table>
