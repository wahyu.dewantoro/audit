<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<table valign="top">
	<tr>
		<td colspan="2">Surat Perintah Pencairan Dana (SP2D)</td>
	</tr>
	<tr>
		<td>SKPD</td>
		<td>: <strong><?= $nm_unit?></strong></td>
	</tr>    					
</table>

<table class="table   table-striped table-hover table-bordered" border="1">
    <thead>
        <tr>
            <th rowspan="2" width="10px">No</th>
			<th rowspan="2">Kode</th>
			<th rowspan="2">Sub Unit</th>
			<th rowspan="2">Rekening</th>
			<th colspan="3">SP2D</th>
			
        </tr>
        <tr>
			
			<th>Jenis</th>
			<th>Jumlah</th>
			<th>Nilai</th>
        </tr>
    </thead>
    <tbody>
		<?php foreach ($sppd_data as $rk)  { ?>
        <tr>
			<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
			<td><?= $rk->kd_skpd ?> </td>
			<td><?= $rk->nm_sub_unit ?> </td>
			<td align="center"><?= $rk->rekening ?> </td>
			<td align="center"><?= $rk->jenis_sp2d ?> </td>
			<td align="center"><?= $rk->jumlah ?> </td>
			<td align="right"><?= number_format($rk->nilai,'0','','.') ?> </td>
		</tr>
		<?php  }   ?>
	</tbody>
</table>