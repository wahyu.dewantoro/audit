<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div class="card">
            <div class="card-body">
                <form action="<?php echo $action ?>" class="form-inline" method="get">
                        <div class="col-md-2">
                            <select class="form-control select2 form-control-xs" name="jenis">
                                <?php $dd=array('TU','GU'); foreach($dd as $dd ){?>
                                <option <?php if($dd==$jenis){echo "selected";}?> value="<?= $dd ?>"><?= $dd ?></option>
                                <?php } ?>
                            </select>    
                        </div>
                        <div class="col-md-6">
                            <select class="form-control form-control-sm select2" name="q" data-placeholder='Silahkan pilih SKPD/OPD'>
                                <option value=""></option>
                                <?php foreach($subunit as $su){?>
                                <option <?php if($su->kd_skpd==$q){echo "selected";}?> value="<?= $su->kd_skpd ?>"><?= $su->nm_sub_unit ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                            <?php if ($q <> '')  { ?>
                                <a href="<?php echo $action ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                          <?php }
                          ?>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <?php if($q<>''){ ?>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
         <div class="card">
            <div class="card-body">
                <form action="<?php echo base_url().'sppd/prosesmappingspj'?>" method="post">
                    <input type="hidden" name="sppd" id="sppd">
                    <div id="res"></div>
                    <button class="btn  btn-primary"><i class="mdi mdi-cloud-done"></i> Submit</button>
                </form>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
    <?php if($q<>''){ ?>
<div class="row">
    <div class="col-md-6">
        <div class="card card-contrast">
                <div class="card-header card-header-contrast card-header-featured">SP2D</div>
                <div class="card-body">
                    <div class="table-responsive">
                             <table class="table table-striped table-hover table-bordered">
                                 <thead>
                                    <th>Jenis</th>
                                     <th>Rekening</th>
                                     <th>Nomor</th>
                                     <th>nilai</th>
                                     <th></th>
                                 </thead>
                                 <tbody>
                                 <?php foreach($sppd as $rn){?>
                                 <tr>
                                     <td><?= $rn->jenis_sp2d ?></td>
                                     <td class="cell-detail"><span><?= $rn->kd_rek_gabung ?></span><span class="cell-detail-description"><?= $rn->nm_rek_5 ?></span></td>
                                     <td class="cell-detail"><span><?= $rn->no_sp2d ?></span>
                                     <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rn->tgl_sp2d))) ?></span>
                                     </td>
                                     <td align="right"><?= number_format($rn->nilai,'2',',','.') ?></td>
                                     <td>
                                        <label class="custom-control custom-radio">
                                             <input class="custom-control-input satu" type="radio" value="<?= $rn->tahun.'#'. $rn->kode_unit.'#'. $rn->kd_skpd.'#'. $rn->nm_unit.'#'. $rn->nm_sub_unit.'#'. $rn->id_prog.'#'. $rn->ket_program.'#'. $rn->kd_keg.'#'. $rn->ket_kegiatan.'#'. $rn->tgl_sp2d.'#'. $rn->no_sp2d.'#'. $rn->keterangan.'#'. $rn->jenis_sp2d.'#'. $rn->nilai.'#'. $rn->kd_rek_1.'#'. $rn->kd_rek_2.'#'. $rn->kd_rek_3.'#'. $rn->kd_rek_4.'#'. $rn->kd_rek_5.'#'. $rn->kd_rek_gabung.'#'. $rn->nm_rek_5.'#'. $rn->nm_penerima.'#'. $rn->rek_penerima.'#'. $rn->bank_penerima.'#'. $rn->npwp.'#'. $rn->tgl_spm.'#'. $rn->no_spm.'#'. $rn->id_bukti ?>" name="sp2d"><span class="custom-control-label"></span>
                                        </label>
                                    </td>
                                 </tr>
                                 <?php } ?>
                                 </tbody>
                             </table>
                         </div>
                </div>
                
              </div>
    </div>
    <div class="col-md-6">
        <div class="card card-contrast">
                <div class="card-header card-header-contrast card-header-featured">SPJ</div>
                <div class="card-body">
                    <div class="table-responsive">
                             <table class="table table-striped table-hover table-bordered">
                                 <thead>
                                     <th></th>
                                     <th>Jenis</th>
                                     <th>Rekening</th>
                                     <th>Nomor</th>
                                     <th>nilai</th>
                                 </thead>
                                 <tbody>
                                 <?php foreach($spj as $rk){?>
                                 <tr>
                                    <td>
                                        <label class="custom-control custom-checkbox">
                                             <input class="custom-control-input dua" type="checkbox" name="spj[]" value="<?= $rk->tahun.'#'. $rk->kd_skpd.'#'. $rk->nm_unit.'#'. $rk->nm_sub_unit.'#'. $rk->id_prog.'#'. $rk->ket_program.'#'. $rk->kd_keg.'#'. $rk->ket_kegiatan.'#'. $rk->tgl_spj.'#'. $rk->no_spj.'#'. $rk->tgl_bukti.'#'. $rk->no_bukti.'#'. $rk->tgl_pengesahan.'#'. $rk->no_pengesahan.'#'. $rk->keterangan.'#'. $rk->jenis_spj.'#'. $rk->nilai.'#'. $rk->kd_rek_1.'#'. $rk->kd_rek_2.'#'. $rk->kd_rek_3.'#'. $rk->kd_rek_4.'#'. $rk->kd_rek_5.'#'. $rk->kd_rek_gabung.'#'. $rk->nm_rek_5.'#'. $rk->uraian.'#'. $rk->id_bukti ?>"><span class="custom-control-label"></span>
                                        </label>
                                    </td>
                                     <td><?= $rk->jenis_spj ?></td>
                                     <td class="cell-detail"><span><?= $rk->kd_rek_gabung ?></span><span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span></td>
                                     <td class="cell-detail"><span><?= $rk->no_spj ?></span>
                                     <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spj))) ?></span>
                                     </td>
                                     <td align="right"><?= number_format($rk->nilai,'2',',','.') ?></td>
                                 </tr>
                                 <?php } ?>
                                 </tbody>
                             </table>
                         </div>
                </div>
                
              </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast card-header-featured">SP2D VS SPJ</div>
                <div class="card-body">
                    <div class="table-responsive">
                             <table class="table table-striped  table-bordered">
                                 <thead>
                                       <th>SP2D</th>
                                       <th>Rekening</th>
                                       <th>Nilai SP2D</th>
                                       <th>SPJ</th>
                                       <th>Rekening</th>
                                       <th>Nilai</th>
                                       
                                       <th></th>
                                 </thead>
                                 <tbody>
                                 <?php
                                 $awal=0;
                                 $akhir=0;
                                   foreach($mapping as $rkk){
                                    
                                    $start_date = new DateTime(date('Y-m-d',strtotime($rkk->tgl_sp2d)));
                                    $end_date   = new DateTime(date('Y-m-d',strtotime($rkk->tgl_spj)));
                                    $interval   = $start_date->diff($end_date);
                                    $awal=$rkk->sp2d_nilai;
                                    $akhir+=$rkk->spj_nilai;
                                    ?>
                                 <tr <?php if($interval->days > 30){ ?> class="table-warning"  <?php } ?> >
                                       
                                       <td class="cell-detail"><?= $rkk->no_sp2d?><span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rkk->tgl_sp2d)))?></span></td>
                                       <td class="cell-detail"><?= $rkk->sp2d_kd_rek_gabung?><span class="cell-detail-description"><?= $rkk->sp2d_nm_rek_5?></span></td>
                                       <td align="right"><?= number_format($rkk->sp2d_nilai,'2',',','.') ?></td>
                                       
                                       <td class="cell-detail"><?= $rkk->no_spj?><span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rkk->tgl_spj)))?></span></td>
                                       <td class="cell-detail"><?= $rkk->spj_kd_rek_gabung?><span class="cell-detail-description"><?= $rkk->spj_nm_rek_5?></span></td>
                                       <td align="right"><?= number_format($rkk->spj_nilai,'2',',','.') ?></td>
                                       
                                       <th><?= anchor('sppd/hapusmaping?p='.$rkk->id,'<i class="mdi mdi-delete"></i>','class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"'); 
                                       
                                       ?></th>
                                 </tr>
                                 <?php } ?>
                                 </tbody>
                                 <tfoot>
                                     <tr>
                                         <td colspan="2"><strong>Jumlah SP2D</strong></td>
                                         <td align="right"><?= number_format($awal,'2',',','.') ?></td>
                                         <td colspan="2"><strong>Jumlah SPJ</strong></td>
                                         <td align="right"><?= number_format($akhir,'2',',','.') ?></td>
                                         <td align="right"><?= number_format($awal-$akhir,'2',',','.') ?></td>
                                     </tr>
                                 </tfoot>
                             </table>
                             Awal : <?= $awal?>
                             Akhir : <?= $akhir ?>

                         </div>
                </div>
            </div>
        </div>
</div>

<?php } ?>
