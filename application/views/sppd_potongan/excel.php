<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Potongan Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Potongan Surat Perintah Pencairan Dana (SP2D)</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tahun</th>
								<th>No SP2D</th>
                                <th>Tanggal SP2D</th>
								<th>Keterangan</th>
								<th>No Rekening</th>
                                <th>Nama Rekening</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($sppd_potongan_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td valign="top" ><?php echo $rk->Tahun ?></td>
                                <td valign="top" width="200px"><?php echo $rk->No_SP2D; ?></td>
                                <td valign="top" style='mso-number-format:"\@"'><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_SP2D))) ?></td>
                                <td valign="top" ><?= $rk->Keterangan ?></td>
                                <td valign="top" ><?= $rk->Kd_Rek_Gabung  ?></td>
                                <td valign="top" ><?php echo $rk->Nm_Rek_5 ?></td>
                                <td valign="top" class="text-right"><?php echo number_format($rk->Nilai,'2',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="7" align="right">TOTAL NILAI</th>
                                <td class="text-right"><?= $nilai ?></td>
                            </tr>
                        </tfoot>
					</table>