<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-4">
    				<table valign="top">
    					<tr>
    						<td>SKPD</td>
    						<td>: <strong><?= $Kd_SKPD?></strong></td>
    					</tr>
    					<tr valign="top">
    						<td>Unit</td>
    						<td>: <strong><?= $Nm_Sub_Unit?></strong></td>
    					</tr>
    				</table>
            	</div>
            </div>			<hr>
			<div class="row">
				<div class="col-md-12">
					<form action="<?php echo $action ?>" method="get">
						<div class="row">
						<div width="300px" style="padding-left:15px;padding-right: 15px;">
								<div class="form-group">
									<label><strong>Tanggal SP2D</strong></label>
									<div class="row">
										<div width="120px" style="padding-left:20px;padding-right: 15px;">
											<input style="width:85px" type="text" name="tanggal1" value="<?= $tanggal1 ?>" placeholder="Tgl Mulai" class="form-control form-control-xs datepicker"  >
										</div>
										<div width="120px" style="padding-left:15px;padding-right: 15px;">
											<input style="width:85px" type="text" name="tanggal2" value="<?= $tanggal2 ?>" placeholder="Tgl Akhir" class="form-control form-control-xs datepicker"  >
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label><strong>Rekening</strong></label>
									<select class="form-control select2 form-control-sm" data-placeholder="Semua" name="kd_rek_gabung">
										<option value=""></option>
										<?php foreach($rekening as $rek){?>
										<option <?php if($rek->kd_rek_gabung==$kd_rek_gabung){echo "selected";}?> value="<?php echo $rek->kd_rek_gabung?>"><?php echo $rek->kd_rek_gabung.' - '.$rek->nm_rek_5 ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label><strong>Pencarian</strong></label>
									 <div class="input-group">
										<input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
										<input type="hidden" name="Kd_SKPD" value="<?= $Kd_SKPD ?>">
										<input type="hidden" name="Nm_Sub_Unit" value="<?= $Nm_Sub_Unit ?>">
										<span class="input-group-btn">
										<div class="btn-group">
											<button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
											<?php if ($q <> ''  || $kd_rek_gabung<>'')  { ?>
												<a href="<?php echo $action ; ?>" class="btn btn-warning"><i class="mdi mdi-close"></i></a>
											  <?php } ?>
											  <?= anchor($cetak,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success"')?>

										</div>
									</span>
								</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				</div>
                	<div class="table-responsive ">
                    <table class="table table-striped table-hover table-bordered"  >
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tahun</th>
								<th>No SP2D</th>
								<th>Keterangan</th>
								<th>Rekening</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($sppd_potongan_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td><?php echo $rk->Tahun ?></td>

								<td class="cell-detail"><span><?php echo $rk->No_SP2D ?></span>
										<span class="cell-detail-description"><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_SP2D))) ?></span>
								</td>
								<!-- <td><?php echo $rk->Kd_SKPD ?></td>
								<td class="cell-detail"><span><?php echo $rk->Nm_Unit ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Sub_Unit ?></span></td> -->

								<td><?php echo $rk->Keterangan ?></td>

							 	<td class="cell-detail">
							 		<span><?= $rk->Kd_Rek_Gabung?></span>
							 		<span class="cell-detail-description"><?= $rk->Nm_Rek_5?></span>
							 	</td>
							 	<td align="right"><?php echo number_format($rk->Nilai,'2',',','.') ?></td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo number_format($total_rows,'0','','.' )?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
					<div class="float-right">
                        <button  class="btn page-link  btn-space btn-success" disabled>Total Nilai : <?php echo $nilai ?></button>
					</div>
            </div>
        </div><!-- end card-->
    </div>
</div>