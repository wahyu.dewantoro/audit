<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Potongan Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Potongan Surat Perintah Pencairan Dana (SP2D)</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Kode SKPD</th>
								<th>Sub Unit</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($sppd_potongan_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td valign="top" ><?php echo $rk->Kd_SKPD ?></td>
                                <td valign="top" width="200px"><?php echo $rk->Nm_Sub_Unit; ?></td>
                                <td valign="top" ><?php echo number_format($rk->nilai,'2',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="3" align="right">TOTAL NILAI</th>
                                <td class="text-right"><?= $jum_nilai ?></td>
                            </tr>
                        </tfoot>
					</table>