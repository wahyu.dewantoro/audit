<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Pengguna <?php echo form_error('ms_pengguna_id') ?></label>
            <!-- <input type="text"  placeholder="Ms Pengguna Id" value="<?php echo $ms_pengguna_id; ?>" /> -->
            <select class="form-control form-control-xs" name="ms_pengguna_id" id="ms_pengguna_id">
                <option value=""></option>
                <?php foreach($pengguna as $pengguna){?>
                <option <?php if($pengguna->id_inc==$ms_pengguna_id){echo "selected"; }?> value="<?= $pengguna->id_inc ?>"><?= $pengguna->nama ?></option>
                <?php } ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Role <?php echo form_error('ms_role_id') ?></label>
            <!-- <input type="text"  placeholder="Ms Role Id" value="<?php echo $ms_role_id; ?>" /> -->
            <select class="form-control form-control-xs" name="ms_role_id" id="ms_role_id">
                <option value=""></option>
                <?php foreach($role as $role){ ?>
                <option <?php if($ms_role_id==$role->id_inc){echo "selected";}?> value="<?= $role->id_inc ?>"><?= $role->nama_role ?></option>
                <?php } ?>
            </select>
        </div>
	    <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
	    <button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button> 
	    <a class="btn btn-sm btn-warning" href="<?php echo site_url('asignrole') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
	</form>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>