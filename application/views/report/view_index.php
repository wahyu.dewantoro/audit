<div class="row pricing-tables">
<?php foreach($data as $rk){?>
<div class="col-md-3">
  <div class="pricing-table pricing-table-primary">
    <div class="pricing-table-image">
        <image src="<?= base_url().'assets/img/report.png'?>" width="100" >
    </div>
    <ul class="pricing-table-features">
      <li><strong><?= $rk->nama_laporan?></strong></li>
    </ul>
    <div class="card-divider card-divider-xl"></div>
    <a class="btn btn-primary" href="<?= base_url().$rk->url_laporan ?>">Tampilkan</a>
  </div>
</div>
<?php } ?>
</div>