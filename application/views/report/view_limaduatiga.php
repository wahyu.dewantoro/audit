<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
            
                  <table class="table table-striped table-hover table-bordered" id="table1">
                    <thead>
                      <tr>
                        <th width="15x">No</th>
                           <th>Kode SKPD</th>
                           <th>Sub Unit</th>
                           <th>Kode Rek 3</th>
                           <th>Nilai</th>
                           <!-- <th>Aksi</th> -->
                      </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($data as $rk){?>
                        <tr>
                            <td align="center"><?= $no++ ?></td>
                            <td><?= $rk->kd_skpd ?></td>
                            <td><?= $rk->nm_sub_unit ?></td>
                            <td><?= $rk->kode_rek_3 ?></td>
                            <td align="right"><?= number_format($rk->nilai,'2',',','.') ?></td>
                           <!--  <td align="center">
                                <form action="<?php //echo base_url().'report/detailLimaDuaTiga'?>">
                                    <input type="hidden" name="kd_skpd" value="<?= $rk->kd_skpd?>">
                                    <input type="hidden" name="nm_sub_unit" value="<?= $rk->nm_sub_unit?>">
                                    <input type="hidden" name="kode_rek_3" value="<?= $rk->kode_rek_3?>">
                                    <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>

                                </form>
                            </td> -->
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>