<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Tahun <?php echo form_error('Tahun') ?></label>
            <input type="text" class="form-control form-control-xs" name="Tahun" id="Tahun" placeholder="Tahun" value="<?php echo $Tahun; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Kd Jurnal <?php echo form_error('Kd_Jurnal') ?></label>
            <input type="text" class="form-control form-control-xs" name="Kd_Jurnal" id="Kd_Jurnal" placeholder="Kd Jurnal" value="<?php echo $Kd_Jurnal; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Jurnal <?php echo form_error('Nm_Jurnal') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Jurnal" id="Nm_Jurnal" placeholder="Nm Jurnal" value="<?php echo $Nm_Jurnal; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Tgl Bukti <?php echo form_error('Tgl_Bukti') ?></label>
            <input type="text" class="form-control form-control-xs" name="Tgl_Bukti" id="Tgl_Bukti" placeholder="Tgl Bukti" value="<?php echo $Tgl_Bukti; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">No Bukti <?php echo form_error('No_Bukti') ?></label>
            <input type="text" class="form-control form-control-xs" name="No_Bukti" id="No_Bukti" placeholder="No Bukti" value="<?php echo $No_Bukti; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">No BKU <?php echo form_error('No_BKU') ?></label>
            <input type="text" class="form-control form-control-xs" name="No_BKU" id="No_BKU" placeholder="No BKU" value="<?php echo $No_BKU; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Kd SKPD <?php echo form_error('Kd_SKPD') ?></label>
            <input type="text" class="form-control form-control-xs" name="Kd_SKPD" id="Kd_SKPD" placeholder="Kd SKPD" value="<?php echo $Kd_SKPD; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Unit <?php echo form_error('Nm_Unit') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Unit" id="Nm_Unit" placeholder="Nm Unit" value="<?php echo $Nm_Unit; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Sub Unit <?php echo form_error('Nm_Sub_Unit') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Sub_Unit" id="Nm_Sub_Unit" placeholder="Nm Sub Unit" value="<?php echo $Nm_Sub_Unit; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">ID Prog <?php echo form_error('ID_Prog') ?></label>
            <input type="text" class="form-control form-control-xs" name="ID_Prog" id="ID_Prog" placeholder="ID Prog" value="<?php echo $ID_Prog; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Ket Program <?php echo form_error('Ket_Program') ?></label>
            <input type="text" class="form-control form-control-xs" name="Ket_Program" id="Ket_Program" placeholder="Ket Program" value="<?php echo $Ket_Program; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Kd Keg <?php echo form_error('Kd_Keg') ?></label>
            <input type="text" class="form-control form-control-xs" name="Kd_Keg" id="Kd_Keg" placeholder="Kd Keg" value="<?php echo $Kd_Keg; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Ket Kegiatan <?php echo form_error('Ket_Kegiatan') ?></label>
            <input type="text" class="form-control form-control-xs" name="Ket_Kegiatan" id="Ket_Kegiatan" placeholder="Ket Kegiatan" value="<?php echo $Ket_Kegiatan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Keterangan <?php echo form_error('Keterangan') ?></label>
            <input type="text" class="form-control form-control-xs" name="Keterangan" id="Keterangan" placeholder="Keterangan" value="<?php echo $Keterangan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Akun Akrual 1 <?php echo form_error('Akun_Akrual_1') ?></label>
            <input type="text" class="form-control form-control-xs" name="Akun_Akrual_1" id="Akun_Akrual_1" placeholder="Akun Akrual 1" value="<?php echo $Akun_Akrual_1; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Akun Akrual 2 <?php echo form_error('Akun_Akrual_2') ?></label>
            <input type="text" class="form-control form-control-xs" name="Akun_Akrual_2" id="Akun_Akrual_2" placeholder="Akun Akrual 2" value="<?php echo $Akun_Akrual_2; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Akun Akrual 3 <?php echo form_error('Akun_Akrual_3') ?></label>
            <input type="text" class="form-control form-control-xs" name="Akun_Akrual_3" id="Akun_Akrual_3" placeholder="Akun Akrual 3" value="<?php echo $Akun_Akrual_3; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Akun Akrual 4 <?php echo form_error('Akun_Akrual_4') ?></label>
            <input type="text" class="form-control form-control-xs" name="Akun_Akrual_4" id="Akun_Akrual_4" placeholder="Akun Akrual 4" value="<?php echo $Akun_Akrual_4; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Akun Akrual 5 <?php echo form_error('Akun_Akrual_5') ?></label>
            <input type="text" class="form-control form-control-xs" name="Akun_Akrual_5" id="Akun_Akrual_5" placeholder="Akun Akrual 5" value="<?php echo $Akun_Akrual_5; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Akrual 1 <?php echo form_error('Nm_Akrual_1') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Akrual_1" id="Nm_Akrual_1" placeholder="Nm Akrual 1" value="<?php echo $Nm_Akrual_1; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Akrual 2 <?php echo form_error('Nm_Akrual_2') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Akrual_2" id="Nm_Akrual_2" placeholder="Nm Akrual 2" value="<?php echo $Nm_Akrual_2; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Akrual 3 <?php echo form_error('Nm_Akrual_3') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Akrual_3" id="Nm_Akrual_3" placeholder="Nm Akrual 3" value="<?php echo $Nm_Akrual_3; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Akrual 4 <?php echo form_error('Nm_Akrual_4') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Akrual_4" id="Nm_Akrual_4" placeholder="Nm Akrual 4" value="<?php echo $Nm_Akrual_4; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nm Akrual 5 <?php echo form_error('Nm_Akrual_5') ?></label>
            <input type="text" class="form-control form-control-xs" name="Nm_Akrual_5" id="Nm_Akrual_5" placeholder="Nm Akrual 5" value="<?php echo $Nm_Akrual_5; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Debet <?php echo form_error('Debet') ?></label>
            <input type="text" class="form-control form-control-xs" name="Debet" id="Debet" placeholder="Debet" value="<?php echo $Debet; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Kredit <?php echo form_error('Kredit') ?></label>
            <input type="text" class="form-control form-control-xs" name="Kredit" id="Kredit" placeholder="Kredit" value="<?php echo $Kredit; ?>" />
        </div>
	    <input type="hidden" name="" value="<?php echo $; ?>" /> 
	    <button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button> 
	    <a class="btn btn-sm btn-warning" href="<?php echo site_url('lradetail') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
	</form>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>