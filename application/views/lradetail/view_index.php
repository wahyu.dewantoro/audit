<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <form action="<?php echo site_url('lradetail/index'); ?>" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control form-control-sm select2" name="q">
                                <option value="">Pilih SKPD/OPD</option>
                                <?php foreach($subunit as $su){?>
                                <option <?php if($su->nm_sub_unit==$q){echo "selected";}?> value="<?= $su->nm_sub_unit ?>"><?= $su->nm_sub_unit ?></option>
                                <?php } ?>
                            </select>    
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">

                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('lradetail'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php }

                                    if($akses['create']==1){
                                     echo anchor(site_url('lradetail/create'),'<i class="mdi mdi-plus"></i> Tambah', 'class="btn btn-success btn-sm"');
                                    }
                                 ?>
                            </div>
                        </div>
                    </div>
                    
                </form>
                
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>SKPD</th>
								<th>Sub Unit</th>
								<th>Debet</th>
								<th>Kredit</th>
								<th width="10px">Detail</th>

                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($lradetail_data as $rk)  { ?>
								<tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->kd_skpd ?></td>
								<td><?php echo $rk->nm_sub_unit ?></td>
								<td align="right"><?php echo number_format($rk->debet,'2',',','.') ?></td>
								<td align="right"><?php echo number_format($rk->kredit,'2',',','.') ?></td>
								<td align="center">
									<form action="<?= base_url().'lradetail/read'?>">
										<input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd ?>'>
										<input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit ?>'>
										<button class="btn btn-primary btn-xs"><i class="mdi mdi-search"></i></button>
									</form>
								</td>

							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
					<!-- <div class="float-right">
						<?php echo $pagination ?>
					</div> -->
					<div class="float-right">
                    <button  class="btn page-link  btn-space btn-warning" disabled>Total Kredit : <?php echo $jum_kredit ?></button>
					</div>
					<div class="float-right">
                    <button  class="btn page-link  btn-space btn-success" disabled>Total Debet : <?php echo $jum_debet ?></button>
					</div>
            </div>
        </div><!-- end card-->
    </div>
</div>