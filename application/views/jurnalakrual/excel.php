<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Jurnal Akrual.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Jurnal Akrual</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>Tahun</th>
								<th>Jurnal</th>
								<th>Bukti</th>
                                <th>Tanggal Bukti</th>
								<th>BKU</th>
								<th>Program</th>
								<th>Kegiatan</th>
								<th>Keterangan</th>
								<th>Akrual 1 - 4</th>
								<th>Akrual 5</th>
								<th>Debet</th>
								<th>Kredit</th>
								<th>D/K</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($jurnalakrual_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top" ><?php echo $rk->Tahun ?></td>
                                <td valign="top" style='mso-number-format:"\@"'><?php echo $rk->Kd_Jurnal ?><br><?php echo $rk->Nm_Jurnal ?></td>
								<td valign="top"><?php echo $rk->No_Bukti ?></td>
								<td valign="top" style='mso-number-format:"\@"'><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_Bukti))) ?></td>
								<td valign="top"><?php echo $rk->No_BKU ?></td>
								<td valign="top"><?php echo $rk->Ket_Program ?></td>
								<td valign="top"><?php echo $rk->Ket_Kegiatan ?></td>
								<td valign="top"><?php echo $rk->Keterangan ?></td>
								<td valign="top" style='mso-number-format:"\@"'><span style="float:left;"><?php echo $rk->Akun_Akrual_1 ?></span><br>
									<?php echo $rk->Nm_Akrual_1 ?><br>
									<?php echo $rk->Akun_Akrual_2 ?><br>
									<?php echo $rk->Nm_Akrual_2 ?><br>
									<?php echo $rk->Akun_Akrual_3 ?><br>
									<?php echo $rk->Nm_Akrual_3 ?><br>
									<?php echo $rk->Akun_Akrual_4 ?><br>
									<?php echo $rk->Nm_Akrual_4 ?>
								</td>
								<td valign="top"  class="cell-detail"><?php echo $rk->Akun_Akrual_5 ?><br>
									<?php echo $rk->Nm_Akrual_5 ?></td>
								</td>
								<td align="right"><?php echo number_format($rk->Debet,'2',',','.') ?></td>
								<td align="right"><?php echo number_format($rk->Kredit,'2',',','.') ?></td>
                                <td><?php echo $rk->D_K ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="10" align="right">TOTAL</th>
                                <td class="text-right"><?= $jum_debet ?></td>
                                <td class="text-right"><?= $jum_kredit ?></td>
                                <td></td>
                            </tr>
                        </tfoot>
					</table>