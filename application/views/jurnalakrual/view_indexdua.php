<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-3">
            		<table>
            			<tr>
            				<td>SKPD</td>
            				<td>: <strong><?= $kd_skpd?></strong></td>
            			</tr>
            			<tr valign="top">
            				<td>Sub Unit</td>
            				<td>:<strong> <?= $nm_sub_unit ?></strong></td>
            			</tr>
            		</table>
            	</div>
            	<div class="col-md-9">
                    <form action="<?php echo $action ?>" method="get">
                        <div class="row">
								<div width="300px" style="padding-left:15px;padding-right: 15px;">
                                <div class="form-group">
                                    <label><strong>Tanggal Bukti</strong></label>
                                    <div class="row">
                                        <div width="120px" style="padding-left:20px;padding-right: 15px;">
                                            <input style="width:85px" type="text" name="tanggal1" value="<?= $tanggal1 ?>" placeholder="Tgl Mulai" class="form-control form-control-xs datepicker"  >
                                        </div>
                                        <div width="120px" style="padding-left:15px;padding-right: 15px;">
                                            <input style="width:85px" type="text" name="tanggal2" value="<?= $tanggal2 ?>" placeholder="Tgl Akhir" class="form-control form-control-xs datepicker"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label><strong>Akrual 5</strong></label>
                                <select class="form-control form-control-sm select2" data-placeholder='Semua' name="akun_akrual_5">
                                    <option value=""></option>
                                    <?php foreach($akrual5 as $ak){?>
                                    <option <?php if($akun_akrual_5==$ak->akun_akrual_5){echo "selected";} ?> value="<?= $ak->akun_akrual_5 ?>"><?= $ak->akun_akrual_5.' - '.$ak->nm_akrual_5 ?></option>>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Pencarian</strong></label>
                                    <div class="input-group">
                                        <input type='hidden' name='kd_skpd' value="<?= $kd_skpd ?>">
                                        <input type='hidden' name='nm_sub_unit' value="<?= $nm_sub_unit ?>">
                                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                                        <span class="input-group-btn">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                                <?php if ($q <> '' || $akun_akrual_5<>'')  { ?>
                                                    <a href="<?php echo $action ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> </a>
                                              <?php }  ?>
                                              <?= anchor($cetak,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success"')?>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

            	</div>
            </div>


                	<div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tahun</th>
								<th>Jurnal</th>
								<th>Bukti</th>
								<th>BKU</th>
								<th>Program</th>
								<th>Kegiatan</th>
								<th>Keterangan</th>
								<th>Akrual 1 - 4</th>
								<th>Akrual 5</th>
								<th>Debet</th>
								<th>Kredit</th>
								<th>D/K</th>

                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($jurnalakrual_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td><?php echo $rk->Tahun ?></td>
								<td class="cell-detail"><?php echo $rk->Kd_Jurnal ?> <span class="cell-detail-description"><?php echo $rk->Nm_Jurnal ?></span></td>
								<td class="cell-detail"><?php echo $rk->No_Bukti ?><span class="cell-detail-description"><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_Bukti))) ?></span></td>
								<td><?php echo $rk->No_BKU ?></td>
								<td><?php echo $rk->Ket_Program ?></td>
								<td><?php echo $rk->Ket_Kegiatan ?></td>
								<td><?php echo $rk->Keterangan ?></td>
								<td class="cell-detail"><span><?php echo $rk->Akun_Akrual_1 ?></span> <span class="cell-detail-description"><?php echo $rk->Nm_Akrual_1 ?></span>
									<span><?php echo $rk->Akun_Akrual_2 ?></span> <span class="cell-detail-description"><?php echo $rk->Nm_Akrual_2 ?></span>
									<span><?php echo $rk->Akun_Akrual_3 ?></span> <span class="cell-detail-description"><?php echo $rk->Nm_Akrual_3 ?></span>
									<span><?php echo $rk->Akun_Akrual_4 ?></span> <span class="cell-detail-description"><?php echo $rk->Nm_Akrual_4 ?></span>
								</td>


								<td class="cell-detail"><?php echo $rk->Akun_Akrual_5 ?> <span class="cell-detail-description"><?php echo $rk->Nm_Akrual_5 ?></span></td>
								<td align="right"><?php echo number_format($rk->Debet,'2',',','.') ?></td>
								<td align="right"><?php echo number_format($rk->Kredit,'2',',','.') ?></td>

								<td><?php echo $rk->D_K ?></td>

							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo number_format($total_rows,'0','','.'); ?></button>

					<div class="float-right">
						<?php echo $pagination ?>
					</div>
                    <div class="float-right">
                        <div class="btn-group">
                        <button  class="btn page-link  btn-space btn-success" disabled>Total Debet : <?php echo $jum_debet ?></button>
                        <button  class="btn page-link btn-space btn-warning" disabled>Total Kredit : <?php echo $jum_kredit ?></button>
                        </div>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>