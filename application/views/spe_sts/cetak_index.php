<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Tanda Setoran (STS).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Tanda Setoran (STS)</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>No SKPD</th>
                                <th>Unit</th>
                                <th>Sub Unit</th>
                                <th>No STS</th>
                                <th>Tgl STS</th>
                                <th>keterangan</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                
                            </tr>
                        </thead>
                        <tbody>

              <?php
                            // $no=1;
              foreach ($spe_sts_data as $rk)  { ?>
                            <tr>
                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                    <td><?= $rk->kd_skpd ?></td>
                                    <td><?= $rk->nm_unit ?></td>
                                    <td><?= $rk->nm_sub_unit ?></td>
                                    <td class="cell-detail"><?= $rk->no_sts ?></td>
                                    <td><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sts))) ?></td>
                                    <td><?= $rk->keterangan ?></td>
                                    <td align="center"><?= number_format($rk->jumlah,'0','','.') ?></td>
                                    <td align="right"><?php echo number_format($rk->nilai,'2',',','.') ?></td>
           
              </tr>
              <?php   } ?>
            </tbody>
					</table>