<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                
                	<div class="table-responsive noSwipe">
                    <table class="table table-striped table-hover table-bordered" id="table1" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No SKPD</th>
                                <th>Unit</th>
                                <th>Sub Unit</th>
                                <th>STS</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th></th>
                            </tr>
                        </thead> 
                        <tbody>
                            <?php foreach($data as $rk){?>
                            <tr>
                                <td align="center"><?= ++$start ?></td>
                                <td><?= $rk->Kd_SKPD ?></td>
                                <td><?= $rk->Nm_Unit ?></td>
                                <td><?= $rk->Nm_Sub_Unit ?></td>
                                <td class="cell-detail"><span><?= $rk->No_STS ?></span><span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->Tgl_STS))) ?></span></td>
                                <td><?= $rk->Keterangan ?></td>
                                <td align="center"><?= number_format($rk->jumlah,'0','','.')?></td>
                                <td align="right"><?= number_format($rk->nilai,'0','','.')?></td>
                                <td align="center">
                                    <form action="<?= base_url().'spe_sts/read' ?>">
                                        <input type="hidden" name="Kd_SKPD" value="<?= $rk->Kd_SKPD ?>">
                                        <input type="hidden" name="Nm_Unit" value="<?= $rk->Nm_Unit ?>">
                                        <input type="hidden" name="Nm_Sub_Unit" value="<?= $rk->Nm_Sub_Unit ?>">
                                        <input type="hidden" name="no_sts" value="<?= $rk->No_STS ?>">
                                        <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
                                    </form>

                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>       
					</table>
					</div>
					

            </div>
        </div><!-- end card-->
    </div>
</div>