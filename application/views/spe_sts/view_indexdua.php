<style type="text/css">
  #wgtmsr{
	 width:150px;
	}

	#wgtmsr option{
	  width:150px;
	}


</style>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-4">
    				<table valign="top">
    					<tr valign="top">
    						<td>SKPD</td>
    						<td>: </td>
    						<td><strong><?= $Kd_SKPD?></strong></td>
    					</tr>
    					<tr>
    						<td>Unit</td>
    						<td>:</td>
    						<td><strong><?= trim($Nm_Unit) ?></strong></td>
    					</tr>
    					<tr valign="top">
    						<td>Sub Unit</td>
    						<td>:</td>
    						<td> <strong><?= $Nm_Sub_Unit?></strong></td>
    					</tr>
    				</table>
            	</div>
            </div>
<!-- 
            		<hr>
            	<form action="<?php //echo $action ?>"  method="get" autocomplate="off">
            		<div class="row">
	                    <div width="300px" style="padding-left:15px;padding-right: 15px;">
	                    	<div class="form-group">
	                    		<label><strong>Tanggal STS</strong></label>
	                    		<div class="row">
	                    			<div width="120px" style="padding-left:20px;padding-right: 15px;">
	                    				<input  style="width:85px" type="text" name="tanggal1" value="<?= $tanggal1 ?>" placeholder="Tgl Mulai" class="form-control form-control-xs datepicker"  >
	                    			</div>
	                    			<div width="120px" style="padding-left:15px;padding-right: 15px;">
	                    				<input style="width:85px" type="text" name="tanggal2" value="<?= $tanggal2 ?>" placeholder="Tgl Akhir" class="form-control form-control-xs datepicker"  >
	                    			</div>
	                    		</div>
	                    	</div>
	                    </div>
	             

	                    <div class="col-md-5">
	                    	<div class="form-group">
	                    	<label><strong>Pencarian</strong></label>
	                    		<div class="input-group">
	                    		<input type="text" name="q" value="<?= $q ?>" placeholder="Pencarian" class="form-control form-control-xs" autofocus='true'>
	                    		<input type="hidden" name="kd_rek_gabung" value="">
	                    		<input type="hidden" name="rekening" value="<?= $kd_rek ?>">
	                        	<input type="hidden" name="Kd_SKPD" value="<?= $Kd_SKPD ?>">
	                        	<input type="hidden" name="Nm_Sub_Unit" value="<?= $Nm_Sub_Unit ?>">
	                        	<div class="btn-group">
			                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
			                                <?php if ($q <> '' || $kd_rek_gabung<> '')  { ?>
			                                    <a href="<?php echo $action ; ?>" class="btn btn-warning"><i class="mdi mdi-close"></i></a>
			                              <?php } ?>
										  
			                            </div>
			                     </div>
	                    	</div>
	                    </div>





	    
	                </div>
	                </form>
 -->


                	<div class="table-responsive ">
                    <table class="table table-striped table-hover table-bordered"  >
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tanggal</th>
								<th>STS</th>
								<th>Keterangan</th>
								<th>Kode Rekening</th>
								<th>Rekening</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php if(count($spe_sts_data)>0){ foreach ($spe_sts_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td><?= date_indo(date('Y-m-d',strtotime($rk->Tgl_STS))) ?></th>
								<td><?= $rk->No_STS ?></th>
								<td><?= $rk->Keterangan ?></th>
								<td><?= $rk->Kd_Rek_Gabung ?></th>
								<td><?= $rk->Nm_Rek_5 ?></th>
								<td align="right"><?= number_format($rk->Nilai,'0','','.') ?></th>
							</tr>
							<?php  } } else{  ?>
							<tr>
								<td align="center" colspan="7">
									<strong>Tidak ada data!</strong>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					</div>
					<!-- <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php //echo number_format($total_rows,'0','','.' )?></button> -->

					<div class="float-right">
						<?php //echo $pagination ?>
					</div>
					<div class="float-right">
						<!-- <button  class="btn page-link btn-space btn-success" disabled>Total Nilai : <?php //echo $nilai ?></button> -->
					</div>
            </div>
        </div><!-- end card-->
    </div>
</div>