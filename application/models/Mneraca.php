<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mneraca extends CI_Model
{

    public $table = 'tb_spe_detail_neraca';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd,$nm_sub_unit,$q = NULL) {
    	$this->db->where('kd_skpd',$kd_skpd);
    	$this->db->where('nm_sub_unit',$nm_sub_unit);
    	$this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Id_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Saldo_Awal_Debet like '%$q%' or Saldo_Awal_Kredit like '%$q%' or Mutasi_Debet like '%$q%' or Mutasi_Kredit like '%$q%' or Penyesuaian_Debet like '%$q%' or Penyesuaian_Kredit like '%$q%' or Gabungan_Debet like '%$q%' or Gabungan_Kredit like '%$q%')",null,false);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd,$nm_sub_unit, $limit, $start = 0, $q = NULL) {
    	$this->db->where('kd_skpd',$kd_skpd);
    	$this->db->where('nm_sub_unit',$nm_sub_unit);
       	$this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Id_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Saldo_Awal_Debet like '%$q%' or Saldo_Awal_Kredit like '%$q%' or Mutasi_Debet like '%$q%' or Mutasi_Kredit like '%$q%' or Penyesuaian_Debet like '%$q%' or Penyesuaian_Kredit like '%$q%' or Gabungan_Debet like '%$q%' or Gabungan_Kredit like '%$q%')",null,false);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function get_all_data($kd_skpd,$nm_sub_unit, $q = NULL) {
    	$this->db->where('kd_skpd',$kd_skpd);
    	$this->db->where('nm_sub_unit',$nm_sub_unit);
       	$this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Id_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Saldo_Awal_Debet like '%$q%' or Saldo_Awal_Kredit like '%$q%' or Mutasi_Debet like '%$q%' or Mutasi_Kredit like '%$q%' or Penyesuaian_Debet like '%$q%' or Penyesuaian_Kredit like '%$q%' or Gabungan_Debet like '%$q%' or Gabungan_Kredit like '%$q%')",null,false);
        return $this->db->get($this->table)->result();
    }

    function getData($limit, $start = 0, $q = NULL){
    	$this->db->select("kd_skpd,nm_sub_unit,sum(saldo_awal_debet) saldo_awal_debet,sum(saldo_awal_kredit) saldo_awal_kredit",false);
		$this->db->group_by('kd_skpd',false);
		$this->db->group_by('nm_sub_unit',false);
		$this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' )",null,false);
    	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    function getAllData($q = NULL){
        $this->db->select("kd_skpd,nm_sub_unit,sum(saldo_awal_debet) saldo_awal_debet,sum(saldo_awal_kredit) saldo_awal_kredit",false);
        $this->db->order_by("kd_skpd", "asc");
		$this->db->group_by('kd_skpd',false);
		$this->db->group_by('nm_sub_unit',false);
		$this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' )",null,false);
        return $this->db->get($this->table)->result();
    }
    function getRow($q=NULL){
    	$this->db->select("kd_skpd,nm_sub_unit,sum(saldo_awal_debet) saldo_awal_debet,sum(saldo_awal_kredit) saldo_awal_kredit",false);
		$this->db->group_by('kd_skpd',false);
		$this->db->group_by('nm_sub_unit',false);
		$this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' )",null,false);
    	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function getSubunit(){
        return $this->db->query("select distinct nm_sub_unit
                                from spe_detail_neraca")->result();
    }

    function getSum($q=null){
        $this->db->select("sum(saldo_awal_debet) debet,sum(saldo_awal_kredit) kredit",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' )",null,false);
        return $this->db->get($this->table)->row();
    }

    function getSumdetail($kd_skpd,$nm_sub_unit,$q = NULL){
        $this->db->select("sum(saldo_awal_debet) debet,sum(saldo_awal_kredit) kredit",false);
        $this->db->where('kd_skpd',$kd_skpd);
        $this->db->where('nm_sub_unit',$nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Id_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Saldo_Awal_Debet like '%$q%' or Saldo_Awal_Kredit like '%$q%' or Mutasi_Debet like '%$q%' or Mutasi_Kredit like '%$q%' or Penyesuaian_Debet like '%$q%' or Penyesuaian_Kredit like '%$q%' or Gabungan_Debet like '%$q%' or Gabungan_Kredit like '%$q%')",null,false);
        return $this->db->get($this->table)->row();
    }
}

/* End of file Mneraca.php */
/* Location: ./application/models/Mneraca.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:54:54 */
/* http://harviacode.com */