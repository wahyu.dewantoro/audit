<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mspp extends CI_Model
{

    public $table = 'tb_spe_spp';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows

	function total_rows($kd_skpd, $nm_sub_unit, $jenis_spp, $q = NULL) {
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->where('nm_sub_unit',$nm_sub_unit);
		$this->db->where('jenis_spp',$jenis_spp);
		$this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPP like '%$q%' or No_SPP like '%$q%' or Uraian like '%$q%' or Jenis_SPP like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPD like '%$q%' )",NULL,false);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
   function get_limit_data($nm_unit) {
        $this->db->select("kd_skpd,nm_sub_unit,jenis_spp,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) rekening,count(jenis_spp) jumlah,sum(nilai) nilai",false);
        $this->db->where("nm_unit like '%$nm_unit%'",null,false);
        $this->db->group_by("kd_skpd",false);
        $this->db->group_by("nm_sub_unit",false);
        $this->db->group_by("jenis_spp",false);
        $this->db->group_by("convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3)",false);
        return $this->db->get('tb_spe_spp')->result();
    }

    function get_all_data($kd_skpd, $nm_sub_unit, $jenis_spp, $q = NULL) {
		$this->db->where('kd_skpd',$kd_skpd);
		$this->db->where('nm_sub_unit',$nm_sub_unit);
		$this->db->where('jenis_spp',$jenis_spp);
		$this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPP like '%$q%' or No_SPP like '%$q%' or Uraian like '%$q%' or Jenis_SPP like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPD like '%$q%' )",NULL,false);
        return $this->db->get($this->table)->result();
    }
    function getrows($q=NULL){
    	$this->db->select("kd_skpd,nm_sub_unit,jenis_spp,count(jenis_spp) jumlah ,sum(nilai) nilai",false);
		$this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_spp like '%$q%')",null,false);
		$this->db->group_by('kd_skpd',false);
		$this->db->group_by('nm_sub_unit',false);
		$this->db->group_by('jenis_spp',false);
    	$this->db->from($this->table,false);
        return $this->db->count_all_results();
    }

    function getData($limit, $start = 0, $q = NULL){
		$this->db->select("kd_skpd,nm_sub_unit,jenis_spp,count(jenis_spp) jumlah ,sum(nilai) nilai",false);
		$this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_spp like '%$q%')",null,false);
		$this->db->group_by('kd_skpd',false);
		$this->db->group_by('nm_sub_unit',false);
		$this->db->group_by('jenis_spp',false);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table,false)->result();
    }

    /*function getAllData($q = NULL){
         $this->db->select("nm_unit
                        ,sum(case when jenis_spp ='up' then 1 end) jnsup
                        ,sum(case when jenis_spp ='up' then nilai end) nilaiup
                        ,sum(case when jenis_spp ='gu' then 1 end) jnsgu
                        ,sum(case when jenis_spp ='gu' then nilai end) nilaigu
                        ,sum(case when jenis_spp ='tu' then 1 end) jnstu
                        ,sum(case when jenis_spp ='tu' then nilai end) nilaitu
                        ,sum(case when jenis_spp ='ls' then 1 end) jnsls
                        ,sum(case when jenis_spp ='ls' then nilai end) nilails
                        ,sum(case when jenis_spp ='nihil' then 1 end) jnsnihil
                        ,sum(case when jenis_spp ='nihil' then nilai end) nilainihil",false);
        $this->db->where("nm_unit like '%$q%' ",NULL,false);
        $this->db->group_by('nm_unit',false);
        return $this->db->get($this->table,false)->result();

		 
    }*/

     function getalldata($limit, $start = 0, $q = NULL){
            $this->db->select("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spp ,no_spp ,uraian keterangan,jenis_spp ,count(*) jumlah ,sum(nilai) nilai",false); 
        $this->db->where("nm_unit like '%$q%' ",NULL,false);
        $this->db->limit($limit, $start);
        $this->db->group_by('kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spp ,no_spp ,jenis_spp ,uraian',false); 
        return $this->db->get($this->table,false)->result();
    }


     function getRow($q=NULL){
      $this->db->select("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spp ,no_spp ,uraian ,jenis_spp ,count(*) jumlah ,sum(nilai) nilai",false); 
      $this->db->where("nm_unit like '%$q%' ",NULL,false);
      // $this->db->limit($limit, $start);
      $this->db->group_by('kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spp ,no_spp ,jenis_spp ,uraian',false); 
      $this->db->from($this->table,false);
      return $this->db->count_all_results();
    }

    function getSumNilai($q=null){
        $this->db->select("sum(nilai) jum_nilai",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        return $this->db->get($this->table)->row();
    }

    function getSUbunit(){
          return $this->db->query("SELECT  nm_unit from tb_spe_spp
                                    group by nm_unit
                                    order by ltrim(nm_unit) asc")->result();
    }
    function getSumdetail($kd_skpd,$nm_sub_unit,$q = NULL,$jenis_spp){
        $this->db->select("sum(nilai) nilai",false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%' and jenis_spp like '%$jenis_spp%')",null,false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPP like '%$q%' or No_SPP like '%$q%' or Uraian like '%$q%' or Jenis_SPP like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPD like '%$q%' )",NULL,false);
        
        return $this->db->get($this->table)->row();
    }
  
    function getdetailspp($kd_skpd,$nm_unit,$nm_sub_unit,$no_spp){
        return $this->db->query("SELECT  tgl_spp ,no_spp ,no_spp ,jenis_spp ,nilai ,kd_rek_gabung ,nm_rek ,uraian ,nm_penerima ,rek_penerima ,bank_penerima
                                  FROM SPE_SPp
                                  where 
                                  kd_skpd ='$kd_skpd' and
                                  nm_unit ='$nm_unit' and
                                  nm_sub_unit ='$nm_sub_unit' and
                                  no_spp ='$no_spp'")->result();
    }

}
 