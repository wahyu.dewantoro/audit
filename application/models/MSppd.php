<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MSppd extends CI_Model
{

    public $table = 'tb_spe_sp2d';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

   function total_rows($kd_skpd=null,$nm_sub_unit=null,$jenis_sp2d=null,$q = NULL) {

    	if($kd_skpd != null){
            $this->db->where('kd_skpd',$kd_skpd);
        }

    	if($nm_sub_unit != null){
            $this->db->where('nm_sub_unit',$nm_sub_unit);
        }

    	if($jenis_sp2d != null){
            $this->db->where('jenis_sp2d',$jenis_sp2d);
        }

    	$this->db->where("(Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Ket_Program like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Jenis_SP2D like '%$q%' or Nilai like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or Tgl_SPM like '%$q%' or No_SPM    	 like '%$q%')",NULL,false);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_limit_data($nm_unit) {
        $this->db->select("kd_skpd,nm_sub_unit,jenis_sp2d,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) rekening,count(jenis_sp2d) jumlah,sum(nilai) nilai",false);
        $this->db->where("nm_unit like '%$nm_unit%'",null,false);
        $this->db->group_by("kd_skpd",false);
        $this->db->group_by("nm_sub_unit",false);
        $this->db->group_by("jenis_sp2d",false);
        $this->db->group_by("convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3)",false);

        return $this->db->get('tb_spe_sp2d')->result();
    }

    function getdetailsp2d($kode_unit, $kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d){
        return $this->db->query("SELECT ket_program, ket_kegiatan, tgl_sp2d, no_sp2d,  nilai, kd_rek_gabung, nm_rek_5
                                    FROM tb_spe_sp2d
                                    WHERE 
                                    kode_unit ='$kode_unit' and 
                                    kd_skpd ='$kd_skpd' and 
                                    nm_unit ='$nm_unit' and 
                                    nm_sub_unit ='$nm_sub_unit' and 
                                    no_sp2d ='$no_sp2d'  
                                    ORDER BY tgl_sp2d desc")->result();
    }

    // get data with limit and search
    function get_limit_datamonitor($kd_skpd=null,$nm_sub_unit=null,$jenis_sp2d=null,$limit, $start = 0, $q = NULL) {
        if($kd_skpd != null){
            $this->db->where('kd_skpd',$kd_skpd);
        }

        if($nm_sub_unit != null){
            $this->db->where('nm_sub_unit',$nm_sub_unit);
        }

        if($jenis_sp2d != null){
            $this->db->where('jenis_sp2d',$jenis_sp2d);
        }
        
    	$this->db->where("(Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Ket_Program like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Jenis_SP2D like '%$q%' or Nilai like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or Tgl_SPM like '%$q%' or No_SPM    	 like '%$q%')",NULL,false);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }



    function get_all_data($kd_skpd,$nm_sub_unit,$jenis_sp2d,$q = NULL) {
		$this->db->where('kd_skpd',$kd_skpd);
    	$this->db->where('nm_sub_unit',$nm_sub_unit);
    	$this->db->where('jenis_sp2d',$jenis_sp2d);
    	$this->db->where("(Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Ket_Program like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Jenis_SP2D like '%$q%' or Nilai like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or Tgl_SPM like '%$q%' or No_SPM    	 like '%$q%')",NULL,false);
        return $this->db->get($this->table)->result();
    }
  function getData($limit, $start = 0, $q = NULL){
		$this->db->select("kd_skpd,nm_sub_unit,jenis_sp2d,count(jenis_sp2d) jumlah,sum(nilai) nilai",false);
		$this->db->group_by('kd_skpd',false);
		$this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_sp2d like '%$q%')",NULL,false);
		$this->db->group_by('nm_sub_unit',false);
		$this->db->group_by('jenis_sp2d',false);
        $this->db->group_by('kd_skpd',false);
		$this->db->limit($limit, $start);
		return $this->db->get($this->table,false)->result();
    }
    
    function getAllData($limit, $start = 0, $q = NULL){
        
        $this->db->select("kode_unit ,kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sp2d ,no_sp2d ,keterangan ,jenis_sp2d,count(*) jumlah ,sum(nilai) nilai,nm_penerima ,rek_penerima ,bank_penerima",false);

        $this->db->group_by("kode_unit ,kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sp2d ,no_sp2d ,keterangan ,jenis_sp2d ,nm_penerima ,rek_penerima ,bank_penerima",false);
        $this->db->where("(kode_unit  like '%$q%' or kd_skpd  like '%$q%' or nm_unit  like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();

       /* $this->db->select("nm_unit
                        ,sum(case when jenis_sp2d ='up' then 1 end) jnsup
                        ,sum(case when jenis_sp2d ='up' then nilai end) nilaiup
                        ,sum(case when jenis_sp2d ='gu' then 1 end) jnsgu
                        ,sum(case when jenis_sp2d ='gu' then nilai end) nilaigu
                        ,sum(case when jenis_sp2d ='tu' then 1 end) jnstu
                        ,sum(case when jenis_sp2d ='tu' then nilai end) nilaitu
                        ,sum(case when jenis_sp2d ='ls' then 1 end) jnsls
                        ,sum(case when jenis_sp2d ='ls' then nilai end) nilails
                        ,sum(case when jenis_sp2d ='nihil' then 1 end) jnsnihil
                        ,sum(case when jenis_sp2d ='nihil' then nilai end) nilainihil",false);
        $this->db->where("nm_unit like '%$q%' ",NULL,false);
        $this->db->group_by('nm_unit',false);
        return $this->db->get($this->table,false)->result();*/

		/*$this->db->select("kd_skpd,nm_sub_unit,jenis_sp2d,count(jenis_sp2d) jumlah,sum(nilai) nilai",false);
		$this->db->group_by('kd_skpd',false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_sp2d like '%$q%')",NULL,false);
        $this->db->order_by("kd_skpd", "asc");
		$this->db->group_by('nm_sub_unit',false);
		$this->db->group_by('jenis_sp2d',false);
        $this->db->group_by('kd_skpd',false);
		*/
    }

    function getRow($q=NULL){
        $this->db->select("kode_unit ,kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sp2d ,no_sp2d ,keterangan ,jenis_sp2d ,sum(nilai) nilai,nm_penerima ,rek_penerima ,bank_penerima",false);
        $this->db->group_by("kode_unit ,kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sp2d ,no_sp2d ,keterangan ,jenis_sp2d ,nm_penerima ,rek_penerima ,bank_penerima",false);
        $this->db->where("(kode_unit  like '%$q%' or kd_skpd  like '%$q%' or nm_unit  like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


    function getSumNilai($q=null){
        $this->db->select("sum(nilai) jum_nilai",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        return $this->db->get($this->table)->row();
    }

    function getSUbunit(){
        return $this->db->query("SELECT  nm_unit from tb_spe_sp2d 
                                    group by nm_unit
                                    order by ltrim(nm_unit) asc")->result();
    }
    function getSumdetail($kd_skpd,$nm_sub_unit,$q = NULL,$jenis_sp2d){
        $this->db->select("sum(nilai) nilai",false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%' and jenis_sp2d like '%$jenis_sp2d%')",null,false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPM like '%$q%' or No_SP2D like '%$q%' or  Jenis_SP2D like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPM like '%$q%' )",NULL,false);
        // $this->db->group_by('Kd_SKPD',false);
        // $this->db->group_by('Nm_Sub_Unit',false);
        // $this->db->group_by('jenis_sp2d',false);
        return $this->db->get($this->table)->row();
    }


    function listpihak3(){
        return $this->db->query("SELECT DISTINCT nm_penerima from tb_spe_sp2d order by nm_penerima asc")->result();
    }

    function pihakTigaByPenerima($var){
        return $this->db->query("select * from tb_spe_sp2d where nm_penerima like '%$var%'")->result();
    }

    function sppdTu($kd_skpd,$jenis){
        return $this->db->query("SELECT tahun,jenis_sp2d, kode_unit, kd_skpd, nm_unit, nm_sub_unit, id_prog, ket_program, kd_keg, ket_kegiatan, tgl_sp2d, no_sp2d, keterangan, jenis_sp2d, nilai, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_gabung, nm_rek_5, nm_penerima, rek_penerima, bank_penerima, npwp, tgl_spm, no_spm, id_bukti FROM TB_SPE_SP2D WHERE JENIS_SP2D in ('$jenis') AND KD_SKPD='$kd_skpd' 
                            
         order by tgl_sp2d desc")->result();
    }

    function spjTu($kd_skpd,$jenis){
        return $this->db->query("SELECT tahun, jenis_spj,kd_skpd, nm_unit, nm_sub_unit, id_prog, ket_program, kd_keg, ket_kegiatan, tgl_spj, no_spj, tgl_bukti, no_bukti, tgl_pengesahan, no_pengesahan, keterangan, jenis_spj, nilai, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_gabung, nm_rek_5, uraian, id_bukti from tb_spe_spj where jenis_spj in ('$jenis') and kd_skpd='$kd_skpd'
                    and concat(tahun, kd_skpd, nm_unit, nm_sub_unit, id_prog, ket_program, kd_keg, ket_kegiatan, tgl_spj, no_spj, tgl_bukti, no_bukti, tgl_pengesahan, no_pengesahan, keterangan, jenis_spj, nilai, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_gabung, nm_rek_5, uraian, id_bukti ) not in (
                    select concat(  SPJ_Tahun, SPJ_Kd_SKPD, SPJ_Nm_Unit, SPJ_Nm_Sub_Unit, SPJ_ID_Prog, SPJ_Ket_Program, SPJ_Kd_Keg, SPJ_Ket_Kegiatan, SPJ_Tgl_SPJ, SPJ_No_SPJ, SPJ_Tgl_Bukti, SPJ_No_Bukti, SPJ_Tgl_Pengesahan, SPJ_No_Pengesahan, SPJ_Keterangan, SPJ_Jenis_SPJ, SPJ_Nilai, SPJ_Kd_Rek_1, SPJ_Kd_Rek_2, SPJ_Kd_Rek_3, SPJ_Kd_Rek_4, SPJ_Kd_Rek_5, SPJ_Kd_Rek_Gabung, SPJ_Nm_Rek_5, SPJ_Uraian, SPJ_Id_Bukti) from sp2d_vs_spj
                )
         order by tgl_spj DESC")->result();
    }

    function mappingsp2dvsspj($q){
        return $this->db->query("SELECT id,
                                   sp2d_tgl_sp2d tgl_sp2d,
                                    sp2d_no_sp2d no_sp2d, 
                                    sp2d_keterangan,
                                    sp2d_kd_rek_gabung,
                                    sp2d_nm_rek_5,
                                    sp2d_nilai,
                                   spj_tgl_spj tgl_spj,
                                    spj_no_spj no_spj,
                                   spj_keterangan,
                                   spj_nilai,
                                   spj_kd_rek_gabung,
                                    spj_nm_rek_5
                                   from sp2d_vs_spj
                                   where sp2d_kd_skpd='$q'")->result();
    }
}

/* End of file MSppd.php */
/* Location: ./application/models/MSppd.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:50:41 */
/* http://harviacode.com */