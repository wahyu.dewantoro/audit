<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mreport extends CI_Model
{

    
    /*public $id = 'id_inc';
    public $order = 'DESC';*/

    function __construct()
    {
        parent::__construct();
        /*
         $this->table = 'ms_pengguna';
        $this->menu_db = $this->load->database('menu', true);*/
    }

    function getList(){
        return $this->db->query("SELECT nama_laporan,url_laporan FROM MS_SHORCTCUT_REPORT")->result();
    }
    
    function getLimaDuaDua(){
        return $this->db->query("SELECT kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) kode_rek_3,sum(nilai) nilai
                                from tb_spe_sp2d
                                where convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) ='5.2.3'
                                group by kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3)
                                having  sum(nilai) < 1000000 
                                order by sum(nilai) asc")->result();
    }

    function getLimaDuaTiga(){
        return $this->db->query("SELECT kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) kode_rek_3,sum(nilai) nilai
                                from tb_spe_sp2d
                                where convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) ='5.2.3'
                                group by kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3)
                                having   sum(nilai) > 300000000
                                order by sum(nilai) asc")->result();
    }

    function getDetailDuaTiga($kd_skpd,$akun){
        return $this->db->query("SELECT kd_skpd,nm_sub_unit, tgl_sp2d,no_sp2d,jenis_sp2d,kd_rek_gabung,nm_rek_5,nilai
                                FROM tb_spe_sp2d 
                                where convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3)='$akun'
                                and kd_skpd='$kd_skpd'")->result();
    }

}

 