<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manggaran extends CI_Model
{

    public $table = 'tb_spe_anggaran';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd,$nm_sub_unit,$q = NULL) {
        $this->db->where('kd_skpd',$kd_skpd);
        $this->db->where('nm_sub_unit',$nm_sub_unit);
        $this->db->where("(Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_3 like '%$q%' or Nm_Akrual_3 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')",null,false);
        $this->db->from($this->table);
        $this->db->select("kd_skpd,ltrim(nm_sub_unit) nm_sub_unit,akun_akrual_3,nm_akrual_3,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2");
        $this->db->group_by('kd_skpd',false);
        $this->db->group_by('nm_sub_unit',false);
        $this->db->group_by('akun_akrual_3',false);
        $this->db->group_by('nm_akrual_3',false);
        return $this->db->count_all_results();
    }

    function total_rows_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$q = NULL){
        $this->db->where('kd_skpd',$kd_skpd);
        $this->db->where('nm_sub_unit',$nm_sub_unit);
        $this->db->where("(case when akun_akrual_3  is null then '' else akun_akrual_3 end ='$akun_akrual_3')",null,false);
        $this->db->where("(ket_program like '%$q%' or ket_kegiatan like '%$q%' or Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_5 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')",null,false);
        $this->db->from($this->table);
        
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd,$nm_sub_unit,$limit, $start = 0, $q = NULL) {
        $this->db->where('kd_skpd',$kd_skpd);
        $this->db->where('nm_sub_unit',$nm_sub_unit);
        $this->db->where("(Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_3 like '%$q%' or Nm_Akrual_3 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')",null,false);
	    $this->db->select("kd_skpd,ltrim(nm_sub_unit) nm_sub_unit,akun_akrual_3,nm_akrual_3,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2");
        $this->db->group_by('kd_skpd',false);
        $this->db->group_by('nm_sub_unit',false);
        $this->db->group_by('akun_akrual_3',false);
        $this->db->group_by('nm_akrual_3',false);
        $this->db->limit($limit, $start);

        return $this->db->get($this->table)->result();
    }

    function get_limit_data_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$limit, $start = 0, $q = NULL){
        $this->db->where('kd_skpd',$kd_skpd);
        $this->db->where('nm_sub_unit',$nm_sub_unit);
        $this->db->where("(case when akun_akrual_3  is null then '' else akun_akrual_3 end ='$akun_akrual_3')",null,false);
        $this->db->where("(ket_program like '%$q%' or ket_kegiatan like '%$q%' or Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_5 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')",null,false);
        $this->db->select("kd_skpd,ket_program,ket_kegiatan,ltrim(nm_sub_unit) nm_sub_unit,akun_akrual_5,nm_akrual_5,anggaran_awal, anggaran_perubahan_1, anggaran_perubahan_2");
        $this->db->limit($limit, $start);
        $this->db->order_by('akun_akrual_5','asc');
        return $this->db->get($this->table)->result();
    }


    function getData($limit, $start = 0, $q = NULL){
        $this->db->select("kd_skpd,nm_sub_unit,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        $this->db->group_by('kd_skpd');
        $this->db->group_by('nm_sub_unit');
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function getAllData( $q = NULL){
        $this->db->order_by("nm_sub_unit", "asc");
        $this->db->select("kd_skpd,ltrim(nm_sub_unit) nm_sub_unit ,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        $this->db->group_by('kd_skpd');
        $this->db->group_by('nm_sub_unit');
        return $this->db->get($this->table)->result();
    }
    function getRow($q=NULL){
        $this->db->select("kd_skpd,nm_sub_unit,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        $this->db->group_by('kd_skpd');
        $this->db->group_by('nm_sub_unit');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function getSubunit(){
        // return $this->db->query("select distinct nm_sub_unit from spe_anggaran")->result();
        return $this->db->query("SELECT distinct ltrim(nm_sub_unit) nm_sub_unit from spe_anggaran order by nm_sub_unit asc")->result();
    }

     function getSum($q=null){
        $this->db->select("sum(anggaran_awal) awal,sum(anggaran_perubahan_1) satu,sum(anggaran_perubahan_2) dua",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        return $this->db->get($this->table)->row();
    }

    function getSumdetail($kd_skpd,$nm_sub_unit,$q = NULL){
        $this->db->select("sum(anggaran_awal) awal,sum(anggaran_perubahan_1) satu,sum(anggaran_perubahan_2) dua",false);
        $this->db->where('kd_skpd',$kd_skpd);
        $this->db->where('nm_sub_unit',$nm_sub_unit);
        $this->db->where("(Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_3 like '%$q%' or Nm_Akrual_3 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')",null,false);
    
        return $this->db->get($this->table)->row();
    }

    function getSumdetail_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$q = NULL){
        $this->db->select("sum(anggaran_awal) awal,sum(anggaran_perubahan_1) satu,sum(anggaran_perubahan_2) dua",false);
        $this->db->where('kd_skpd',$kd_skpd);
        $this->db->where('nm_sub_unit',$nm_sub_unit);
        $this->db->where("(case when akun_akrual_3  is null then '' else akun_akrual_3 end ='$akun_akrual_3')",null,false);
        $this->db->where("(ket_program like '%$q%' or ket_kegiatan like '%$q%' or Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_5 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')",null,false);
        return $this->db->get($this->table)->row();       
    }

    function getDataRealisasi($limit, $start = 0, $q = NULL){
        $this->db->select("tb_spe_anggaran.kd_skpd, tb_spe_anggaran.nm_sub_unit, kd_rek_gabung, nm_rek_5,sum( case when anggaran_perubahan_1 < 0 then anggaran_awal else anggaran_perubahan_1 end ) anggaran, sum(nilai) nilai ",false);
        $this->db->join('tb_spe_sp2d','tb_spe_anggaran.kd_skpd=tb_spe_sp2d.kd_skpd  and tb_spe_anggaran.akun_akrual_5=tb_spe_sp2d.kd_rek_gabung',false);
        $this->db->limit($limit, $start);
        $this->db->where("(tb_spe_anggaran.kd_skpd like '%$q%' or tb_spe_anggaran.nm_sub_unit like '%$q%' or kd_rek_gabung like '%$q%' or nm_rek_5 like '%$q%' or anggaran_awal like '%$q%' or anggaran_perubahan_1 like '%$q%' or anggaran_perubahan_2 like '%$q%' or nilai like '%$q%')",null,false);
        $this->db->group_by("tb_spe_anggaran.kd_skpd",false);
        $this->db->group_by("tb_spe_anggaran.nm_sub_unit",false);
        $this->db->group_by("kd_rek_gabung",false);
        $this->db->group_by("nm_rek_5",false);
        // $this->db->group_by("case when anggaran_perubahan_2 is null then case when anggaran_perubahan_1 is null then anggaran_awal else anggaran_perubahan_1 end  else anggaran_perubahan_2 end ",false);

        return $this->db->get('tb_spe_anggaran')->result();
    }

    function getAllDataRealisasi($q = NULL){
        $this->db->select("tb_spe_anggaran.kd_skpd, tb_spe_anggaran.nm_sub_unit, kd_rek_gabung, nm_rek_5,sum( case when anggaran_perubahan_1 < 0 then anggaran_awal else anggaran_perubahan_1 end ) anggaran, sum(nilai) nilai ",false);
        $this->db->join('tb_spe_sp2d','tb_spe_anggaran.kd_skpd=tb_spe_sp2d.kd_skpd  and tb_spe_anggaran.akun_akrual_5=tb_spe_sp2d.kd_rek_gabung',false);
        $this->db->where("(tb_spe_anggaran.kd_skpd like '%$q%' or tb_spe_anggaran.nm_sub_unit like '%$q%' or kd_rek_gabung like '%$q%' or nm_rek_5 like '%$q%' or anggaran_awal like '%$q%' or anggaran_perubahan_1 like '%$q%' or anggaran_perubahan_2 like '%$q%' or nilai like '%$q%')",null,false);
        $this->db->group_by("tb_spe_anggaran.kd_skpd",false);
        $this->db->group_by("tb_spe_anggaran.nm_sub_unit",false);
        $this->db->group_by("kd_rek_gabung",false);
        $this->db->group_by("nm_rek_5",false);
        // $this->db->group_by("case when anggaran_perubahan_2 is null then case when anggaran_perubahan_1 is null then anggaran_awal else anggaran_perubahan_1 end  else anggaran_perubahan_2 end ",false);

        return $this->db->get('tb_spe_anggaran')->result();
    }

    function getRowRealisasi($q=null){
        $this->db->select("tb_spe_anggaran.kd_skpd, tb_spe_anggaran.nm_sub_unit, kd_rek_gabung, nm_rek_5,sum( case when anggaran_perubahan_1 < 0 then anggaran_awal else anggaran_perubahan_1 end ) anggaran, sum(nilai) nilai ",false);
        $this->db->join('tb_spe_sp2d','tb_spe_anggaran.kd_skpd=tb_spe_sp2d.kd_skpd  and tb_spe_anggaran.akun_akrual_5=tb_spe_sp2d.kd_rek_gabung',false);
        // $this->db->limit($limit, $start);
        $this->db->where("(tb_spe_anggaran.kd_skpd like '%$q%' or tb_spe_anggaran.nm_sub_unit like '%$q%' or kd_rek_gabung like '%$q%' or nm_rek_5 like '%$q%' or anggaran_awal like '%$q%' or anggaran_perubahan_1 like '%$q%' or anggaran_perubahan_2 like '%$q%' or nilai like '%$q%')",null,false);
        $this->db->group_by("tb_spe_anggaran.kd_skpd",false);
        $this->db->group_by("tb_spe_anggaran.nm_sub_unit",false);
        $this->db->group_by("kd_rek_gabung",false);
        $this->db->group_by("nm_rek_5",false);    


        /*$this->db->select("tb_spe_anggaran.kd_skpd,tb_spe_anggaran.nm_sub_unit,kd_rek_gabung,nm_rek_5,case when anggaran_perubahan_2 is null then case when anggaran_perubahan_1 is null then anggaran_awal else anggaran_perubahan_1 end  else anggaran_perubahan_2 end  anggaran,sum(nilai) nilai",false);
        $this->db->join('tb_spe_sp2d','tb_spe_anggaran.kd_skpd=tb_spe_sp2d.kd_skpd  and tb_spe_anggaran.akun_akrual_5=tb_spe_sp2d.kd_rek_gabung',false);
        $this->db->group_by("tb_spe_anggaran.kd_skpd",false);
        $this->db->group_by("tb_spe_anggaran.nm_sub_unit",false);
        $this->db->group_by("kd_rek_gabung",false);
        $this->db->group_by("nm_rek_5",false);*/
        // $this->db->group_by("case when anggaran_perubahan_2 is null then case when anggaran_perubahan_1 is null then anggaran_awal else anggaran_perubahan_1 end  else anggaran_perubahan_2 end ",false);
        /*$this->db->group_by("anggaran_awal",false);
        $this->db->group_by("anggaran_perubahan_1",false);
        $this->db->group_by("anggaran_perubahan_2",false);*/
        // $this->db->where("(tb_spe_anggaran.kd_skpd like '%$q%' or tb_spe_anggaran.nm_sub_unit like '%$q%' or kd_rek_gabung like '%$q%' or nm_rek_5 like '%$q%' or anggaran_awal like '%$q%' or anggaran_perubahan_1 like '%$q%' or anggaran_perubahan_2 like '%$q%' or nilai like '%$q%')",null,false);
        $this->db->from('tb_spe_anggaran');
     return $this->db->count_all_results();
    }
}

/* End of file Manggaran.php */
/* Location: ./application/models/Manggaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:57:23 */
/* http://harviacode.com */