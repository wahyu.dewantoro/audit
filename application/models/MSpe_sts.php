<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MSpe_sts extends CI_Model
{

    public $table = 'tb_spe_sts';
    public $id    = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }


    function getListRektiga($kd_skpd){
        return $this->db->query("SELECT  Kd_SKPD
                                  ,Nm_Unit
                                  ,Nm_Sub_Unit
                                    ,Tgl_STS
                                  ,No_STS
                                  ,Keterangan 
                                  ,count(*) jumlah,sum(Nilai) nilai
                              FROM SPE_STS
                              where kd_skpd='$kd_skpd'
                              group by  Kd_SKPD
                                  ,Nm_Unit
                                  ,Nm_Sub_Unit
                                    ,Tgl_STS
                                  ,No_STS
                                  ,Keterangan")->result();
    }


    // get total rows
    function total_rows($Kd_SKPD,$Nm_Sub_Unit,$q = NULL) {
    $this->db->where('Kd_SKPD',$Kd_SKPD);
    $this->db->where('Nm_Sub_Unit',$Nm_Sub_Unit);
    $this->db->where("(Tahun like '%$q%' or  Ket_Program like '%$q%'  or Ket_Kegiatan like '%$q%' or No_STS like '%$q%' or Keterangan like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )",null,false);

    $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($Kd_SKPD,$Nm_Sub_Unit,$limit, $start = 0, $q = NULL) {
        $this->db->where('Kd_SKPD',$Kd_SKPD);
        $this->db->where('Nm_Sub_Unit',$Nm_Sub_Unit);
        $this->db->where("(Tahun like '%$q%' or  Ket_Program like '%$q%'  or Ket_Kegiatan like '%$q%' or No_STS like '%$q%' or Keterangan like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )",null,false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function get_all_data($Kd_SKPD,$Nm_Sub_Unit, $q = NULL) {
        $this->db->where('Kd_SKPD',$Kd_SKPD);
        $this->db->where('Nm_Sub_Unit',$Nm_Sub_Unit);
        $this->db->where("(Tahun like '%$q%' or  Ket_Program like '%$q%'  or Ket_Kegiatan like '%$q%' or No_STS like '%$q%' or Keterangan like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )",null,false);
        return $this->db->get($this->table)->result();
    }
    function getdata($limit,$start=0,$q=NULL){
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai',false);
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')",null,false);
        $this->db->group_by('Kd_SKPD',false);
        $this->db->group_by('Nm_Sub_Unit',false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    
    
    function getalldata($limit, $start = 0, $q = NULL){
        $this->db->select('kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan ,count(*) jumlah,sum(nilai) nilai',false);
        $this->db->where("(Kd_SKPD like '%$q%' or nm_unit like '%$q%' or  Nm_Sub_Unit like '%$q%')",null,false);
        $this->db->group_by('kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan',false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    function gettotal($q = NULL) {
        $this->db->select('kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan ,count(*) jumlah,sum(nilai) nilai',false);
        $this->db->where("(Kd_SKPD like '%$q%' or nm_unit like '%$q%' or  Nm_Sub_Unit like '%$q%')",null,false);
        $this->db->group_by('kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan',false);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function getSum($q=null){
        $this->db->select("sum(nilai) nilai",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' )",null,false);
        return $this->db->get($this->table)->row();
    }

    function getSubunit(){
        return $this->db->query("select  ltrim(nm_unit) nm_unit
                                from ".$this->table.' group by nm_unit order by nm_unit asc')->result();
    }

    function getSumdetail($kd_skpd,$nm_sub_unit,$q = NULL){
        $this->db->select("sum(nilai) nilai",false);
        $this->db->where('Kd_SKPD',$kd_skpd);
        $this->db->where('Nm_Sub_Unit',$nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or  Ket_Program like '%$q%'  or Ket_Kegiatan like '%$q%' or No_STS like '%$q%' or Keterangan like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )",null,false);
        return $this->db->get($this->table)->row();
    }

}

/* End of file MSpe_sts.php */
/* Location: ./application/models/MSpe_sts.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 12:03:48 */
/* http://harviacode.com */