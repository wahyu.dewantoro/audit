<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mspd extends CI_Model
{

    public $table = 'tb_spe_spd';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

      // get total rows
    function total_rows($Kd_SKPD,$Nm_Sub_Unit,$q = NULL) {
    $this->db->where('Kd_SKPD',$Kd_SKPD);
    $this->db->where('Nm_Sub_Unit',$Nm_Sub_Unit);
    $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPD like '%$q%' or No_SPD like '%$q%' or uraian like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )",null,false);

    $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($Kd_SKPD,$Nm_Sub_Unit,$limit, $start = 0, $q = NULL) {
        // $this->db->order_by($this->id, $this->order);
            $this->db->where('Kd_SKPD',$Kd_SKPD);
            $this->db->where('Nm_Sub_Unit',$Nm_Sub_Unit);
            $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPD like '%$q%' or No_SPD like '%$q%' or uraian like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )",null,false);

            $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function get_all_data($Kd_SKPD,$Nm_Sub_Unit,$q = NULL) {
        // $this->db->order_by($this->id, $this->order);
            $this->db->where('Kd_SKPD',$Kd_SKPD);
            $this->db->where('Nm_Sub_Unit',$Nm_Sub_Unit);
            $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPD like '%$q%' or No_SPD like '%$q%' or uraian like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )",null,false);
        return $this->db->get($this->table)->result();
    }

    function getdata($limit,$start=0,$q=NULL){
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai',false);
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')",null,false);
        $this->db->group_by('Kd_SKPD',false);
        $this->db->group_by('Nm_Sub_Unit',false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();

    }

    function getAlldata($q=NULL){
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, count(*) jumlah,sum(nilai) nilai',false);
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')",null,false);
        $this->db->order_by("kd_skpd", "asc");
        $this->db->group_by('Kd_SKPD',false);
        $this->db->group_by('Nm_Sub_Unit',false);
        return $this->db->get($this->table)->result();

    }

    function gettotal($q = NULL) {
        /*//*/
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')",null,false);
        $this->db->from($this->table);
        $this->db->group_by('Kd_SKPD',false);
        $this->db->group_by('Nm_Sub_Unit',false);
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai',false);
        return $this->db->count_all_results();
    }
    function getSumNilai($q=null){
        $this->db->select("sum(nilai) jum_nilai",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        return $this->db->get($this->table)->row();
    }

    function getSUbunit(){
        return $this->db->query("select distinct   nm_sub_unit from $this->table order by  nm_sub_unit asc")->result();
    }
    function getSumdetail($kd_skpd,$nm_sub_unit,$q = NULL){
        $this->db->select("sum(nilai) nilai",false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%')",null,false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPD like '%$q%' or No_SPD like '%$q%' or Uraian like '%$q%' or  Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or No_SPD like '%$q%' )",NULL,false);
        // $this->db->group_by('Kd_SKPD',false);
        // $this->db->group_by('Nm_Sub_Unit',false);
        return $this->db->get($this->table)->row();
    }
}

/* End of file Mspd.php */
/* Location: ./application/models/Mspd.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 12:12:03 */
/* http://harviacode.com */