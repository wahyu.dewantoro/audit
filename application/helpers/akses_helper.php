<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Session Helper
 *
 * A simple session class helper for Codeigniter
 *
 * @package     Codeigniter Session Helper
 * @author      Dwayne Charrington
 * @copyright   Copyright (c) 2014, Dwayne Charrington
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 * @link        http://ilikekillnerds.com
 * @since       Version 1.0
 * @filesource
 */
if (!function_exists('showMenu'))
{
    function showMenu($kode_group)
    {
        $CI = get_instance();
         $CI->menu_db=$CI->load->database('menu',true); 
        $qmenu0  =$CI->menu_db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'  
                                          AND parent='0' 
                                          ORDER BY sort ASC")->result_array();

        $varmenu='';
              foreach ($qmenu0 as $row0) {
                $parent  =$row0['id_inc'];
                $qmenu1  =$CI->menu_db->query("SELECT a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'  
                                          AND parent='$parent' 
                                          ORDER BY sort ASC");
                $cekmenu =$qmenu1->num_rows();
                $dmenu1  =$qmenu1->result_array();
                  if($cekmenu>0){
                    $varmenu.= "<li class='parent'>
                      <a href='#' > <i class='".$row0['icon']."'></i>  <span>".ucwords($row0['nama_menu'])."</span> <span class='menu-arrow'></span></a>";
                        $varmenu.= "<ul class='sub-menu'>";
                          foreach($dmenu1 as $row1){
                              $varmenu.= "<li>".anchor(strtolower($row1['link_menu']),"<i class='mdi mdi-chevron-right'></i> <span>".ucwords($row1['nama_menu']."</span>"),'class="nav-link "')."</li>";
                          }
                      $varmenu.= "</ul>
                      </li>";
                      }else{
                       $varmenu.= "<li>".anchor(strtolower($row0['link_menu']),"<i class='".$row0['icon']."'></i> <span>".ucwords($row0['nama_menu'].'</span>'),'class="nav-link "')."</li>";
                    }
                  }
           echo $varmenu;
    }
}


if (!function_exists('errorbos'))
{
    function errorbos()
    {
            $CI = &get_instance();
            $CI->load->view('errors/403','',true);
            die();
    }
}

if (!function_exists('cek'))
{
    function cek($pengguna,$url,$var)
    {
        $CI = get_instance();
        $CI->menu_db=$CI->load->database('menu',true); 

        if(empty($var)){
            errorbos();
        }
        switch ($var) {
            case 'read':
                # code...
                $vv='status';
                break;  
            case 'create':
                  # code...
            $vv='created';
                  break;  
            case 'update':
            $vv='updated';
                # code...
                break;  
              case 'delete':
              $vv='deleted';
                # code...
                break;
            default:
                # code...
                $vv='';
                break;
        }

        $res=$CI->menu_db->query("SELECT a.*
                                FROM ms_privilege a
                                JOIN ms_menu b ON a.ms_menu_id=b.id_inc
                                JOIN ms_assign_role d on d.ms_role_id=a.ms_role_id
                                JOIN ms_pengguna c ON c.id_inc=d.ms_pengguna_id
                                WHERE c.id_inc=$pengguna AND REPLACE(link_menu,'/','.')='$url' AND a.".$vv." ='1'")->row();

        if($res){
                return array(
                    'read'=>$res->status,
                    'create'=>$res->created,
                    'update'=>$res->updated,
                    'delete'=>$res->deleted
                );

        }else{
            errorbos();
        }
    }
}