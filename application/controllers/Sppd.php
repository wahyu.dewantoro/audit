<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sppd extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('MSppd');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Sppd';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']  = base_url() . 'sppd?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sppd?q=' . urlencode($q);
            $cetak               = base_url() . 'sppd/cetak_index?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'sppd';
            $config['first_url'] = base_url() . 'sppd';
            $cetak               = base_url() . 'sppd/cetak_index';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->MSppd->getRow($q);
        $sppd                      = $this->MSppd->getAllData($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $subunit =$this->MSppd->getSUbunit();
        $data = array(
            'sppd_data'  => $sppd,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'title'      => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'      => $akses,
            'subunit'    => $subunit,
            'cetak'      => $cetak,
        );
        $this->template->load('layout','sppd/view_index',$data);


/*

        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $cetak = base_url() . 'sppd/cetak_index?q=' . urlencode($q);
        } else {
            $cetak = base_url() . 'sppd/cetak_index';
        }

        $sppd = $this->MSppd->getAllData($q);
        $subunit =$this->MSppd->getSUbunit();
        $data = array(
            'sppd_data'  => $sppd,
            'q'          => $q,
            'start'      => $start,
            'title'      => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'      => $akses,
            'subunit'    => $subunit,
            'cetak'      => $cetak,
        );
        $this->template->load('layout','sppd/view_index',$data);*/
    }

    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $sppd  = $this->MSppd->getAllData($q);
        // $total=$this->MSppd->getSumNilai($q);
        $start = intval($this->input->get('start'));
        $data = array(
            'sppd_data' => $sppd,
            'start'      => $start,
        );
        $this->load->view('sppd/cetak_index',$data);
    }
    
    public function read()
    {
        $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $sppd    = $this->MSppd->get_limit_data($nm_unit);
        $start   = intval($this->input->get('start'));
        $data = array(
            'sppd_data' => $sppd,
            'title'     => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'     => $akses,
            'nm_unit'   => $nm_unit,
            'kembali'   => base_url().'sppd',
            'start'     => $start,
            'cetak'     => base_url().'sppd/cetakdua?nm_unit='.$nm_unit
        );

        $this->template->load('layout','sppd/view_indexdua',$data);
    }

    function cetakdua(){
        $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $sppd    = $this->MSppd->get_limit_data($nm_unit);
        $start       = intval($this->input->get('start'));
        $data = array(
            'sppd_data' => $sppd,
            'title'     => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'     => $akses,
            'nm_unit'   => $nm_unit,
            'kembali'   => base_url().'sppd',
            'start'     => $start,
            'cetak'     => base_url().'sppd/cetakdua?nm_unit='.$nm_unit
        );

        $this->load->view('sppd/cetak_indexdua',$data);   
    }

    function readdua(){
/*        echo "<pre>";
        print_r($this->input->get());
        die();*/
        $akses       = $this->cekAkses('read');

        $kode_unit   =urldecode($this->input->get("kode_unit",true));
        $kd_skpd     =urldecode($this->input->get("kd_skpd",true));
        $nm_unit     =urldecode($this->input->get("nm_unit",true));
        $nm_sub_unit =urldecode($this->input->get("nm_sub_unit",true));
        $no_sp2d     =urldecode($this->input->get("no_sp2d",true));
        $start       = intval($this->input->get('start'));
        

        // $kode_unit, $kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d
        $sppd=$this->MSppd->getdetailsp2d($kode_unit, $kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d);
        $data = array(
            'sppd_data'   => $sppd,
            'title'       => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'       => $akses,
                
            'kode_unit' => $kode_unit,
            'kd_skpd' => $kd_skpd,
            'nm_unit' => $nm_unit,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sp2d' => $no_sp2d,


            'kembali'     => base_url().'sppd',
            'start'       => $start,
            'script'      =>'report/datatables',
            'cetak'       =>base_url().'sppd/cetaktiga?kode_unit='.urlencode($kode_unit). '&kd_skpd='.urlencode($kd_skpd). '&nm_unit='.urlencode($nm_unit). '&nm_sub_unit='.urlencode($nm_sub_unit). '&no_sp2d='.urlencode($no_sp2d)
        );

        $this->template->load('layout','sppd/view_indextiga',$data);   
    }

    function cetaktiga(){
       $kode_unit   =urldecode($this->input->get("kode_unit",true));
        $kd_skpd     =urldecode($this->input->get("kd_skpd",true));
        $nm_unit     =urldecode($this->input->get("nm_unit",true));
        $nm_sub_unit =urldecode($this->input->get("nm_sub_unit",true));
        $no_sp2d     =urldecode($this->input->get("no_sp2d",true));
        $start       = intval($this->input->get('start'));
        
        $sppd=$this->MSppd->getdetailsp2d($kode_unit, $kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d);
        $data = array(
            'sppd_data'   => $sppd,
            'title'       => 'Surat Perintah Pencairan Dana (SP2D)',
            // 'akses'       => $akses,
             'kode_unit' => $kode_unit,
            'kd_skpd' => $kd_skpd,
            'nm_unit' => $nm_unit,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sp2d' => $no_sp2d,


            'start'       => $start,
            // 'script'      =>'report/datatables',
            // 'cetak'       =>base_url().'sppd/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_unit='.urlencode($nm_unit).'&nm_sub_unit='.urlencode($nm_sub_unit).'&rekening='.urlencode($rekening).'&jenis_sp2d='.urlencode($jenis_sp2d)
        );

        $this->load->view('sppd/cetak_indextiga',$data);      
    }

    /*public function read()
    {
        $akses       = $this->cekAkses('read');
        $res         =$this->input->get();
        $kd_skpd     =urldecode($res['kd_skpd']);
        $nm_sub_unit =urldecode($res['nm_sub_unit']);
        $jenis_sp2d  =urldecode($this->input->get('jenis_sp2d',true));

        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));


        if($tanggal1==''){
            $tanggal1='01/01/'.date('Y');
        }

        if($tanggal2==''){
            $tanggal2=date('d/m/Y');
        }

         if ($kd_rek_gabung<>'' || $q<> '' || $tanggal1 <> '' || $tanggal2<>'') {
            $config['base_url']  = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $config['first_url'] = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $cetak=base_url() . 'sppd/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
        } else {
            $config['base_url']  = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d);
            $config['first_url'] = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d);
            $cetak=base_url() . 'sppd/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $config['total_rows']        = $this->MSppd->total_rows($kd_skpd,$nm_sub_unit,$jenis_sp2d,$q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $sppd                         = $this->MSppd->get_limit_data($kd_skpd,$nm_sub_unit,$jenis_sp2d,$config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->MSppd->getSumdetail($kd_skpd,$nm_sub_unit,$q,$jenis_sp2d);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $rekening=$this->db->query("select distinct kd_rek_gabung,nm_rek_5
                                    from tb_spe_sp2d
                                    where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit' and jenis_sp2d='$jenis_sp2d'
                                    order by nm_rek_5 asc")->result();
        $data = array(
			'sppd_data'    => $sppd,
			'q'           => $q,
			'pagination'  => $this->pagination->create_links(),
			'total_rows'  => $config['total_rows'],
			'start'       => $start,
			'title'       => 'Surat Perintah Pencairan Dana (SP2D)',
			'akses'       => $akses,
			'action'      => base_url().'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d),
			'kd_skpd'     =>$kd_skpd,
			'nm_sub_unit' =>$nm_sub_unit,
			'jenis_sp2d'  =>$jenis_sp2d,
			'kembali'     => base_url().'sppd',
            'cetak'=>$cetak,
            'rekening'      =>$rekening,
            'kd_rek_gabung' =>$kd_rek_gabung,
            'nilai'         =>number_format($nilai,'2',',','.'),
            'tanggal1'      =>$tanggal1,
            'tanggal2'      =>$tanggal2,

        );
        // echo $data['kd_skpd'];
        $this->template->load('layout','sppd/view_indexdua',$data);
    }*/

    public function cetak()
    {
		$akses       = $this->cekAkses('read');
        $res   =$this->input->get();
        $kd_skpd     =urldecode($res['kd_skpd']);
        $nm_sub_unit =urldecode($res['nm_sub_unit']);
		$jenis_sp2d  =urldecode($this->input->get('jenis_sp2d',true));

        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $sppd                         = $this->MSppd->get_all_data($kd_skpd,$nm_sub_unit,$jenis_sp2d, $q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->MSppd->getSumdetail($kd_skpd,$nm_sub_unit,$q,$jenis_sp2d);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $data = array(
			'sppd_data'    => $sppd,
            'nilai'         =>number_format($nilai,'2',',','.'),

        );
        $this->load->view('sppd/excel',$data);
    }

    function pihakketiga(){
        $url   ='Sppd.pihakketiga';
        $akses =cek($this->id_pengguna,$url,'read');
        $start = intval($this->input->get('start'));
        $q     =urldecode($this->input->get('q',true));
        

        $kd_skpd=null;
        $nm_sub_unit=null;
        $jenis_sp2d=null;

         if ($q<> '') {
            $config['base_url']  = base_url() . 'sppd/pihakketiga?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sppd/pihakketiga?q=' . urlencode($q);
            
        } else {
              $config['base_url']  = base_url() . 'sppd/pihakketiga';
              $config['first_url'] = base_url() . 'sppd/pihakketiga';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;

        // $sppd=$this->MSppd->getdetailsp2d($kd_skpd,$rekening,$jenis_sp2d);
        
        $config['total_rows']        = $this->MSppd->total_rows($kd_skpd,$nm_sub_unit,$jenis_sp2d,$q);
        $sppd                         = $this->MSppd->get_limit_datamonitor($kd_skpd,$nm_sub_unit,$jenis_sp2d,$config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $total=$this->MSppd->getSumdetail($kd_skpd,$nm_sub_unit,$q,$jenis_sp2d);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        
        $data = array(
            'sppd_data'     => $sppd,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Monitor Pihak Ke 3 (tiga)',
            'akses'         => $akses,
            'action'        => base_url().'sppd/pihakketiga',
            'nilai'         =>number_format($nilai,'2',',','.'),
        
        );

        $this->template->load('layout','sppd/monitorPihaktiga',$data);   
    }

    function sppdvsspj(){
        $url   ='Sppd.sppdvsspj';
        $akses =cek($this->id_pengguna,$url,'read');
        $opd=$this->db->query("select 
                                distinct kd_skpd,ltrim(nm_sub_unit) nm_sub_unit
                                from tb_spe_sp2d where jenis_sp2d in ('TU','GU')
                                order by  nm_sub_unit asc")->result();
        $q=$this->input->get('q');
        $jenis=$this->input->get('jenis');

        if($q<>'' || $jenis<>''){
            $sppd =$this->MSppd->sppdTu($q,$jenis);
            $spj  =$this->MSppd->spjTu($q,$jenis);
            $mapping=$this->MSppd->mappingsp2dvsspj($q);
        }else{
            $sppd    =[];
            $spj     =[];
            $mapping =[];
        }

        $data=array(
                'title'   =>'Monitoring TGL SP2D vs TGL SPJ',
                'action'  =>base_url().'sppd/sppdvsspj',
                'subunit' =>$opd,
                'q'       =>$q,
                'jenis'   =>$jenis,
                'sppd'    =>$sppd,
                'spj'     =>$spj,
                'script'  =>'sppd/jsvs',
                'mapping' =>$mapping
            );
        $this->template->load('layout','sppd/sp2dvsspj',$data);   
    }   

    function prosesmappingspj(){
        $url   ='Sppd.sppdvsspj';
        $akses =cek($this->id_pengguna,$url,'read');

        $res=$this->input->post();

        /*if(!empty($res['sppd']) && !empty($res['spj']) ){
        echo "<pre>";
        print_r($res);
        echo "</pre>";

    }else{
        echo "sp2d dan spj tidak boleh kosong";
    }
        die();*/

        if(!empty($res['sppd']) && !empty($res['spj']) ){
            
            $exp=explode('#', $res['sppd']);

            for($i=0;$i<count($res['spj']);$i++){
            $pxe=explode('#',$res['spj'][$i]);
            $data=array(
                'SP2D_Tahun'         =>$exp[0],
                'SP2D_Kode_Unit'     =>$exp[1],
                'SP2D_Kd_SKPD'       =>$exp[2],
                'SP2D_Nm_Unit'       =>$exp[3],
                'SP2D_Nm_Sub_Unit'   =>$exp[4],
                'SP2D_ID_Prog'       =>$exp[5],
                'SP2D_Ket_Program'   =>$exp[6],
                'SP2D_Kd_Keg'        =>$exp[7],
                'SP2D_Ket_Kegiatan'  =>$exp[8],
                'SP2D_Tgl_SP2D'      =>$exp[9],
                'SP2D_No_SP2D'       =>$exp[10],
                'SP2D_Keterangan'    =>$exp[11],
                'SP2D_Jenis_SP2D'    =>$exp[12],
                'SP2D_Nilai'         =>$exp[13],
                'SP2D_Kd_Rek_1'      =>$exp[14],
                'SP2D_Kd_Rek_2'      =>$exp[15],
                'SP2D_Kd_Rek_3'      =>$exp[16],
                'SP2D_Kd_Rek_4'      =>$exp[17],
                'SP2D_Kd_Rek_5'      =>$exp[18],
                'SP2D_Kd_Rek_Gabung' =>$exp[19],
                'SP2D_Nm_Rek_5'      =>$exp[20],
                'SP2D_Nm_Penerima'   =>$exp[21],
                'SP2D_Rek_Penerima'  =>$exp[22],
                'SP2D_Bank_Penerima' =>$exp[23],
                'SP2D_NPWP'          =>$exp[24],
                'SP2D_Tgl_SPM'       =>$exp[25],
                'SP2D_No_SPM'        =>$exp[26],
                'SP2D_Id_Bukti'      =>$exp[27],
                'SPJ_Tahun'          =>$pxe[0],
                'SPJ_Kd_SKPD'        =>$pxe[1],
                'SPJ_Nm_Unit'        =>$pxe[2],
                'SPJ_Nm_Sub_Unit'    =>$pxe[3],
                'SPJ_ID_Prog'        =>$pxe[4],
                'SPJ_Ket_Program'    =>$pxe[5],
                'SPJ_Kd_Keg'         =>$pxe[6],
                'SPJ_Ket_Kegiatan'   =>$pxe[7],
                'SPJ_Tgl_SPJ'        =>$pxe[8],
                'SPJ_No_SPJ'         =>$pxe[9],
                'SPJ_Tgl_Bukti'      =>$pxe[10],
                'SPJ_No_Bukti'       =>$pxe[11],
                'SPJ_Tgl_Pengesahan' =>$pxe[12],
                'SPJ_No_Pengesahan'  =>$pxe[13],
                'SPJ_Keterangan'     =>$pxe[14],
                'SPJ_Jenis_SPJ'      =>$pxe[15],
                'SPJ_Nilai'          =>$pxe[16],
                'SPJ_Kd_Rek_1'       =>$pxe[17],
                'SPJ_Kd_Rek_2'       =>$pxe[18],
                'SPJ_Kd_Rek_3'       =>$pxe[19],
                'SPJ_Kd_Rek_4'       =>$pxe[20],
                'SPJ_Kd_Rek_5'       =>$pxe[21],
                'SPJ_Kd_Rek_Gabung'  =>$pxe[22],
                'SPJ_Nm_Rek_5'       =>$pxe[23],
                'SPJ_Uraian'         =>$pxe[24],
                'SPJ_Id_Bukti'       =>$pxe[25],
                );

                set_flashdata('success','Data berhasil di simpan.');
                $this->db->insert('SP2D_VS_SPJ',$data);
            }
        }else{
            set_flashdata('warning','Data harus memilih SPPD dan SPJ.');
        }

        redirect($_SERVER['HTTP_REFERER']);
        
    }

    function hapusmaping(){
        $id=$this->input->get('p');
        $this->db->where('id',$id);
        $this->db->delete('SP2D_VS_SPJ');
        set_flashdata('success','Data berhasil di hapus.');
        redirect($_SERVER['HTTP_REFERER']);
    }


}

/* End of file Sppd.php */
/* Location: ./application/controllers/Sppd.php */