<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Anggaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Manggaran');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Anggaran';
        return cek($this->id_pengguna,$url,$var);
    }


    public function realisasi(){
        $url='Anggaran.realisasi';
        $akses=cek($this->id_pengguna,$url,'read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'anggaran/realisasi?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'anggaran/realisasi?q=' . urlencode($q);
            $cetak=base_url() . 'anggaran/cetak_realisasi?q='.urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'anggaran/realisasi';
            $config['first_url'] = base_url() . 'anggaran/realisasi';
            $cetak=base_url() . 'anggaran/cetak_realisasi';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Manggaran->getRowRealisasi($q);
        $anggaran             = $this->Manggaran->getDataRealisasi($config['per_page'], $start, $q);
        /*echo $this->db->last_query();
        die();*/
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $subunit=$this->Manggaran->getSUbunit();

        // $total=$this->Manggaran->getSum($q);
        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Realisasi Anggaran',
            'akses'         => $akses,
            'subunit'       =>$subunit,
            'cetak'         => $cetak
        );
        $this->template->load('layout','anggaran/view_realisasi',$data);
    }
    public function cetak_realisasi(){
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $anggaran             = $this->Manggaran->getAllDataRealisasi($q);
        $data = array(
            'anggaran_data' => $anggaran,
        );
        $this->load->view('anggaran/cetak_realisasi',$data);
    }
    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'anggaran?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'anggaran?q=' . urlencode($q);
            $cetak=base_url() . 'anggaran/cetak_index?q='.urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'anggaran';
            $config['first_url'] = base_url() . 'anggaran';
            $cetak=base_url() . 'anggaran/cetak_index';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Manggaran->getRow($q);
        $anggaran                    = $this->Manggaran->getAllData($q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $subunit=$this->Manggaran->getSUbunit();

        $total=$this->Manggaran->getSum($q);
        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Anggaran',
            'akses'         => $akses,
            'subunit'       =>$subunit,
            'awal'          => number_format($total->awal,'2',',','.'),
            'satu'          => number_format($total->satu,'2',',','.'),
            'dua'           => number_format($total->dua,'2',',','.'),
            'cetak'         => $cetak
        );
        $this->template->load('layout','anggaran/view_index',$data);
    }

    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $anggaran                      = $this->Manggaran->getAllData($q);
        $total=$this->Manggaran->getSum($q);
        $data = array(
            'anggaran_data' => $anggaran,
            'awal' => number_format($total->awal,'2',',','.'),
            'satu' => number_format($total->satu,'2',',','.'),
            'dua' => number_format($total->dua,'2',',','.'),
        );
        $this->load->view('anggaran/cetak_index',$data);
    }

     public function readtiga()
    {
        $akses         =$this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       = urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit',true));
        $akun_akrual_3 = urldecode($this->input->get('akun_akrual_3',true));
        $nm_akrual_3   = urldecode($this->input->get('nm_akrual_3',true));
        $akun_akrual_5 = urldecode($this->input->get('akun_akrual_5',true));

        if ($akun_akrual_5 <>'' || $q <> '') {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3).'&akun_akrual_5='.urlencode($akun_akrual_5).'&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3).'&akun_akrual_5='.urlencode($akun_akrual_5).'&q=' . urlencode($q);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3).'&akun_akrual_5='.urlencode($akun_akrual_5).'&q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3);
        }

        $config['per_page']          = 2000;
        $config['page_query_string'] = TRUE;
        
        if($akun_akrual_5 <>'' ){
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }
        
        $config['total_rows']        = $this->Manggaran->total_rows_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$q);
        if($akun_akrual_5<>''){
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }
        $anggaran                    = $this->Manggaran->get_limit_data_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$config['per_page'], $start, $q);
        /*echo $this->db->last_query();
        die();*/

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        if($akun_akrual_5<>''){
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }
        $total=$this->Manggaran->getSumdetail_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$q);


        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Detail Anggaran',
            'akses'         => $akses,
            'action'        => base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3),
            'kembali'       => base_url().'anggaran/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
            'akun_akrual_5' => $akun_akrual_5,
            'akun_akrual_3' =>$akun_akrual_3,
            'nm_akrual_3'   =>$nm_akrual_3,
            'awal'          => number_format($total->awal,'2',',','.'),
            'satu'          => number_format($total->satu,'2',',','.'),
            'dua'           => number_format($total->dua,'2',',','.'),
            'cetak'         =>$cetak,
        );
        $this->template->load('layout','anggaran/view_indextiga',$data);
    }

   
    public function read()
    {
        $akses         =$this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       = urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit',true));
        $akun_akrual_3 = urldecode($this->input->get('akun_akrual_3',true));

        if ($q <> '' || $akun_akrual_3<>'' || $q <> '') {
            $config['base_url']  = base_url() . 'anggaran/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'anggaran/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&q=' . urlencode($q);
            $cetak               = base_url() . 'anggaran/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'anggaran/read?kd_skpd='.$kd_skpd.'&nm_sub_unit='.$nm_sub_unit;
            $config['first_url'] = base_url() . 'anggaran/read?kd_skpd='.$kd_skpd.'&nm_sub_unit='.$nm_sub_unit;
            $cetak               =base_url() . 'anggaran/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($akun_akrual_3<>''){
            $this->db->where('akun_akrual_3',$akun_akrual_3);
        }
        $config['total_rows']        = $this->Manggaran->total_rows($kd_skpd,$nm_sub_unit,$q);
        if($akun_akrual_3<>''){
            $this->db->where('akun_akrual_3',$akun_akrual_3);
        }
        $anggaran                    = $this->Manggaran->get_limit_data($kd_skpd,$nm_sub_unit,$config['per_page'], $start, $q);


        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $akrual3=$this->db->query("select distinct nm_akrual_3,akun_akrual_3
                                    from tb_spe_anggaran
                                    where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit' order by akun_akrual_3 asc")->result();


        if($akun_akrual_3<>''){
            $this->db->where('akun_akrual_3',$akun_akrual_3);
        }
        $total=$this->Manggaran->getSumdetail($kd_skpd,$nm_sub_unit,$q);


        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Anggaran',
            'akses'         => $akses,
            'action'        =>base_url() . 'anggaran/read?kd_skpd='.$kd_skpd.'&nm_sub_unit='.$nm_sub_unit,
            'kembali'       =>base_url().'anggaran',
            'kd_skpd'       =>$kd_skpd,
            'nm_sub_unit'   =>$nm_sub_unit,
            'akrual3'       =>$akrual3,
            'akun_akrual_3' =>$akun_akrual_3,
            'awal'          => number_format($total->awal,'2',',','.'),
            'satu'          => number_format($total->satu,'2',',','.'),
            'dua'           => number_format($total->dua,'2',',','.'),
            'cetak'         =>$cetak,
        );
        $this->template->load('layout','anggaran/view_indexdua',$data);
    }

    public function cetak()
    {
        $akses         =$this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       =urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit   =urldecode($this->input->get('nm_sub_unit',true));
        $akun_akrual_3 =urldecode($this->input->get('akun_akrual_3',true));

        if($akun_akrual_3<>''){
            $this->db->where('akun_akrual_3',$akun_akrual_3);
        }
        $config['total_rows']        = $this->Manggaran->total_rows($kd_skpd,$nm_sub_unit,$q);

        if($akun_akrual_3<>''){
            $this->db->where('akun_akrual_3',$akun_akrual_3);
        }
        $anggaran                    = $this->Manggaran->get_data_3($kd_skpd,$nm_sub_unit, $q);
        if($akun_akrual_3<>''){
            $this->db->where('akun_akrual_3',$akun_akrual_3);
        }
    
        $total=$this->Manggaran->getSumdetail($kd_skpd,$nm_sub_unit,$q);
        $data = array(
            'anggaran_data' => $anggaran,
            'kd_skpd'       =>$kd_skpd,
            'nm_sub_unit'   =>$nm_sub_unit,
            'awal'          => number_format($total->awal,'2',',','.'),
            'satu'          => number_format($total->satu,'2',',','.'),
            'dua'           => number_format($total->dua,'2',',','.'),
        );

        $this->load->view('anggaran/excel',$data);
    }

    public function cetaktiga()
    {
        $akses         =$this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       = urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit',true));
        $akun_akrual_3 = urldecode($this->input->get('akun_akrual_3',true));
        $nm_akrual_3   = urldecode($this->input->get('nm_akrual_3',true));
        $akun_akrual_5 = urldecode($this->input->get('akun_akrual_5',true));

        if ($akun_akrual_5 <>'' || $q <> '') {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3).'&akun_akrual_5='.urlencode($akun_akrual_5).'&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3).'&akun_akrual_5='.urlencode($akun_akrual_5).'&q=' . urlencode($q);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3).'&akun_akrual_5='.urlencode($akun_akrual_5).'&q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3);
        }

        $config['per_page']          = 2000;
        $config['page_query_string'] = TRUE;
        
        if($akun_akrual_5 <>'' ){
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }
        
        $config['total_rows']        = $this->Manggaran->total_rows_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$q);
        if($akun_akrual_5<>''){
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }
        $anggaran                    = $this->Manggaran->get_limit_data_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$config['per_page'], $start, $q);
        /*echo $this->db->last_query();
        die();*/

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        if($akun_akrual_5<>''){
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }
        $total=$this->Manggaran->getSumdetail_tiga($kd_skpd,$nm_sub_unit,$akun_akrual_3,$q);


        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,
            // 'pagination'    => $this->pagination->create_links(),
            // 'total_rows'    => $config['total_rows'],
            // 'start'         => $start,
            'title'         => 'Detail Anggaran',
            'akses'         => $akses,
            // 'action'        => base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3),
            // 'kembali'       => base_url().'anggaran/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
            'akun_akrual_5' => $akun_akrual_5,
            'akun_akrual_3' =>$akun_akrual_3,
            'nm_akrual_3'   =>$nm_akrual_3,
            'awal'          => number_format($total->awal,'2',',','.'),
            'satu'          => number_format($total->satu,'2',',','.'),
            'dua'           => number_format($total->dua,'2',',','.'),
            // 'cetak'         =>$cetak,
        );
        $this->load->view('anggaran/exceltiga',$data);
    }

}

/* End of file Anggaran.php */
/* Location: ./application/controllers/Anggaran.php */