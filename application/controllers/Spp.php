<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spp extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Mspp');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Spp';
        return cek($this->id_pengguna,$url,$var);
    }


     public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        if ($q <> '') {
            $cetak=base_url() . 'spp/cetak_index?q='.urlencode($q);
            $config['base_url']  = base_url() . 'spp?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spp?q=' . urlencode($q);
        } else {
            $cetak=base_url() . 'spp/cetak_index';
            $config['base_url']  = base_url() . 'spp?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spp?q=' . urlencode($q);
        }
        // 

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mspp->getRow($q);
        
        $spp                  = $this->Mspp->getalldata($config['per_page'], $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $subunit =$this->Mspp->getSUbunit();
        $data = array(
            'spp_data'     => $spp,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'    => 'Surat Permintaan Pembayaran (SPP)',
            'subunit'      => $subunit,
            'cetak'        => $cetak,
            // 'script'=>'report/datatables'
        );
        $this->template->load('layout','spp/view_index',$data);
    }


   /* public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $cetak               = base_url() . 'spp/cetak_index?q='.urlencode($q);
        } else {
            $cetak               = base_url() . 'spp/cetak_index';
        }

        $spp                      = $this->Mspp->getAllData($q);

        $subunit=$this->Mspp->getSUbunit();
        $data = array(
            'spp_data' => $spp,
            'q'        => $q,
            'start'    => $start,
            'title'    => 'Surat Permintaan Pembayaran (SPP)',
            'akses'    => $akses,
            'subunit'  => $subunit,
            'cetak'    => $cetak
        );
        $this->template->load('layout','spp/view_index',$data);
    }*/
    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        if ($q <> '') {
            $cetak=base_url() . 'spp/cetak_index?q='.urlencode($q);
            $config['base_url']  = base_url() . 'spp?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spp?q=' . urlencode($q);
        } else {
            $cetak=base_url() . 'spp/cetak_index';
            $config['base_url']  = base_url() . 'spp?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spp?q=' . urlencode($q);
        }
        // 

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mspp->getRow($q);
        
        $spp                  = $this->Mspp->getalldata(10000, $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $subunit =$this->Mspp->getSUbunit();
        $data = array(
            'spp_data'     => $spp,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'    => 'Surat Permintaan Pembayaran (SPP)',

        );
        $this->load->view('spp/cetak_index',$data);
    }

    public function read()
    {
        $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $spp     = $this->Mspp->get_limit_data($nm_unit);
        $start   = intval($this->input->get('start'));
        $data    = array(
            'spp_data' => $spp,
            'title'     => 'Surat Permintaan Pembayaran (SPP)',
            'akses'     => $akses,
            'nm_unit'   => $nm_unit,
            'kembali'   => base_url().'spp',
            'start'     => $start,
            'cetak'     => base_url().'spp/cetakdua?nm_unit='.$nm_unit
        );
        $this->template->load('layout','spp/view_indexdua',$data);
     }

     function cetakdua(){
         $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $spp    = $this->Mspp->get_limit_data($nm_unit);
        $start   = intval($this->input->get('start'));
        $data = array(
            'spp_data' => $spp,
            'title'     => 'Surat Permintaan Pembayaran (SPP)',
            'nm_unit'   => $nm_unit,
            'start'     => $start,
        );
        
        $this->load->view('spp/cetak_indexdua',$data);
     }


 /*    function readdua(){
        $akses       = $this->cekAkses('read');
        $kd_skpd     =$this->input->get('kd_skpd',true);
        $nm_unit     =$this->input->get('nm_unit',true);
        $nm_sub_unit =$this->input->get('nm_sub_unit',true);
        $rekening    =$this->input->get('rekening',true);
        $jenis_spp  =$this->input->get('jenis_spp',true);

        $start       = intval($this->input->get('start'));
        
        $spp=$this->Mspp->getdetailspp($kd_skpd,$rekening,$jenis_spp);
        $data = array(
            'spp_data'    => $spp,
            'title'       => 'Surat Permintaan Pembayaran (SPP)',
            'akses'       => $akses,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     =>$kd_skpd,
            'nm_sub_unit' =>$nm_sub_unit,
            'jenis'       =>$jenis_spp,
            'kembali'     => base_url().'spp/read?nm_unit='.urlencode($nm_unit),
            'start'       => $start,
            'script'      =>'report/datatables',
            'cetak'       =>base_url().'spp/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_unit='.urlencode($nm_unit).'&nm_sub_unit='.urlencode($nm_sub_unit).'&rekening='.urlencode($rekening).'&jenis_spp='.urlencode($jenis_spp)
        );

        $this->template->load('layout','spp/view_indextiga',$data);   
    }
*/


    function readdua(){
        $akses       = $this->cekAkses('read');
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_unit     =urldecode($this->input->get('nm_unit',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $no_spp      =urldecode($this->input->get('no_spp',true));
        $start       = intval($this->input->get('start'));
        
        $spp=$this->Mspp->getdetailspp($kd_skpd,$nm_unit,$nm_sub_unit,$no_spp);
        $data = array(
            'spp_data'    => $spp,
            'title'       => 'Surat Permintaan Pembayaran (SPP)',
            'akses'       => $akses,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spp'       => $no_spp,
            'kembali'     => base_url().'spp?q='.urlencode($nm_unit),
            'start'       => $start,
            'script'      => 'report/datatables',
            'cetak'       => base_url().'spp/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_unit='.urlencode($nm_unit).'&nm_sub_unit='.urlencode($nm_sub_unit).'&no_spp='.urlencode($no_spp)
        );

        $this->template->load('layout','spp/view_indextiga',$data);   
    }

    function cetaktiga(){
         $akses       = $this->cekAkses('read');
         $akses       = $this->cekAkses('read');
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_unit     =urldecode($this->input->get('nm_unit',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $no_spp      =urldecode($this->input->get('no_spp',true));
        $start       = intval($this->input->get('start'));
        
        $spp=$this->Mspp->getdetailspp($kd_skpd,$nm_unit,$nm_sub_unit,$no_spp);
        $data = array(
            'spp_data'    => $spp,
            'title'       => 'Surat Permintaan Pembayaran (SPP)',
            'akses'       => $akses,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spp'       => $no_spp,
            'kembali'     => base_url().'spp?q='.urlencode($nm_unit),
            'start'       => $start,
            'script'      => 'report/datatables',
            'cetak'       => base_url().'spp/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_unit='.urlencode($nm_unit).'&nm_sub_unit='.urlencode($nm_sub_unit).'&no_spp='.urlencode($no_spp)
        );

        $this->load->view('spp/cetak_indextiga',$data);  
    }

    function excel(){
        $akses         =$this->cekAkses('read');
        $res           =$this->input->get();
        $kd_skpd       =urldecode($res['kd_skpd']);
        $nm_sub_unit   =urldecode($res['nm_sub_unit']);
        $jenis_spp     =urldecode($res['jenis_spp']);
        $start         = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }
        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $spp = $this->Mspp->get_limit_data($kd_skpd, $nm_sub_unit, $jenis_spp,$q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }
        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }

        $total=$this->Mspp->getSumdetail($kd_skpd,$nm_sub_unit,$q,$jenis_spp);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $data = array(
            'spp_data'      => $spp,
            'nilai'         =>number_format($nilai,'2',',','.'),
        );
        $this->load->view('spp/excel',$data);
    }


}

/* End of file Spp.php */
/* Location: ./application/controllers/Spp.php */