<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Akrualrek extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Makrual');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Akrualrek';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']  = base_url() . 'akrualrek?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'akrualrek?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'akrualrek';
            $config['first_url'] = base_url() . 'akrualrek';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Makrual->total_rows($q);
        $akrualrek                      = $this->Makrual->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'akrualrek_data' => $akrualrek,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Ref Rekening Akrual',
            'akses'               => $akses
        );
        $this->template->load('layout','akrualrek/view_index',$data);
    }

  

}

/* End of file Akrualrek.php */
/* Location: ./application/controllers/Akrualrek.php */