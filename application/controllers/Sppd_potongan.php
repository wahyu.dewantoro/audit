<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sppd_potongan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Msppdpot');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Sppd_potongan';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'sppd_potongan?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sppd_potongan?q=' . urlencode($q);
            $cetak = base_url() . 'sppd_potongan/cetak_index?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'sppd_potongan';
            $config['first_url'] = base_url() . 'sppd_potongan';
            $cetak = base_url() . 'sppd_potongan/cetak_index';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Msppdpot->gettotal($q);
        $sppd_potongan               = $this->Msppdpot->getalldata($q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $total=$this->Msppdpot->getSumNilai($q);
        $subunit=$this->Msppdpot->getSUbunit();

        $data = array(
            'sppd_potongan_data' => $sppd_potongan,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Potongan Surat Perintah Pencairan Dana (SP2D)',
			'akses'                 => $akses,
            'subunit'               =>$subunit,
            'cetak'                 => $cetak,
            'jum_nilai'             =>number_format($total->jum_nilai,'2',',','.'),
        );
        $this->template->load('layout','sppd_potongan/view_index',$data);
    }
    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $config['total_rows']        = $this->Msppdpot->gettotal($q);
        $sppd_potongan               = $this->Msppdpot->getalldata($q);
        $total=$this->Msppdpot->getSumNilai($q);
        $data = array(
            'sppd_potongan_data' => $sppd_potongan,
            'jum_nilai'             =>number_format($total->jum_nilai,'2',',','.'),
        );
        $this->load->view('sppd_potongan/cetak_index',$data);
    }
    public function read()
    {
        $akses =$this->cekAkses('read');
        $res   =$this->input->get();
        $Kd_SKPD     =urldecode($res['Kd_SKPD']);
        $Nm_Sub_Unit =urldecode($res['Nm_Sub_Unit']);

        // $q           = urldecode($this->input->get('q', TRUE));

        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));


        if($tanggal1==''){
            $tanggal1='01/01/'.date('Y');
        }

        if($tanggal2==''){
            $tanggal2=date('d/m/Y');
        }

         if ($kd_rek_gabung<>'' || $q<> '' || $tanggal1 <> '' || $tanggal2<>'') {
            $config['base_url']  = base_url() . 'sppd_potongan/read?Kd_SKPD='.urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $config['first_url'] = base_url() . 'sppd_potongan/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $cetak               =base_url() . 'sppd_potongan/cetak?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
        } else {
            $config['base_url']  = base_url() . 'sppd_potongan/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit);
            $config['first_url'] = base_url() . 'sppd_potongan/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit);
            $cetak               = base_url() . 'sppd_potongan/cetak?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $config['total_rows']        = $this->Msppdpot->total_rows($Kd_SKPD,$Nm_Sub_Unit,$q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $sp2d_potongan                      = $this->Msppdpot->get_limit_data($Kd_SKPD,$Nm_Sub_Unit,$config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Msppdpot->getSumdetail($Kd_SKPD,$Nm_Sub_Unit,$q);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $rekening=$this->db->query("select distinct kd_rek_gabung,nm_rek_5
                                    from tb_spe_sp2d_potongan
                                    where kd_skpd='$Kd_SKPD' and nm_sub_unit='$Nm_Sub_Unit'
                                    order by nm_rek_5 asc")->result();
        $data = array(
            'sppd_potongan_data' => $sp2d_potongan,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'        => 'Potongan Surat Perintah Pencairan Dana (SP2D)',
            'akses'        => $akses,
            'Kd_SKPD'      =>$Kd_SKPD,
            'Nm_Sub_Unit'  =>$Nm_Sub_Unit,
            'action'       =>base_url() . 'sppd_potongan/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit),
            'kembali'=>base_url().'sppd_potongan',
            'cetak'         =>$cetak,
            'rekening'      =>$rekening,
            'kd_rek_gabung' =>$kd_rek_gabung,
            'nilai'         =>number_format($nilai,'2',',','.'),
            'tanggal1'      =>$tanggal1,
            'tanggal2'      =>$tanggal2,

        );
        $this->template->load('layout','sppd_potongan/view_indexdua',$data);
    }
    public function cetak()
    {
        $akses =$this->cekAkses('read');
        $res   =$this->input->get();
        $Kd_SKPD     =urldecode($res['Kd_SKPD']);
        $Nm_Sub_Unit =urldecode($res['Nm_Sub_Unit']);
        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }
        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $sp2d_potongan= $this->Msppdpot->get_all_data($Kd_SKPD,$Nm_Sub_Unit, $q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Msppdpot->getSumdetail($Kd_SKPD,$Nm_Sub_Unit,$q);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $data = array(
            'sppd_potongan_data' => $sp2d_potongan,
            'nilai'         =>number_format($nilai,'2',',','.'),

        );
        $this->load->view('sppd_potongan/excel',$data);
    }
}

/* End of file Sppd_potongan.php */
/* Location: ./application/controllers/Sppd_potongan.php */