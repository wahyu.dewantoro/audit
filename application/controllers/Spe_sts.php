<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spe_sts extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('MSpe_sts');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Spe_sts';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $cetak=base_url() . 'spe_sts/cetak_index?q='.urlencode($q);
            $config['base_url']  = base_url() . 'spe_sts?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spe_sts?q=' . urlencode($q);
            
        } else {
            
            $cetak=base_url() . 'spe_sts/cetak_index';
            $config['base_url']  = base_url() . 'spe_sts';
            $config['first_url'] = base_url() . 'spe_sts';
        }


        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->MSpe_sts->gettotal($q);
        $spe_sts                      = $this->MSpe_sts->getalldata($config['per_page'], $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $subunit =$this->MSpe_sts->getSubunit();
        $data = array(
            'spe_sts_data' => $spe_sts,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'        => 'Surat Tanda Setoran (STS)',
            'akses'        => $akses,
            'subunit'      => $subunit,
            'cetak'        => $cetak,
            // 'script'       =>'report/datatables'
        );
        $this->template->load('layout','spe_sts/view_index',$data);
    }
    public function cetak_index()
    {
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $cetak=base_url() . 'spe_sts/cetak_index?q='.urlencode($q);
            $config['base_url']  = base_url() . 'spe_sts?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spe_sts?q=' . urlencode($q);
            
        } else {
            $cetak=base_url() . 'spe_sts/cetak_index';
            $config['base_url']  = base_url() . 'spe_sts';
            $config['first_url'] = base_url() . 'spe_sts';
        }


        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->MSpe_sts->gettotal($q);
        $spe_sts                      = $this->MSpe_sts->getalldata(20000, $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $subunit =$this->MSpe_sts->getSubunit();
        $data = array(
            'spe_sts_data' => $spe_sts,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'        => $start,
            'title'        => 'Surat Tanda Setoran (STS)',
            // 'akses'        => $akses,
            'subunit'      => $subunit,
            'cetak'        => $cetak,
        );
        $this->load->view('spe_sts/cetak_index',$data);
    }

    function readsetengah(){
        $akses       =$this->cekAkses('read');
        $Kd_SKPD     =urldecode($this->input->get('Kd_SKPD',true));
        $Nm_Sub_Unit =urldecode($this->input->get('Nm_Sub_Unit',true));
        $start       =intval($this->input->get('start',true));

        $sts=$this->MSpe_sts->getListRektiga($Kd_SKPD);
        $data=array(
            'title'       => 'Surat Tanda Setoran (STS)',
            'akses'       => $akses,
            'data'        => $sts,
            'kd_skpd'     =>$Kd_SKPD,
            'nm_sub_unit' =>$Nm_Sub_Unit,
            'kembali'     =>base_url().'spe_sts',
            'start'       => $start,
            'script'=>'report/datatables'
            );
        $this->template->load('layout','spe_sts/view_indexsetengah',$data);
    }

    public function read()
    {
        $akses       =$this->cekAkses('read');
        
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_unit     =urldecode($this->input->get('nm_unit',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $no_sts      =urldecode($this->input->get('no_sts',true));
        $start       =intval($this->input->get('start'));
        
        $spe_sts=$this->db->query("SELECT   Tgl_STS
                                          ,No_STS
                                          ,Keterangan
                                          ,Kd_Rek_Gabung
                                          ,Nm_Rek_5  ,Nilai
                                      FROM e_audit.dbo.SPE_STS
                                      where no_sts='$no_sts' and kd_skpd='$kd_skpd'")->result();

        $data = array(
            'spe_sts_data'  => $spe_sts,
            'start'       => $start,
            'title'       => 'Surat Tanda Setoran (STS)',
            'akses'       => $akses,
            'Kd_SKPD'     =>$kd_skpd,
            'Nm_Sub_Unit' =>$nm_sub_unit,
            'Nm_Unit'     =>$nm_unit,
            
            'kembali'       =>base_url().'spe_sts/index?q='.urlencode($nm_unit),
            
        );
        $this->template->load('layout','spe_sts/view_indexdua',$data);


    }

    function cetak(){
        $akses         =$this->cekAkses('read');
        // $res        =$this->input->get();
        $Kd_SKPD       =urldecode($this->input->get('Kd_SKPD',true));
        $Nm_Sub_Unit   =urldecode($this->input->get('Nm_Sub_Unit',true));
        $start         =intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        $rekening=urldecode($this->input->get('rekening',true));

        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }


        $this->db->where("convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) like '%$rekening%'",null,false);
        $config['total_rows']        = $this->MSpe_sts->total_rows($Kd_SKPD,$Nm_Sub_Unit,$q);

        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

         if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);

        }

        $this->db->where("convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) like '%$rekening%'",null,false);
        $spe_sts = $this->MSpe_sts->get_all_data($Kd_SKPD,$Nm_Sub_Unit, $q);

        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

         if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);

        }

        $this->db->where("convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) like '%$rekening%'",null,false);
        $total=$this->MSpe_sts->getSumdetail($Kd_SKPD,$Nm_Sub_Unit,$q);
        $data = array(
            'spe_sts_data'  => $spe_sts,
            'nilai'         =>number_format($total->nilai,'2',',','.'),

        );
        $this->load->view('spe_sts/cetak_excel',$data);
    }


}

/* End of file tb_spe_sts.php */
/* Location: ./application/controllers/Spe_sts.php */