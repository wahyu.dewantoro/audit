<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lodetail extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Mlodetail');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Lodetail';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'lodetail?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'lodetail?q=' . urlencode($q);
            $cetak= base_url() . 'lodetail/cetak_index?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'lodetail';
            $config['first_url'] = base_url() . 'lodetail';
            $cetak= base_url() . 'lodetail/cetak_index';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlodetail->getRow($q);
        $lodetail                      = $this->Mlodetail->getAllData($q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $total=$this->Mlodetail->getSumNilai($q);
        $subunit=$this->Mlodetail->getSUbunit();
        $data = array(
			'lodetail_data' => $lodetail,
			'cetak'				=> $cetak,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Detail LO',
            'akses'               => $akses,
            'subunit'           =>$subunit,
            'jum_debet'             =>number_format($total->jum_debet,'2',',','.'),
            'jum_kredit'            =>number_format($total->jum_kredit,'2',',','.'),
        );
        $this->template->load('layout','lodetail/view_index',$data);
    }
    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $lodetail                      = $this->Mlodetail->getAllData($q);
        $total=$this->Mlodetail->getSumNilai($q);
        $data = array(
			'lodetail_data' => $lodetail,
            'jum_debet'             =>number_format($total->jum_debet,'2',',','.'),
            'jum_kredit'            =>number_format($total->jum_kredit,'2',',','.'),
        );
        $this->load->view('lodetail/cetak_index',$data);
    }
    public function read()
    {
		$akses =$this->cekAkses('read');
		$q           = urldecode($this->input->get('q', TRUE));
		$start         = intval($this->input->get('start'));
		$kd_skpd     =urldecode($this->input->get('kd_skpd',TRUE));
		$nm_sub_unit =urldecode($this->input->get('nm_sub_unit',TRUE));
		$akun_akrual_5 =urldecode($this->input->get('akun_akrual_5',true));
		$tanggal1      =urldecode($this->input->get('tanggal1',true));
		$tanggal2      =urldecode($this->input->get('tanggal2',true));

	   if($tanggal1==''){
		   $tanggal1='01/01/'.date('Y');
	   }

	   if($tanggal2==''){
		   $tanggal2=date('d/m/Y');
	   }

	   if ($q <> '' || $akun_akrual_5<>''|| $tanggal1 <>'' || $tanggal2 <>'') {
			$config['base_url']  = base_url() . 'lodetail/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_5='.urlencode($akun_akrual_5).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2).'&q='.urlencode($q);
			$config['first_url'] = base_url() . 'lodetail/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_5='.urlencode($akun_akrual_5).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2).'&q='.urlencode($q);
			$cetak= base_url() . 'lodetail/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_5='.urlencode($akun_akrual_5).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2).'&q='.urlencode($q);
		} else {
			$config['base_url']  = base_url() . 'lodetail/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
			$config['first_url'] = base_url() . 'lodetail/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
			$cetak = base_url() . 'lodetail/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
		}

		$config['per_page']          = 10;
		$config['page_query_string'] = TRUE;
        if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
		$config['total_rows']        = $this->Mlodetail->total_rows($kd_skpd,$nm_sub_unit,$q);
        if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
		$lodetail                = $this->Mlodetail->get_limit_data($kd_skpd,$nm_sub_unit,$config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$akrual5=$this->db->query("select distinct akun_akrual_5,nm_akrual_5
									from tb_spe_detail_lo
									where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit'
									order by akun_akrual_5 asc")->result();
									if ($akun_akrual_5 <>'') {
										$this->db->where('akun_akrual_5',$akun_akrual_5);
									}

									if($tanggal1 <> '' && $tanggal2 <> '' ){
										$t1=explode('/',$tanggal1);
										$tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

										$t2=explode('/',$tanggal2);
										$tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
										$this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
									}
		$total=$this->Mlodetail->getSumNilaidetail($kd_skpd,$nm_sub_unit,$q);
		$data = array(
			'lodetail_data' => $lodetail,
			'q'                 => $q,
			'cetak'				=> $cetak,
			'pagination'        => $this->pagination->create_links(),
			'total_rows'        => $config['total_rows'],
			'start'             => $start,
			'title'             => 'Detail LO',
			'akses'             => $akses,
			'kembali'           => base_url().'lodetail',
			'kd_skpd'           =>$kd_skpd,
			'nm_sub_unit'       =>$nm_sub_unit,
			'action'            =>base_url() . 'lodetail/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
			'akrual5'           =>$akrual5,
			'akun_akrual_5'=>$akun_akrual_5,
			'jum_debet'             =>number_format($total->jum_debet,'2',',','.'),
		   'jum_kredit'            =>number_format($total->jum_kredit,'2',',','.'),
		   'tanggal1' =>$tanggal1,
		   'tanggal2' =>$tanggal2,


		);
		$this->template->load('layout','lodetail/view_indexdua',$data);
    }

    public function cetak()
    {
		$akses =$this->cekAkses('read');
		$q           = urldecode($this->input->get('q', TRUE));
		$start         = intval($this->input->get('start'));
		$kd_skpd     =urldecode($this->input->get('kd_skpd',TRUE));
		$nm_sub_unit =urldecode($this->input->get('nm_sub_unit',TRUE));
		$akun_akrual_5 =urldecode($this->input->get('akun_akrual_5',true));
		$tanggal1      =urldecode($this->input->get('tanggal1',true));
		$tanggal2      =urldecode($this->input->get('tanggal2',true));
        if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
		$lodetail                = $this->Mlodetail->get_all_data($kd_skpd,$nm_sub_unit, $q);
		if ($akun_akrual_5 <>'') {
			$this->db->where('akun_akrual_5',$akun_akrual_5);
		}
		if($tanggal1 <> '' && $tanggal2 <> '' ){
			$t1=explode('/',$tanggal1);
			$tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];
			$t2=explode('/',$tanggal2);
			$tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
			$this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
		}
		$total=$this->Mlodetail->getSumNilaidetail($kd_skpd,$nm_sub_unit,$q);
		$data = array(
			'lodetail_data' => $lodetail,
			'jum_debet'             =>number_format($total->jum_debet,'2',',','.'),
		   'jum_kredit'            =>number_format($total->jum_kredit,'2',',','.'),
		);
		$this->load->view('lodetail/excel',$data);
    }
    public function create()
    {
        $this->cekAkses('create');
        $data = array(
            'button' => 'Simpan',
            'title'  => 'Tambah lodetail',
            'action' => site_url('lodetail/create_action'),
	    'Tahun' => set_value('Tahun'),
	    'Kd_Jurnal' => set_value('Kd_Jurnal'),
	    'Nm_Jurnal' => set_value('Nm_Jurnal'),
	    'Tgl_Bukti' => set_value('Tgl_Bukti'),
	    'No_Bukti' => set_value('No_Bukti'),
	    'No_BKU' => set_value('No_BKU'),
	    'Kd_SKPD' => set_value('Kd_SKPD'),
	    'Nm_Unit' => set_value('Nm_Unit'),
	    'Nm_Sub_Unit' => set_value('Nm_Sub_Unit'),
	    'ID_Prog' => set_value('ID_Prog'),
	    'Ket_Program' => set_value('Ket_Program'),
	    'Kd_Keg' => set_value('Kd_Keg'),
	    'Ket_Kegiatan' => set_value('Ket_Kegiatan'),
	    'Keterangan' => set_value('Keterangan'),
	    'Akun_Akrual_1' => set_value('Akun_Akrual_1'),
	    'Akun_Akrual_2' => set_value('Akun_Akrual_2'),
	    'Akun_Akrual_3' => set_value('Akun_Akrual_3'),
	    'Akun_Akrual_4' => set_value('Akun_Akrual_4'),
	    'Akun_Akrual_5' => set_value('Akun_Akrual_5'),
	    'Nm_Akrual_1' => set_value('Nm_Akrual_1'),
	    'Nm_Akrual_2' => set_value('Nm_Akrual_2'),
	    'Nm_Akrual_3' => set_value('Nm_Akrual_3'),
	    'Nm_Akrual_4' => set_value('Nm_Akrual_4'),
	    'Nm_Akrual_5' => set_value('Nm_Akrual_5'),
	    'Debet' => set_value('Debet'),
	    'Kredit' => set_value('Kredit'),
	);
        $this->template->load('layout','lodetail/view_form',$data);
    }

    public function create_action()
    {
        $this->cekAkses('create');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'Tahun' => $this->input->post('Tahun',TRUE),
		'Kd_Jurnal' => $this->input->post('Kd_Jurnal',TRUE),
		'Nm_Jurnal' => $this->input->post('Nm_Jurnal',TRUE),
		'Tgl_Bukti' => $this->input->post('Tgl_Bukti',TRUE),
		'No_Bukti' => $this->input->post('No_Bukti',TRUE),
		'No_BKU' => $this->input->post('No_BKU',TRUE),
		'Kd_SKPD' => $this->input->post('Kd_SKPD',TRUE),
		'Nm_Unit' => $this->input->post('Nm_Unit',TRUE),
		'Nm_Sub_Unit' => $this->input->post('Nm_Sub_Unit',TRUE),
		'ID_Prog' => $this->input->post('ID_Prog',TRUE),
		'Ket_Program' => $this->input->post('Ket_Program',TRUE),
		'Kd_Keg' => $this->input->post('Kd_Keg',TRUE),
		'Ket_Kegiatan' => $this->input->post('Ket_Kegiatan',TRUE),
		'Keterangan' => $this->input->post('Keterangan',TRUE),
		'Akun_Akrual_1' => $this->input->post('Akun_Akrual_1',TRUE),
		'Akun_Akrual_2' => $this->input->post('Akun_Akrual_2',TRUE),
		'Akun_Akrual_3' => $this->input->post('Akun_Akrual_3',TRUE),
		'Akun_Akrual_4' => $this->input->post('Akun_Akrual_4',TRUE),
		'Akun_Akrual_5' => $this->input->post('Akun_Akrual_5',TRUE),
		'Nm_Akrual_1' => $this->input->post('Nm_Akrual_1',TRUE),
		'Nm_Akrual_2' => $this->input->post('Nm_Akrual_2',TRUE),
		'Nm_Akrual_3' => $this->input->post('Nm_Akrual_3',TRUE),
		'Nm_Akrual_4' => $this->input->post('Nm_Akrual_4',TRUE),
		'Nm_Akrual_5' => $this->input->post('Nm_Akrual_5',TRUE),
		'Debet' => $this->input->post('Debet',TRUE),
		'Kredit' => $this->input->post('Kredit',TRUE),
	    );

            $res=$this->Mlodetail->insert($data);

            if($res){
                set_flashdata('success','Data berhasil di simpan.');
            }else{
                set_flashdata('warning','Data gagal di simpan.');
            }


            redirect(site_url('lodetail'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id=rapikan($ide);
        $row = $this->Mlodetail->get_by_id($id);

        if ($row) {
            $data = array(
                'title'  => 'Edit lodetail',
                'button' => 'Update',
                'action' => site_url('lodetail/update_action'),
		'Tahun' => set_value('Tahun', $row->Tahun),
		'Kd_Jurnal' => set_value('Kd_Jurnal', $row->Kd_Jurnal),
		'Nm_Jurnal' => set_value('Nm_Jurnal', $row->Nm_Jurnal),
		'Tgl_Bukti' => set_value('Tgl_Bukti', $row->Tgl_Bukti),
		'No_Bukti' => set_value('No_Bukti', $row->No_Bukti),
		'No_BKU' => set_value('No_BKU', $row->No_BKU),
		'Kd_SKPD' => set_value('Kd_SKPD', $row->Kd_SKPD),
		'Nm_Unit' => set_value('Nm_Unit', $row->Nm_Unit),
		'Nm_Sub_Unit' => set_value('Nm_Sub_Unit', $row->Nm_Sub_Unit),
		'ID_Prog' => set_value('ID_Prog', $row->ID_Prog),
		'Ket_Program' => set_value('Ket_Program', $row->Ket_Program),
		'Kd_Keg' => set_value('Kd_Keg', $row->Kd_Keg),
		'Ket_Kegiatan' => set_value('Ket_Kegiatan', $row->Ket_Kegiatan),
		'Keterangan' => set_value('Keterangan', $row->Keterangan),
		'Akun_Akrual_1' => set_value('Akun_Akrual_1', $row->Akun_Akrual_1),
		'Akun_Akrual_2' => set_value('Akun_Akrual_2', $row->Akun_Akrual_2),
		'Akun_Akrual_3' => set_value('Akun_Akrual_3', $row->Akun_Akrual_3),
		'Akun_Akrual_4' => set_value('Akun_Akrual_4', $row->Akun_Akrual_4),
		'Akun_Akrual_5' => set_value('Akun_Akrual_5', $row->Akun_Akrual_5),
		'Nm_Akrual_1' => set_value('Nm_Akrual_1', $row->Nm_Akrual_1),
		'Nm_Akrual_2' => set_value('Nm_Akrual_2', $row->Nm_Akrual_2),
		'Nm_Akrual_3' => set_value('Nm_Akrual_3', $row->Nm_Akrual_3),
		'Nm_Akrual_4' => set_value('Nm_Akrual_4', $row->Nm_Akrual_4),
		'Nm_Akrual_5' => set_value('Nm_Akrual_5', $row->Nm_Akrual_5),
		'Debet' => set_value('Debet', $row->Debet),
		'Kredit' => set_value('Kredit', $row->Kredit),
	    );
            // $this->load->view('lodetail/view_form', $data);
            $this->template->load('layout','lodetail/view_form',$data);
        } else {
                set_flashdata('warning','Record Not Found');
                redirect(site_url('lodetail'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('', TRUE));
        } else {
            $data = array(
		'Tahun' => $this->input->post('Tahun',TRUE),
		'Kd_Jurnal' => $this->input->post('Kd_Jurnal',TRUE),
		'Nm_Jurnal' => $this->input->post('Nm_Jurnal',TRUE),
		'Tgl_Bukti' => $this->input->post('Tgl_Bukti',TRUE),
		'No_Bukti' => $this->input->post('No_Bukti',TRUE),
		'No_BKU' => $this->input->post('No_BKU',TRUE),
		'Kd_SKPD' => $this->input->post('Kd_SKPD',TRUE),
		'Nm_Unit' => $this->input->post('Nm_Unit',TRUE),
		'Nm_Sub_Unit' => $this->input->post('Nm_Sub_Unit',TRUE),
		'ID_Prog' => $this->input->post('ID_Prog',TRUE),
		'Ket_Program' => $this->input->post('Ket_Program',TRUE),
		'Kd_Keg' => $this->input->post('Kd_Keg',TRUE),
		'Ket_Kegiatan' => $this->input->post('Ket_Kegiatan',TRUE),
		'Keterangan' => $this->input->post('Keterangan',TRUE),
		'Akun_Akrual_1' => $this->input->post('Akun_Akrual_1',TRUE),
		'Akun_Akrual_2' => $this->input->post('Akun_Akrual_2',TRUE),
		'Akun_Akrual_3' => $this->input->post('Akun_Akrual_3',TRUE),
		'Akun_Akrual_4' => $this->input->post('Akun_Akrual_4',TRUE),
		'Akun_Akrual_5' => $this->input->post('Akun_Akrual_5',TRUE),
		'Nm_Akrual_1' => $this->input->post('Nm_Akrual_1',TRUE),
		'Nm_Akrual_2' => $this->input->post('Nm_Akrual_2',TRUE),
		'Nm_Akrual_3' => $this->input->post('Nm_Akrual_3',TRUE),
		'Nm_Akrual_4' => $this->input->post('Nm_Akrual_4',TRUE),
		'Nm_Akrual_5' => $this->input->post('Nm_Akrual_5',TRUE),
		'Debet' => $this->input->post('Debet',TRUE),
		'Kredit' => $this->input->post('Kredit',TRUE),
	    );

            $res=$this->Mlodetail->update($this->input->post('', TRUE), $data);
            if($res){
                set_flashdata('success','Data berhasil di update.');
            }else{
                set_flashdata('warning','Data gagal di update.');
            }
            redirect(site_url('lodetail'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id  =rapikan($ide);
        $row = $this->Mlodetail->get_by_id($id);

        if ($row) {
            $res=$this->Mlodetail->delete($id);
            if($res){
                set_flashdata('success','Data berhasil di hapus.');
            }else{
                set_flashdata('warning','Data gagal di hapus.');
            }
            redirect(site_url('lodetail'));
        } else {
            set_flashdata('warning','Record Not Found');
            redirect(site_url('lodetail'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('Tahun', 'tahun', 'trim|required');
	$this->form_validation->set_rules('Kd_Jurnal', 'kd jurnal', 'trim|required');
	$this->form_validation->set_rules('Nm_Jurnal', 'nm jurnal', 'trim|required');
	$this->form_validation->set_rules('Tgl_Bukti', 'tgl bukti', 'trim|required');
	$this->form_validation->set_rules('No_Bukti', 'no bukti', 'trim|required');
	$this->form_validation->set_rules('No_BKU', 'no bku', 'trim|required');
	$this->form_validation->set_rules('Kd_SKPD', 'kd skpd', 'trim|required');
	$this->form_validation->set_rules('Nm_Unit', 'nm unit', 'trim|required');
	$this->form_validation->set_rules('Nm_Sub_Unit', 'nm sub unit', 'trim|required');
	$this->form_validation->set_rules('ID_Prog', 'id prog', 'trim|required');
	$this->form_validation->set_rules('Ket_Program', 'ket program', 'trim|required');
	$this->form_validation->set_rules('Kd_Keg', 'kd keg', 'trim|required');
	$this->form_validation->set_rules('Ket_Kegiatan', 'ket kegiatan', 'trim|required');
	$this->form_validation->set_rules('Keterangan', 'keterangan', 'trim|required');
	$this->form_validation->set_rules('Akun_Akrual_1', 'akun akrual 1', 'trim|required');
	$this->form_validation->set_rules('Akun_Akrual_2', 'akun akrual 2', 'trim|required');
	$this->form_validation->set_rules('Akun_Akrual_3', 'akun akrual 3', 'trim|required');
	$this->form_validation->set_rules('Akun_Akrual_4', 'akun akrual 4', 'trim|required');
	$this->form_validation->set_rules('Akun_Akrual_5', 'akun akrual 5', 'trim|required');
	$this->form_validation->set_rules('Nm_Akrual_1', 'nm akrual 1', 'trim|required');
	$this->form_validation->set_rules('Nm_Akrual_2', 'nm akrual 2', 'trim|required');
	$this->form_validation->set_rules('Nm_Akrual_3', 'nm akrual 3', 'trim|required');
	$this->form_validation->set_rules('Nm_Akrual_4', 'nm akrual 4', 'trim|required');
	$this->form_validation->set_rules('Nm_Akrual_5', 'nm akrual 5', 'trim|required');
	$this->form_validation->set_rules('Debet', 'debet', 'trim|required');
	$this->form_validation->set_rules('Kredit', 'kredit', 'trim|required');

	$this->form_validation->set_rules('', '', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Lodetail.php */
/* Location: ./application/controllers/Lodetail.php */