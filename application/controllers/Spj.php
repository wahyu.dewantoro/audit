<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spj extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Mspj');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Spj';
        return cek($this->id_pengguna,$url,$var);
    }

     public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        if ($q <> '') {
            $cetak=base_url() . 'spj/cetak_index?q='.urlencode($q);
            $config['base_url']  = base_url() . 'spj?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spj?q=' . urlencode($q);
        } else {
            $cetak=base_url() . 'spj/cetak_index';
            $config['base_url']  = base_url() . 'spj?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spj?q=' . urlencode($q);
        }
        // 

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mspj->getRow($q);
        
        $spj                  = $this->Mspj->getalldata($config['per_page'], $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $subunit =$this->Mspj->getSUbunit();
        $data = array(
            'spj_data'     => $spj,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'        => 'Surat Pertanggung Jawaban (SPJ)',
            'subunit'      => $subunit,
            'cetak'        => $cetak,
            // 'script'=>'report/datatables'
        );
        $this->template->load('layout','spj/view_index',$data);
    }

    public function cetak_index()
    {
        
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $spj = $this->Mspj->getAllData($q);
        $data = array(
            'spj_data'     => $spj,
            'q'            => $q,
            'start'        => $start,
            'title'       => 'Surat Pertanggung Jawaban (SPJ)',
        );
        $this->load->view('spj/cetak_index',$data);
    }
    
    /* public function read()
    {
        $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $spj     = $this->Mspj->get_limit_data($nm_unit);
        $start   = intval($this->input->get('start'));
        $data    = array(
            'spj_data' => $spj,
            'title'       => 'Surat Pertanggung Jawaban (SPJ)',
            'akses'     => $akses,
            'nm_unit'   => $nm_unit,
            'kembali'   => base_url().'spj',
            'start'     => $start,
            'cetak'     => base_url().'spj/cetakdua?nm_unit='.$nm_unit
        );
        $this->template->load('layout','spj/view_indexdua',$data);
    }*/

    /*function cetakdua(){
        $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $spj     = $this->Mspj->get_limit_data($nm_unit);
        $start   = intval($this->input->get('start'));
        $data    = array(
            'spj_data' => $spj,
            'title'       => 'Surat Pertanggung Jawaban (SPJ)',
            'nm_unit'   => $nm_unit,
            'start'     => $start,
        );
        $this->load->view('spj/cetak_indexdua',$data);
    }*/

     function readdua(){
        $akses       = $this->cekAkses('read');
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_unit     =urldecode($this->input->get('nm_unit',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $no_spj      =urldecode($this->input->get('no_spj',true));
        $start       = intval($this->input->get('start'));
        
        $spj=$this->Mspj->getdetailspj($kd_skpd,$nm_unit,$nm_sub_unit,$no_spj);
        $data = array(
            'spj_data'    => $spj,
            'title'       => 'Surat Pertanggung Jawaban (SPJ)',
            'akses'       => $akses,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spj'       => $no_spj,
            'kembali'     => base_url().'spj',
            'start'       => $start,
            'script'      => 'report/datatables',
            'cetak'       => base_url().'spj/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_unit='.urlencode($nm_unit).'&nm_sub_unit='.urlencode($nm_sub_unit).'&no_spj='.urlencode($no_spj)
        );

        $this->template->load('layout','spj/view_indextiga',$data);   
    }

    function cetaktiga(){
        $akses       = $this->cekAkses('read');
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_unit     =urldecode($this->input->get('nm_unit',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $no_spj      =urldecode($this->input->get('no_spj',true));
        $start       = intval($this->input->get('start'));
        
        $spj=$this->Mspj->getdetailspj($kd_skpd,$nm_unit,$nm_sub_unit,$no_spj);
        $data = array(
            'spj_data'    => $spj,
            'title'       => 'Surat Pertanggung Jawaban (SPJ)',
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spj'       => $no_spj,
            'start'       => $start,
        );

        $this->load->view('spj/cetak_indextiga',$data);
     
    }


     function cetak(){
        $akses= $this->cekAkses('read');

        $res   =$this->input->get();
        $kd_skpd     =urldecode($res['kd_skpd']);
        $nm_sub_unit =urldecode($res['nm_sub_unit']);
        $jenis_spj   =urldecode($res['jenis_spj']);
        $log1 =urldecode($this->input->get('log1'));
        $log2 =urldecode($this->input->get('log2'));

        // $q           = urldecode($this->input->get('q', TRUE));
        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
         if($kd_rek_gabung ){
             $this->db->where("kd_rek_gabung",$kd_rek_gabung);
         }

         if($tanggal1 <> '' && $tanggal2 <> '' ){
             $t1=explode('/',$tanggal1);
             $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

             $t2=explode('/',$tanggal2);
             $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

             $this->db->where("(tgl_spj between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
         }
         $spj= $this->Mspj->get_all_data($kd_skpd,$nm_sub_unit,$jenis_spj,$q);
         if($kd_rek_gabung ){
             $this->db->where("kd_rek_gabung",$kd_rek_gabung);
         }

         if($tanggal1 <> '' && $tanggal2 <> '' ){
             $t1=explode('/',$tanggal1);
             $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

             $t2=explode('/',$tanggal2);
             $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

             $this->db->where("(tgl_spj between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
         }
         $total=$this->Mspj->getSumdetail($kd_skpd,$nm_sub_unit,$q,$jenis_spj);
         if (isset($total->nilai)) {
             $nilai=$total->nilai;
         } else {
             $nilai=0;
         }
         $data = array(
             'spj_data'    => $spj,
             'nilai'         =>number_format($nilai,'2',',','.'),

         );
         $this->load->view('spj/excel',$data);
    }


}

/* End of file Spj.php */
/* Location: ./application/controllers/Spj.php */