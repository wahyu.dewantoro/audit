<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Saldoawal extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Msaldoawal');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Saldoawal';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'saldoawal?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'saldoawal?q=' . urlencode($q);
            $cetak = base_url() . 'saldoawal/cetak_index?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'saldoawal';
            $config['first_url'] = base_url() . 'saldoawal';
            $cetak = base_url() . 'saldoawal/cetak_index';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Msaldoawal->getRow($q);
        $saldoawal                      = $this->Msaldoawal->getAllData($q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $subunit=$this->Msaldoawal->getSubunit();

        $total=$this->Msaldoawal->getSum($q);
        $data = array(
            'saldoawal_data' => $saldoawal,
            'q'              => $q,
            'pagination'     => $this->pagination->create_links(),
            'total_rows'     => $config['total_rows'],
            'start'          => $start,
            'title'          => 'Saldo Awal',
            'akses'          => $akses,
            'subunit'        =>$subunit,
            'cetak'          => $cetak,
            'debet'          =>number_format($total->debet,'2',',','.'),
            'kredit'         =>number_format($total->kredit,'2',',','.'),
        );
        $this->template->load('layout','saldoawal/view_index',$data);
    }
    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $saldoawal                      = $this->Msaldoawal->getAllData($q);
        $total=$this->Msaldoawal->getSum($q);
        $data = array(
            'saldoawal_data' => $saldoawal,
            'debet'          =>number_format($total->debet,'2',',','.'),
            'kredit'         =>number_format($total->kredit,'2',',','.'),
        );
        $this->load->view('saldoawal/cetak_index',$data);
    }
    public function read()
    {
        $akses       =$this->cekAkses('read');
        $q           = urldecode($this->input->get('q', TRUE));
        $start       = intval($this->input->get('start'));
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit'));
        $akun_akrual_5 =urldecode($this->input->get('akun_akrual_5',true));

        if ($q <> '' || $akun_akrual_5<>'') {
            $config['base_url']  = base_url() . 'saldoawal/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&q=' . urlencode($q).'&akun_akrual_5='.urlencode($akun_akrual_5);
            $config['first_url'] = base_url() . 'saldoawal/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&q=' . urlencode($q).'&akun_akrual_5='.urlencode($akun_akrual_5);

            $cetak=base_url() . 'saldoawal/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&q=' . urlencode($q).'&akun_akrual_5='.urlencode($akun_akrual_5);
        } else {
            $config['base_url']  = base_url() . 'saldoawal/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $config['first_url'] = base_url() . 'saldoawal/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $cetak=base_url() . 'saldoawal/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if ($akun_akrual_5<>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }
        $config['total_rows']        = $this->Msaldoawal->total_rows($kd_skpd,$nm_sub_unit,$q);

        if ($akun_akrual_5<>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        $saldoawal                   = $this->Msaldoawal->get_limit_data($kd_skpd,$nm_sub_unit,$config['per_page'], $start, $q);

        $total=$this->Msaldoawal->getSumdetail($kd_skpd,$nm_sub_unit,$q);
        $akrual5=$this->db->query("select distinct akun_akrual_5,nm_akrual_5
                                    from tb_spe_saldo_awal
                                    where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit'
                                    order by akun_akrual_5 asc")->result();

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'saldoawal_data' => $saldoawal,
            'q'              => $q,
            'pagination'     => $this->pagination->create_links(),
            'total_rows'     => $config['total_rows'],
            'start'          => $start,
            'title'          => 'Saldo Awal',
            'akses'          => $akses,
            'kd_skpd'        =>$kd_skpd,
            'nm_sub_unit'    => $nm_sub_unit,
            'kembali'        =>base_url().'saldoawal',
            'cetak'          =>$cetak,
            'action'         =>base_url() . 'saldoawal/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
            'akrual5'        =>$akrual5,
            'akun_akrual_5'  =>$akun_akrual_5,
            'debet'          =>number_format($total->debet,'2',',','.'),
            'kredit'         =>number_format($total->kredit,'2',',','.'),
        );
        $this->template->load('layout','saldoawal/view_indexdua',$data);
    }


    public function cetak()
    {
        $akses       =$this->cekAkses('read');
        $q           = urldecode($this->input->get('q', TRUE));
        $start       = intval($this->input->get('start'));
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit'));
        $akun_akrual_5 =urldecode($this->input->get('akun_akrual_5',true));

        if ($akun_akrual_5<>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        $saldoawal                   = $this->Msaldoawal->get_limit_data($kd_skpd,$nm_sub_unit,$q);

        $total=$this->Msaldoawal->getSumdetail($kd_skpd,$nm_sub_unit,$q);

        $data = array(
            'saldoawal_data' => $saldoawal,
            'debet'          =>number_format($total->debet,'2',',','.'),
            'kredit'         =>number_format($total->kredit,'2',',','.'),
        );
        $this->load->view('saldoawal/excel',$data);
    }
}

/* End of file Saldoawal.php */
/* Location: ./application/controllers/Saldoawal.php */