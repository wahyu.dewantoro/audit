<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{
		$title='Dashboard';
		$data=array('title'=>$title);
		// $this->load->view('layout',$data);
		$this->template->load('layout','starter',$data);
	}
}
