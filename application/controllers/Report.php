<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Mreport');
        
    }

    private function cekAkses($var=null){
        $url='Report';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $data=array(
            'data'=>$this->Mreport->getList(),
            'title'=>'Report'
            );
        $this->template->load('layout','report/view_index',$data);
    }

   public function limaduatigakurangsejuta(){
        $akses =$this->cekAkses('read');
        $data  =array(
            'data'    =>$this->Mreport->getLimaDuaDua(),
            'title'   =>'Belanja Modal kurang dari 1 juta',
            'script'  =>'report/datatables',
            'kembali' =>base_url().'report'
            );
        $this->template->load('layout','report/view_limaduatiga',$data);
   }

   function limaduadualebihtigapuluh(){
            $akses =$this->cekAkses('read');
        $data  =array(
            'data'=>$this->Mreport->getLimaDuaTiga(),
            'title'=>'Belanja Barang dan Jasa Lebih dari 30 jt',
            'script'=>'report/datatables',
            'kembali'=>base_url().'report'
            );
        $this->template->load('layout','report/view_limaduadua',$data);
   }

   function detailLimaDuaDua(){
        $kd_skpd=$this->input->get('kd_skpd',true);
        $nm_sub_unit=$this->input->get('nm_sub_unit',true);
        $kode_rek_3=$this->input->get('kode_rek_3',true);

        $data=array(
            'title'=> "Belanja Barang dan Jasa Lebih dari 30 jt ( ".$nm_sub_unit." )",
            'data'=>$this->Mreport->getDetailDuaTiga($kd_skpd,$kode_rek_3),
            'script'=>'report/datatables',
            'kembali'=>base_url().'report/limaduadualebihtigapuluh'
            );
        $this->template->load('layout','report/view_detaillimaduadua',$data);
   }

}
 