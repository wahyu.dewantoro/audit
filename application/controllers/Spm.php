<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spm extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Mspm');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Spm';
        return cek($this->id_pengguna,$url,$var);
    }

     public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        if ($q <> '') {
            $cetak=base_url() . 'spm/cetak_index?q='.urlencode($q);
            $config['base_url']  = base_url() . 'spm?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spm?q=' . urlencode($q);
        } else {
            $cetak=base_url() . 'spm/cetak_index';
            $config['base_url']  = base_url() . 'spm?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spm?q=' . urlencode($q);
        }
        // 

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mspm->getRow($q);
        
        $spm                  = $this->Mspm->getalldata($config['per_page'], $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $subunit =$this->Mspm->getSUbunit();
        $data = array(
            'spm_data'     => $spm,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'        => 'Surat Perintah Pembayaran (SPM)',
            'subunit'      => $subunit,
            'cetak'        => $cetak,
            // 'script'=>'report/datatables'
        );
        $this->template->load('layout','spm/view_index',$data);
    }

/*    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        if ($q <> '') {
            $cetak=base_url() . 'spm/cetak_index?q='.urlencode($q);
        } else {
            $cetak=base_url() . 'spm/cetak_index';
        }
        $spm                      = $this->Mspm->getAllData($q);
        $subunit=$this->Mspm->getSUbunit();
        $data = array(
            'spm_data'     => $spm,
            'q'            => $q,
            'start'        => $start,
            'title'        => 'Surat Perintah Pembayaran (SPM)',
            'subunit'      => $subunit,
            'cetak'        => $cetak
        );
        $this->template->load('layout','spm/view_index',$data);
    }*/

    public function cetak_index()
    {
        
        $akses = $this->cekAkses('read');
         $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        if ($q <> '') {
            $cetak=base_url() . 'spm/cetak_index?q='.urlencode($q);
            $config['base_url']  = base_url() . 'spm?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spm?q=' . urlencode($q);
        } else {
            $cetak=base_url() . 'spm/cetak_index';
            $config['base_url']  = base_url() . 'spm?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spm?q=' . urlencode($q);
        }
        // 

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mspm->getRow($q);
        
        $spm                  = $this->Mspm->getalldata(100000, $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        // $subunit =$this->Mspm->getSUbunit();
        $data = array(
            'spm_data'     => $spm,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'        => 'Surat Perintah Pembayaran (SPM)',
            // 'subunit'      => $subunit,
            // 'cetak'        => $cetak,
            // 'script'=>'report/datatables'
        );
        $this->load->view('spm/cetak_index',$data);
    }

    public function read()
    {
        $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $spm     = $this->Mspm->get_limit_data($nm_unit);
        $start   = intval($this->input->get('start'));
        $data    = array(
            'spm_data' => $spm,
            'title'       => 'Surat Perintah Pembayaran (SPM)',
            'akses'     => $akses,
            'nm_unit'   => $nm_unit,
            'kembali'   => base_url().'spm',
            'start'     => $start,
            'cetak'     => base_url().'spm/cetakdua?nm_unit='.$nm_unit
        );
        $this->template->load('layout','spm/view_indexdua',$data);
    }


    function cetakdua(){
        $akses   = $this->cekAkses('read');
        $nm_unit =urldecode($this->input->get('nm_unit',true));
        $spm     = $this->Mspm->get_limit_data($nm_unit);
        $start   = intval($this->input->get('start'));
        $data    = array(
            'spm_data' => $spm,
            'title'       => 'Surat Perintah Pembayaran (SPM)',
            'nm_unit'   => $nm_unit,
            'start'     => $start,
        );
        $this->load->view('spm/cetak_indexdua',$data);
    }

     function readdua(){
        $akses       = $this->cekAkses('read');
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_unit     =urldecode($this->input->get('nm_unit',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $no_spm      =urldecode($this->input->get('no_spm',true));
        $start       = intval($this->input->get('start'));
        
        $spm=$this->Mspm->getdetailspm($kd_skpd,$nm_unit,$nm_sub_unit,$no_spm);
        $data = array(
            'spm_data'    => $spm,
            'title'        => 'Surat Perintah Pembayaran (SPM)',
            'akses'       => $akses,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spm'       => $no_spm,
            'kembali'     => base_url().'spm?q='.urlencode($nm_unit),
            'start'       => $start,
            'script'      => 'report/datatables',
            'cetak'       => base_url().'spm/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_unit='.urlencode($nm_unit).'&nm_sub_unit='.urlencode($nm_sub_unit).'&no_spm='.urlencode($no_spm)
        );

        $this->template->load('layout','spm/view_indextiga',$data);   
    }


    function cetaktiga(){
        $akses       = $this->cekAkses('read');
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_unit     =urldecode($this->input->get('nm_unit',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $no_spm      =urldecode($this->input->get('no_spm',true));
        $start       = intval($this->input->get('start'));
        
        $spm=$this->Mspm->getdetailspm($kd_skpd,$nm_unit,$nm_sub_unit,$no_spm);
        $data = array(
            'spm_data'    => $spm,
            'title'        => 'Surat Perintah Pembayaran (SPM)',
            'akses'       => $akses,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spm'       => $no_spm,
            // 'kembali'     => base_url().'spm?q='.urlencode($nm_unit),
            'start'       => $start,
            // 'script'      => 'report/datatables',
            // 'cetak'       => base_url().'spm/cetaktiga?kd_skpd='.urlencode($kd_skpd).'&nm_unit='.urlencode($nm_unit).'&nm_sub_unit='.urlencode($nm_sub_unit).'&no_spm='.urlencode($no_spm)
        );

        $this->load->view('spm/cetak_indextiga',$data);
    }

    function cetak(){
        $res=$this->input->get();
        $q           = urldecode($this->input->get('q', TRUE));
        $kd_skpd     =urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit =urldecode($this->input->get('nm_sub_unit',true));
        $jenis_spm   =urldecode($this->input->get('jenis_spm',true));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));


        if($tanggal1==''){
            $tanggal1='01/01/'.date('Y');
        }

        if($tanggal2==''){
            $tanggal2=date('d/m/Y');
        }
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spm between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $spm                         = $this->Mspm->get_all_data($kd_skpd,$nm_sub_unit,$jenis_spm,$q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spm between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mspm->getSumdetail($kd_skpd,$nm_sub_unit,$q,$jenis_spm);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $data = array(
            'spm_data'    => $spm,
            'nilai'         =>number_format($nilai,'2',',','.'),

        );
        // echo $data['kd_skpd'];
        $this->load->view('spm/excel',$data);
    }



}

/* End of file Spm.php */
/* Location: ./application/controllers/Spm.php */