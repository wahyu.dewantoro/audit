<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subunit extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Msubunit');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Subunit';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses     =$this->cekAkses('read');
        $q         = urldecode($this->input->get('q', TRUE));
        $start     = intval($this->input->get('start'));
        $nm_bidang =urldecode($this->input->get('nm_bidang',true));

        if ($q <> '' || $nm_bidang <>'') {
            $config['base_url']  = base_url() . 'subunit?q=' . urlencode($q).'&nm_bidang='.urlencode($nm_bidang);
            $config['first_url'] = base_url() . 'subunit?q=' . urlencode($q).'&nm_bidang='.urlencode($nm_bidang);
        } else {
            $config['base_url']  = base_url() . 'subunit';
            $config['first_url'] = base_url() . 'subunit';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($nm_bidang<>''){
            $this->db->where('nm_bidang',$nm_bidang);
        }
        $config['total_rows']        = $this->Msubunit->total_rows($q);
        if($nm_bidang<>''){
            $this->db->where('nm_bidang',$nm_bidang);
        }
        $subunit                      = $this->Msubunit->get_limit_data($config['per_page'], $start, $q);

        $bidang=$this->db->query("select distinct nm_bidang from tb_spe_ref_sub_unit")->result();

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'subunit_data' => $subunit,
            'q'            => $q,
            'pagination'   => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],
            'start'        => $start,
            'title'        => 'Refrensi Sub Unit',
            'akses'        => $akses,
            'bidang'       =>$bidang,
            'nm_bidang'    =>$nm_bidang
        );
        $this->template->load('layout','subunit/view_index',$data);
    }

  

}

/* End of file Subunit.php */
/* Location: ./application/controllers/Subunit.php */