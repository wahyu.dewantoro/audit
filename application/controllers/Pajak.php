<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pajak extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Mpajak');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Pajak';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'pajak?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pajak?q=' . urlencode($q);
            $cetak = base_url() . 'pajak/cetak_index?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pajak';
            $config['first_url'] = base_url() . 'pajak';
            $cetak = base_url() . 'pajak/cetak_index';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mpajak->getRow($q);
        $pajak                      = $this->Mpajak->getAllData($q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $subunit=$this->Mpajak->getSubunit();
        $total=$this->Mpajak->getSum($q);
        $data = array(
            'pajak_data' => $pajak,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'title'      => 'Pajak',
            'akses'      => $akses,
            'subunit'     =>$subunit,
            'cetak'      => $cetak,
            'nilai'=>number_format($total->nilai,'2',',','.'),
        );

        $this->template->load('layout','pajak/view_index',$data);
    }
    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $pajak                      = $this->Mpajak->getAllData($q);
        $total=$this->Mpajak->getSum($q);
        $data = array(
            'pajak_data' => $pajak,
            'nilai'=>number_format($total->nilai,'2',',','.'),
        );

        $this->load->view('pajak/cetak_index',$data);
    }
    public function read()
    {
        $akses         =$this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $kd_skpd       =urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit   =urldecode($this->input->get('nm_sub_unit',true));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung',true));

        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));

        $start = intval($this->input->get('start'));

        if($tanggal1==''){
            $tanggal1='01/01/'.date('Y');
        }

        if($tanggal2==''){
            $tanggal2=date('d/m/Y');
        }

        if ($q <> '' || $kd_rek_gabung<>'' || $tanggal1 <> '' || $tanggal2<>'') {
            $config['base_url']  = base_url() . 'pajak/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&q='.urlencode($q).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $config['first_url'] = base_url() . 'pajak/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&q='.urlencode($q).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $cetak               = base_url() . 'pajak/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&q='.urlencode($q).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
        } else {
            $config['base_url']  = base_url() . 'pajak/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $config['first_url'] = base_url() . 'pajak/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $cetak               = base_url() . 'pajak/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($kd_rek_gabung<>''){
            $this->db->where('kd_rek_gabung',$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }


        $config['total_rows']        = $this->Mpajak->total_rows($kd_skpd,$nm_sub_unit,$q);
        if($kd_rek_gabung<>''){
            $this->db->where('kd_rek_gabung',$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $pajak                       = $this->Mpajak->get_limit_data($kd_skpd,$nm_sub_unit,$config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);


        if($kd_rek_gabung<>''){
            $this->db->where('kd_rek_gabung',$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }

        $total=$this->Mpajak->getSumdetail($kd_skpd,$nm_sub_unit,$q);


        $rekening=$this->db->query("select distinct kd_rek_gabung,nm_rek_5
                                    from tb_spe_pajak
                                    where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit'")->result();

        $data = array(
            'pajak_data'    => $pajak,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Pajak',
            'akses'         => $akses,
            'kd_skpd'       =>$kd_skpd,
            'nm_sub_unit'   =>$nm_sub_unit,
            'kembali'       =>base_url().'pajak',
            'action'        =>base_url() . 'pajak/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
            'rekening'      =>$rekening,
            'kd_rek_gabung' =>$kd_rek_gabung,
            'nilai'=>number_format($total->nilai,'2',',','.'),
            'tanggal1' =>$tanggal1,
            'tanggal2' =>$tanggal2,
            'cetak'         => $cetak
        );

        $this->template->load('layout','pajak/view_indexdua',$data);
    }

    public function cetak()
    {
        $akses         =$this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $kd_skpd       =urldecode($this->input->get('kd_skpd',true));
        $nm_sub_unit   =urldecode($this->input->get('nm_sub_unit',true));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung',true));

        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if($kd_rek_gabung<>''){
            $this->db->where('kd_rek_gabung',$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $pajak                       = $this->Mpajak->get_all_data($kd_skpd,$nm_sub_unit,$q);
        if($kd_rek_gabung<>''){
            $this->db->where('kd_rek_gabung',$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mpajak->getSumdetail($kd_skpd,$nm_sub_unit,$q);
        $data = array(
            'pajak_data'    => $pajak,
            'nilai'=>number_format($total->nilai,'2',',','.'),
        );

        $this->load->view('pajak/excel',$data);
    }

}

/* End of file Pajak.php */
/* Location: ./application/controllers/Pajak.php */