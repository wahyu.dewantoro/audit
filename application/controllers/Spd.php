<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spd extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna=$this->session->userdata('audit_id_pengguna');
        $this->load->model('Mspd');
        $this->load->library('form_validation');
    }

    private function cekAkses($var=null){
        $url='Spd';
        return cek($this->id_pengguna,$url,$var);
    }

    public function index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'spd?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spd?q=' . urlencode($q);
            $cetak               = base_url() . 'spd/cetak_index?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'spd';
            $config['first_url'] = base_url() . 'spd';
            $cetak               = base_url() . 'spd/cetak_index';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mspd->gettotal($q);
        $spd                      = $this->Mspd->getAlldata($q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $total=$this->Mspd->getSumNilai($q);
        $subunit=$this->Mspd->getSUbunit();

        $data = array(
            'spd_data' => $spd,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Surat Penyediaan Dana (SPD)',
            'akses'               => $akses,
            'subunit'               =>$subunit,
            'cetak'                 => $cetak,
            'jum_nilai'             =>number_format($total->jum_nilai,'2',',','.'),
        );
        $this->template->load('layout','spd/view_index',$data);
    }

    public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $spd                      = $this->Mspd->getAlldata($q);
        $total=$this->Mspd->getSumNilai($q);
        $data = array(
            'spd_data' => $spd,
            'jum_nilai'             =>number_format($total->jum_nilai,'2',',','.'),
        );
        $this->load->view('spd/cetak_index',$data);
    }
   public function read()
    {
        $akses=$this->cekAkses('read');
        $res   =$this->input->get();
        $Kd_SKPD     =urldecode($res['Kd_SKPD']);
        $Nm_Sub_Unit =urldecode($res['Nm_Sub_Unit']);

        // $q           = urldecode($this->input->get('q', TRUE));
        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));


        if($tanggal1==''){
            $tanggal1='01/01/'.date('Y');
        }

        if($tanggal2==''){
            $tanggal2=date('d/m/Y');
        }

         if ($kd_rek_gabung<>'' || $q<> '' || $tanggal1 <> '' || $tanggal2<>'') {
            $config['base_url']  = base_url() . 'spd/read?Kd_SKPD='.urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $config['first_url'] = base_url() . 'spd/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $cetak               =base_url() . 'spd/excel?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
        } else {
            $config['base_url']  = base_url() . 'spd/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit);
            $config['first_url'] = base_url() . 'spd/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit);
            $cetak               = base_url() . 'spd/excel?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $config['total_rows']        = $this->Mspd->total_rows($Kd_SKPD,$Nm_Sub_Unit,$q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $spd                      = $this->Mspd->get_limit_data($Kd_SKPD,$Nm_Sub_Unit,$config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mspd->getSumdetail($Kd_SKPD,$Nm_Sub_Unit,$q);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $rekening=$this->db->query("select distinct kd_rek_gabung,nm_rek_5
                                    from tb_spe_spd
                                    where kd_skpd='$Kd_SKPD' and nm_sub_unit='$Nm_Sub_Unit'
                                    order by nm_rek_5 asc")->result();
        $data = array(
            'spd_data'    => $spd,
            'q'           => $q,
            'pagination'  => $this->pagination->create_links(),
            'total_rows'  => $config['total_rows'],
            'start'       => $start,
            'title'       => 'Surat Penyediaan Dana (SPD)',
            'akses'       => $akses,
            'Kd_SKPD'     =>$Kd_SKPD,
            'Nm_Sub_Unit' =>$Nm_Sub_Unit,
            'action'      =>base_url() . 'spd/read?Kd_SKPD=' . urlencode($Kd_SKPD).'&Nm_Sub_Unit='.urlencode($Nm_Sub_Unit),
            'kembali'     =>base_url().'spd',
            'cetak'         =>$cetak,
            'rekening'      =>$rekening,
            'kd_rek_gabung' =>$kd_rek_gabung,
            'nilai'         =>number_format($nilai,'2',',','.'),
            'tanggal1'      =>$tanggal1,
            'tanggal2'      =>$tanggal2,

        );
        $this->template->load('layout','spd/view_indexdua',$data);

    }

    function excel(){
        $akses=$this->cekAkses('read');
        $res   =$this->input->get();
        $Kd_SKPD     =urldecode($res['Kd_SKPD']);
        $Nm_Sub_Unit =urldecode($res['Nm_Sub_Unit']);
        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $spd                      = $this->Mspd->get_limit_data($Kd_SKPD,$Nm_Sub_Unit,$q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mspd->getSumdetail($Kd_SKPD,$Nm_Sub_Unit,$q);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $data = array(
            'spd_data'    => $spd,
            'nilai'         =>number_format($nilai,'2',',','.'),

        );
        $this->load->view('spd/excel',$data);
    }


}

/* End of file Spd.php */
/* Location: ./application/controllers/Spd.php */