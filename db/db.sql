/*
SQLyog Ultimate v9.02 
MySQL - 5.5.5-10.1.28-MariaDB : Database - beagle
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ms_assign_role` */

DROP TABLE IF EXISTS `ms_assign_role`;

CREATE TABLE `ms_assign_role` (
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  `ms_pengguna_id` int(11) DEFAULT NULL,
  `ms_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_inc`),
  KEY `FK_ms_assign_role` (`ms_role_id`),
  KEY `fk_pengguna` (`ms_pengguna_id`),
  CONSTRAINT `FK_ms_assign_role` FOREIGN KEY (`ms_role_id`) REFERENCES `ms_role` (`id_inc`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pengguna` FOREIGN KEY (`ms_pengguna_id`) REFERENCES `ms_pengguna` (`id_inc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ms_assign_role` */

insert  into `ms_assign_role`(`id_inc`,`ms_pengguna_id`,`ms_role_id`) values (1,1,1);

/*Table structure for table `ms_menu` */

DROP TABLE IF EXISTS `ms_menu`;

CREATE TABLE `ms_menu` (
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(100) DEFAULT NULL,
  `link_menu` varchar(100) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `ms_menu` */

insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (1,'Sistem','#',0,99,'mdi mdi-settings-square'),(2,'role','role',1,1,'fa fa-angle-right'),(3,'menu','menu',1,2,'fa fa-angle-right'),(6,'Pengguna','Pengguna',1,3,'fa fa-users'),(7,'Asign Role','AsignRole',1,4,'fa fa-lock'),(8,'Dashboard','Welcome',0,0,'mdi mdi-home');

/*Table structure for table `ms_pengguna` */

DROP TABLE IF EXISTS `ms_pengguna`;

CREATE TABLE `ms_pengguna` (
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ms_pengguna` */

insert  into `ms_pengguna`(`id_inc`,`nama`,`username`,`password`) values (1,'Wahyu Dewantoro','admin',''),(2,'Pengguna A','user1','');

/*Table structure for table `ms_privilege` */

DROP TABLE IF EXISTS `ms_privilege`;

CREATE TABLE `ms_privilege` (
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  `ms_role_id` int(11) DEFAULT NULL,
  `ms_menu_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create` int(11) DEFAULT NULL,
  `update` int(11) DEFAULT NULL,
  `delete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_inc`),
  KEY `fk_privilege` (`ms_menu_id`),
  KEY `fk_role` (`ms_role_id`),
  CONSTRAINT `fk_privilege` FOREIGN KEY (`ms_menu_id`) REFERENCES `ms_menu` (`id_inc`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role` FOREIGN KEY (`ms_role_id`) REFERENCES `ms_role` (`id_inc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `ms_privilege` */

insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (1,1,1,1,NULL,NULL,NULL),(2,1,2,1,1,1,1),(3,1,3,1,1,1,1),(8,1,6,1,1,1,NULL),(9,1,7,1,1,1,1),(10,1,8,1,1,1,NULL);

/*Table structure for table `ms_role` */

DROP TABLE IF EXISTS `ms_role`;

CREATE TABLE `ms_role` (
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ms_role` */

insert  into `ms_role`(`id_inc`,`nama_role`) values (1,'Super User');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
